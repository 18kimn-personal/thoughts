---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: the BSD license
tags: [ip]
author: Nathan
---

Comes in four flavors:

- Zero clause
  - Permission to use, copy, modify, and/or distribute this
    software for any purpose with or without fee is hereby
    granted.
- Two clause: Redistribution and use in source and binary
  forms, with or without modification, are permitted
  provided that the following conditions are met:
  1.  Redistributions of source code must retain the above
      copyright notice, this list of conditions and the
      following disclaimer.
  2.  Redistributions in binary form must reproduce the
      above copyright notice, this list of conditions and
      the following disclaimer in the documentation and/or
      other materials provided with the distribution.
- Three clause: Neither the name of the organization nor the
  names of its contributors may be used to endorse or
  promote products derived from this software without
  specific prior written permission.
- Four clause: All advertising materials mentioning features
  or use of this software must display the following
  acknowledgement: This product includes software developed
  by the organization.

The timeline: four clause came first (1990), then three
clause then two clause (but three+two came generally at the
same time) in 1999; zero clause came last in 2006.
Two-clause is known as "FreeBSD" license for its use in
FreeBSD software.

[MIT](./MIT.md) is similar ot the two-clause license since
it doesn't have qualifications on promotions/advertising.

This feels like [Apache](./Apache.md) to me in being focused
on patents and traditional protections of intellectual
property, and is not concerned in any way with the licenses
of their successors ([GPL](./GPL.md)'s copyleft clause) or
[Hippocratic's](./Hippocratic.md) license's ethical
obligations.

---

Copyright (c) <year> <owner>. All rights reserved.

Redistribution and use in source and binary forms, with or
without modification, are permitted provided that the
following conditions are met:

- 1. Redistributions of source code must retain the above
     copyright notice, this list of conditions and the
     following disclaimer.
- 2. Redistributions in binary form must reproduce the above
     copyright notice, this list of conditions and the
     following disclaimer in the documentation and/or other
     materials provided with the distribution.
- 3. All advertising materials mentioning features or use of
     this software must display the following
     acknowledgement:  
     This product includes software developed by the
     organization.
- 4. Neither the name of the copyright holder nor the names
     of its contributors may be used to endorse or promote
     products derived from this software without specific
     prior written permission.

THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
