---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: the MIT license
tags: [ip]
author: Nathan
---

Most permissive canonical/mainstream software license. In my
view the libertarian's favored license.

I oppose it because I think there should be restrictions.
Software shoudln't be used for evil, and developers should
prevent that if they have a chance (choose a restrictive
license). Free software shouldn't be able to be used in
closed software more generally. We have a chance and an
obligation to spread ethical free software, and we should
take that.

---

## The MIT License (MIT)

Copyright © 2021 <copyright holders>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the
Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall
be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
