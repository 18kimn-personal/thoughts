---
created: 2022-01-15
modified: [2022-01-15, 2022-01-23]
title: A love letter to R
author: Nathan
---

I love R. R was the first programming language I've ever
learned in depth, it was the first programming language that
I fell in love with. The first programming language where I
would often stop what I was doing and look up a particular
syntax, or try to figure out how paradigms common in other
languages could be migrated over. It was the first
programming language where I felt frustrated and lonely I
had no one to work with, and later where I felt so thankful
I had people to work with. The first language where I
learned in isolation, the first language where I learned in
community with others, the first language where I
enthusiastically taught others.

I love R's orientation. Is it a statistical programming
language, as the official CRAN header ("The R Project for
Statistical Computing") might suggest? Or as a subheading
describes, is it a software environment for statistical
computing and graphics? Or is R a general programming
language, just as useful for a production-ready web app as
any other language (as Joe Cheng has
[argued](https://www.youtube.com/watch?v=Wy3TY0gOmJw))? The
answer is yes. R is those things. R has some obvious
intentions and scopes, but using R on a day-to-day basis
forces us to look past the supposed "intended scopes" and
redefine them for ourselves.

I love R's syntax. R being dynamically typed, has `<-` in
addition to `=` for variable assignment, no variable
declaration keywords, runs through a live console instead of
through compilation or through reloading through a browser
(see: JS), and so on. These might be points of criticism for
instability and just plain oddity. But I think these
features make them easier for new programmers to learn. We
don't have to worry about types or declarations, and the
arrow `<-` implies a flow of data into a variable. For new
learners, these features let you write immediately.

I love R's resources. Most of all the help command `?` is
better than any other -- way better than the Unix man pages,
than MDN or W3 web docs, than any RFC online could explain.
The single common interface, requiring examples for
contributed functions, and including support for vignettes
and links between docs. Only a language that loves its users
would provide this. And R, I love you too.

But most of all, dwarfing everything else I love about R, I
love the community that embraces and supports R. R is what
we can have if programming were democratic and open.
Supported by wonderful people who are concerned about being
friendly and welcoming to new R users, supported by tireless
volunteers patching and updating code, supported by the
ambitions of those that take R further. Far more than any
other language I have ever experienced, R has teachers for
non-programmers, companies like RStudio that value just and
good ways to do work. R has people that support the language
and others that want to learn it. R is what we can have if
we support each other.
