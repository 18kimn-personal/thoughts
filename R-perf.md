---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22, 2022-01-23, 2022-02-15]
title: R and performance
tags: [perf, R]
author: Nathan
---

Optimization in R is a perpetual topic, because R is so
high-level

To be honest, I don't _really_ understand why tings in R are
slower than in other languages (particularly JavaScript,
which I feel is just insanely fast for many things). Python
and Julia are two langauges R is constantly compared to for
its lack of speed

I really have encountered performance as a problem when
dealing with modeling with large datasets, and particularly
with spatial data. The problems are threefold:

1. For spatial models, this problem is compounded by the
   ridiculously detailed nature of spatial data and how a
   geometry column can often be many, many, many times
   larger in bytes than the rest of a data.frame
2. For regular models, computations often involve N^2
   operations because of how matrix multiplication works
3. For many spatial models, regression often proceeds by
   running a linear model first, then grabbing some
   estimates from it and adjusting it with a second
   matrix-expansive process (don't ask me about the details)

I concede that 3) really isn't a game-breaking problem; if
we solve 1) or 2), then 3) is solved by default.

The first problem I think can be solved by vastly
simplifying data using rmapshaper

---

Now that I think about it...I think R is pretty fast, it
just feels slow because the kind of things I have to do are
kind of intensive for the computer. E.g. in JavaScript (v8)
sorting a 100-item array feels instantaneous, but it would
also be really really fast in R. R just feels slow because I
have to sort 1million-length arrays, or dataframes with 100
columns which are a million rows long.
