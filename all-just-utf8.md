---
created: 2022-01-13
modified: [2022-01-13, 2022-01-22]
title: It's all just utf-8
tags: [plaintext]
author: Nathan
---

Possibly the thing I love most about markdown is that it is
extensible. But the converse is just as valuable: you
actually don't need any form of extensions at all to write
in it.

In the end, at its core, it's just utf-8. Just a plain text
file.
