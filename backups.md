---
created: 2022-02-20
modified: [2022-02-20]
title: Restoring from backups
tags: [workflow]
author: Nathan
---

lost about a week's worth of jounral entries becasue I
didn't mount/unmount properly. sigh. This was also one case
in where a backup actually came in handy, and where copying
everything (e.g. even private files) and in a raw form
(instead of tar.gz'ed) proved useful. It also showed the
need for regular backups, as opposed to sporadic ones.
