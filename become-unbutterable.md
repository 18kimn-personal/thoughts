---
created: 2022-03-17
modified: [2022-03-17]
title: Become unbutterable
tags: [media]
author: Nathan
---

jorts the cat is a popular meme right now, about this cat
named jorts who was thought to be a little bit dumb by his
caretakers. One caretaker tried hard to get him to learn by
buttering him etc (to get him to clean), the other tried to
make accomodations for him with doorstops etc. The two got
into a bit of conflict about this, with the former even
accusing the latter of ethnic stereotyping by saying jorts
(an orange tabby cat) didn't have the smarts of jean the
tortoiseshell cat

https://www.reddit.com/r/AmItheAsshole/comments/rfwgmc/aita_for_perpetuating_ethnic_stereotypes_about/

it's quite a funny story about a white woman (im pretty sure
coworker 1 is a white woman, though i cant remember if it's
actually explained) and her insistence on fake wokeness, but
jorts the cat has really become popular becuase of a twitter
account using jorts' identity as a statement of being proud
and being valued no matter what you are, most directly in
the form of pro-union tweets and memes

https://twitter.com/JortsTheCat

One meme is "become unbutterable":

https://twitter.com/JortsTheCat/status/1500905900428587009

a reference to the anarchist meme/slogan "become
ungovernable," or resist the powers at be and the ways they
discipline. For jorts, that's avoiding being buttered and
trying to fit a certain mold and behavior that has been set
out by him by someone who supposedly knows best (spoiler:
butter/margarine gives cats diarrhea)

I think I find this noteworthy (literally) because it's so
.... non-theory politics! We love cats! we love unions! Do
we really need any more theory???

I mean we do, but definitely not to create at least the kind
of pro-union sentiment this account is trying to create, and
more generally this is such a useful and nonintimidating
introduction for people new to this sort of poplitics. I
sent this to eunkyung (younger sister, not anne) and she
seemed to like it a lot. That felt good to me.
