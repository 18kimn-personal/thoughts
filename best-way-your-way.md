---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: The best way is your way
tags: [workflow, sustainability]
author: Nathan
---

I've written a bit about workflow (as evidenced by this
folder), and a lot of those conclusions I feel has
implicitly come to support or criticize certain ways of
working

Those are opinions I definitely have, but one thing I
realized as I was messing around with Pandoc and RMarkdown
is that the by far the best strategy is simply _your
strategy_. "Your" as in:

- you chose the strategy
- you have the means to use the strategy
- you know to a useful level how the strategy works and can
  provide your own insights into it

With these and possibly other qualifiers, you have ownership
over your method. And that is by far the best way to work. I
hate tinkering around with things I don't understand just to
get references to work, often requiring me to get a million
plugins that do the job for me. It is better to go as
low-level as reasonably possible during a learning stage and
use that low-level tool during my work. Altneratively, I
should reach for higher-level tools only when I understand
how and why they work -- not when I know what they should do
and frustratingly struggle with trying to get them to do it.
