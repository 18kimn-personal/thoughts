---
created: 2022-01-22
modified: [2022-01-22]
title: Beyond Zettelkasten
tags: [zettelkasten]
author: Nathan
---

Even if you don't use Zettelkasten, there are a few
principles that it depends on that I think are helpful for
anyone that is trying to deal with a large amount of
information.

At its core, it's the observation that our mind is chaos;
our thoughts are both fleeting and unorganized. We should
try to work with this:

- archive: write down or archive our thoughts, to make them
  permanent or at least longer-lasting
- organize: group similar thoughts together or make
  connections between thoughts
- repeat: revisit your old thoughts from yesterday or last
  week or last month - having a fresh set of eyes just
  brings new insight - even if not trying to bring new
  insight, revisiting things can help make things more
  permanent \*\* - esp if selectively or with selective
  emphasis

I continue to say "thoughts" here but I really mean anything
we encounter, regardless of if it were our own original
thoughts or not. Most obviously these are readings, but also
quotes from talks or meetings, frustrations that we're
having, etc.

Lastly, I said that FUCK I FORGOT
