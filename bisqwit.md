---
created: 2022-01-02
modified: [2022-01-02, 2022-01-13, 2022-01-22]
title: Bisqwit
tags: [plaintext]
author: Nathan
---

I started watching a few of
[Bisqwit's](https://www.youtube.com/channel/UCKTehwyGCKF-b2wo0RKwrcg)
videos. He's a Finnish programmer, I'd say middle-aged ?

Anyhow as I was watching the video and him explain what
terminal he uses I realized that my other thoughts on why
people use Vim have been a little too naive. I forgot that a
ton of people grew up or learned programming or gained
fifteen years of programming experience etc in a time when
even graphical environments weren't available and
terminal-based editors like Vim were the only option, and
Vim was actually one of the easiest of these to learn and
use.

I bet that a bunch of the current generation of vim users
has arisen because their mentors used vim and so they used
vim too

(of course and I bet a bunch, maybe most, of the current
generation of vim users started using vim for other reasons.
I'm not really trying to quantify or measure things here)

IOTW it's not that people use vim only because they want to
use something difficult but powerful, or because they're
clout chasers who want to be real programmers (me), but
because they learned it first. As Bisqwit said, there's no
undoing fifteen years of muscle memory :)
