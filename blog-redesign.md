---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: Towards a simpler, more preformant, cleaner blog
tags: [workflow, perf]
author: Nathan
---

I want to redo my blog, to incorporate some of these new
perspectives on web development that I've gained over the
past few months. The general perspective in a sentence is
that doing less is often doing more, and if I want to do
more then the "more" should be done async because the main
part of my website that should be focused on can and should
be loaded _quickly_.

Right now the blog is in Gatsby, meaning React, using React
Spring in some places and Framer motion in others. It loads
a background animation, has layout shift animations, and
downloads a wild amount of javascript for each page.^[To be
honest, Gatsby isn't that bad. It downloads a mild amount of
JSON for each page load, which typically comes in at less
than 40kB].

The new blog will improve on this in a few ways:

- redo the blog in Svelte, so that the preprocessing step
  can eliminate some unused JavaScript code
- Use svelte-store to fetch all blog posts upon the _first
  initial page load_, essentially returning a list of
  promises, so that there doesn't have to be a single
  network request to actually navigate anywhere on the site,
  since the network requests begin up-front and run in the
  background
  - the pages will be loaded into a JSON cache. When a user
    clicks a page, they'll either be given the prefetched
    result or will be directed to an entirely new page (e.g.
    like an MPA)
  - this might have to be updated after some benchmarking
  - routing can be managed smoothly and proactively
  -
- also use svelte-store to fetch all of the pictures on the
  first initial page load, so that the projects page doesn't
  have to do a reload every time the iamges are loaded
  - also on the projects page, set the default to be a
    no-image list format instead of with images

There are some other redesign ideas that are on the same
page of slimming down and returning to the roots of simple
and quick web design, but aren't necessarily related to
performance:

- _don't use a styling solution._ Don't use material UI,
  don't use css-in-js, don't even use scss. Just use scoped
  styles that Svelte has by default
-

[some metrics on the svelte vs gatsby side of things](https://www.swyx.io/svelte-static/)
