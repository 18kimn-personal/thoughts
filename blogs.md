---
created: 2022-08-04
modified: [2022-08-04]
title: Less tech-oriented blogs?
tags: [social]
author: Nathan
---

I want to read more long-form individually-authored content,
published freely on the beautiful platform of the web, that
isn't about some new rust struct or sql extension. But
unfortunately all of the most common rss aggregators contain
tech-oriented sites, mostly towards low-level and systems
stuff (why?) but a smattering of stuff up to the frontend
lelow-level and systems stuff (why?) but a smattering of
stuff up to the frontend level too

The days of blogspot seem to be over. Meaning the days of a
platform non-tech people could be invested in and care
about. Nowadays every blogging platform is somehow
tech-oriented, whether because it has all the best markdown
features (all of them), is super light (bear?), has a rich
plugin interface (hugo?), supports scientific authoring
(quarto?), integrates with your favorite UI framework (bunch
of CMSes in general, mdsvex, etc). And the days of syndicated
platforms are ...weird, e.g. Medium i think is past its prime,
but we're definitely there, in the age of substack and
subscriber lists

I want to read a sociologist's journal entry, their reading
lists, their new idea for a project, their reaction to a
tweet...
