this one is a bit tough to figure out

I started using beaker bookmarker recently (~2 weeks ago),
which is a great platform.

One thing they don't do is make bookmarks organizable. So I
thought of making a locally deployable/daemonizable
hyperdrive that displays/stores/organizes these. Then after
I saw the [library](./library.md) bookshelf idea I thought
that this could also be used for bookmarks too. But it would
sort of defeat the purpose of having a web-based bookmarks
management tool.

So I'll stick to
[beaker-bookmarker](personal/projects/beaker-bookmarker/README.md)
for now.
