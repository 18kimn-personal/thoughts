---
modified: [2022-01-23]
date: 2021-11-27
tags: [big_tech]
title: Browsers
---

Argh, browsers.

I uninstalled Chrome and went to Beaker Browser a few months
ago, because I liked the premise a _lot_. I still watch Tara
Vancil's talk once a week or so. There were definitely some
user difficulties involved:

- terrible bookmarks support
- hyperdrive often crashed and daemon wouldn't automatically
  restart
- no passwords manager
- no extensions support, even though it's just Chromium
  under the hood

But this was sort of expected, and even presented fun
challenges^[like
[beaker-bookmarker](https://github.com/18kimn/beaker-bookmarker)].
However I've become a bit jaded as the project doesn't seem
to be under active development:

- Last commit was in
  [December 2020](https://github.com/beakerbrowser/beaker)
- Last retweet was in
  [February 2021](https://twitter.com/pfrazee/status/1358455868950511619)
  and last actual tweet was in
  [January](https://twitter.com/BeakerBrowser/status/1352484622257565697)

Since it's now November, I suspect that the maintainers
think that the experiment is effectively finished now that
it is at a 1.0. :/

Anyhow, so Beaker seems firmly experimental, but that means
I needed an actual browser to do what I wanted. At first I
turned to Firefox and Firefox developer, but after reading
through the
[notable events section](https://en.wikipedia.org/wiki/Mozilla_Corporation#Notable_events)
on the Mozilla Corp. Wikipedia page, I had a bad taste in my
mouth. Though Mozilla Foundation is technically open source,
Mozilla Corp. seemed to be doing more harm to its employees
in the name of revenue than even many for-profit companies.
So I turned towards Firefox alternatives like
[IceCat](https://www.gnu.org/software/gnuzilla/), but after
reading LibreJS was a little hesitant.

My chosen browser as of now is Vivaldi, which seems
relatively open-source, has good privacy protection, and is
ridiculously customizable. They seem up-front about how they
support themselves (search engine sponsorships, except
notably through Google where they make no money).
