---
created: 2022-02-26
modified: [2022-02-26]
title: Smooth like butter
tags: [workflow]
author: Nathan
---

I've realized that the key to a good workflow is something
that's like butter -- it's present, but it (especailly over
time)is so smooth it escapes your notice eventually

if it's a physical action, like in vim doing colon+w or
constantly pressing esc, then it should enter the back of
your mind so you dont have to think about it

good workflow

- fast refresh in vite
- vim motions
- CI/CD (netlify is great for me here)
- extra monitor
- cargo tooling for bugs
- fast connections if ssh or rdp is needed

bad workflow

- psid trajectory
- having to switch tabs or windows

especially when you're new to something, the thing that's on
your mind the most are not the theoretically important
things (the logic of your code, the results of an analyses,
etc) but the things that take up more time and brain space
-- how to log into the enclave, why it takes so long to drag
something, how to get rid of a compiler error.

Even with a bad workflow, over time these are made better
because we stop expending brain energy to solve the same
tasks. I stop thinking about how to log in, i just do it.
But especially at the beginning it's so annoying and
brain/time-consuming
