---
created: 2022-08-08
modified: [2022-08-08]
title: The chainsaw man trailers are so great !
tags: [media]
author: Nathan
---

wow the new chainsaw man trailer looks so great
[youtube link](https://www.youtube.com/watch?v=jk7QSGwupPA)

the first one was so good too
[another link](https://www.youtube.com/watch?v=q15CRdE5Bv0)

They have in common the contrast of soft, eerie music, with
loud, chaotic, drums and bass-centered music. The more
recent (aug 5) trailer has voices too, voices which are so
soft to me. I guess it makes sense for Makima and Aki to be
that way, but even Denji and Power are quite muted. I think
that's the theme of the trailers so far, to try to act like
they're hiding something away from you, that they have so
much beneath some external veneer.

The art style is...so good...it's so ....smooth??? They have
so many frames, they spend so much time on tweening movement
and avoid the jump-based animations of other shows

They also spend a lot of time making it similarly eerie and
mysterious through the visual aesthetics here too, from the
long, long outfits that makima and co. wear (I feel it's not
as pronounced in the manga), and the color of Makima and
Power's eyes gaze right into your soul. Even if you had
never seen the show, you may be able to guess that Makima is
the villain

The other thing that struck my mind was how gory the
trailers are... I guess the manga is gory too, but the
animators spent time in drawing, coloring, tweening each
speck of blood so that it's so unsettling overall
