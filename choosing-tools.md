---
created: 2022-01-24
modified: [2022-01-24]
title: Choosing better tools for a better world
tags: [workflow, big_tech]
author: Nathan
---

**We should choose better tools because we want to make a
better world.** If we want the old world to die and we are
part of the new world strugglign to be born, we should
abandon the tools built for the old world. The master's
tools cannot dismantle the master's house, so we should
choose tools that can do so.

I usually hate vague one-liners like these, so here are some
qualifiers:

1. Use free and open-source software. This is really, really
   easy to do
2. Use software developed in a lineage towards liberation
   (usually this means sustainable software). React and
   Svelte are both open-source; choose Svelte, as it was
   made independently and supports a more sustainable web.
3. Avoid choice paralysis and try to make choices just once.
   A common feature of Linux is distro-hopping, and locking
   yourself up in one choice after another. But in my view
   this detracts from the true point of tools: to build
   things. We should choose reliable and ethical tools once,
   and focus on building things after.

And some more practical thinking:

1. Choose software maintained by people (hopefully a group,
   rather than a one-person project)
2. Choose software with a smaller footprint. Svelte! Not
   React. Don't install MUI.
3.
