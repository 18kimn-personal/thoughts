---
created: 2022-01-13
modified: [2022-01-13]
title: Self-documenting code
author: Nathan
---

A popular topic in self-documenting code: instead of writing
comments that can just add more text, it's even better to
write code that doucments itself. Through function and
variable names, through good usage of indentation and
conditional statements, etc., logic can be immediately
viewable and sometimes even to non-coders.

One thing I realized is that code itself is documentation.
It's docuemntation of the chaotic logic of our brains in
terms of the logic of a computer (an interpreter, or
compiler) and a certain standard that some committee made
up. We often need additional documentation through from the
logic fo these standards to the logic of humans (english,
korean, spanish, chinese, etc.).

Of course, these sets of documentation often serve different
purposes. Code, even self-documenting code, cannot express
the why or rationale behind the logic of a programmer; that
needs text too.
