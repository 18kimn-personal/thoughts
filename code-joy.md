---
created: 2022-02-14
modified: [2022-02-14]
title: Finding joy in code
tags: [workflow, sustainability]
author: Nathan
---

_Rust is such a joy to code in._ _Svelte is such a joy to
code in_. Et cetera.

I feel this somewhat, for Svelte (coming from React), and I
bet people coming from C++ and C feel this way about Rust.

I feel the opposite of this when working in the PSID
enclave. The graphics are terrible, I don't have network
access to install packages, the environment breaks often
because of limited memory and large datasets, the provided R
distribution lacks packages and is outdated, the data itself
is structured so inconsistently and our variable
requirements so many that it is difficult (impossible) to
assemmble it cleanly -- with clean code (DRY, no hacks, no
mistakes) and clean format (a single dataframe produced).
