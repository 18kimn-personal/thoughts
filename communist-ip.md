Software should be free, accessible to all, open-sourced and
extensible. It should be based on standards that are
extensible and portable.

This is... what a lot of open-source software and free
software movements can agree on. My communist views are that
software should be free because we should not be locked into
dependence on corporations and governments, becoming both
consumer and producer with no escape from either
relationship, and that is only possible if we are free to
create and extend our own tools.

[The hippocratic license](https://firstdonoharm.dev/)

[dotCommunist Manifesto](http://moglen.law.columbia.edu/publications/dcm.html#foot17)
