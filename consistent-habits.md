---
created: 2022-07-29
modified: [2022-07-29]
title: Trying to make consistent habits
tags: [workflow, sustainability]
author: Nathan
---

I want to do

- anki cards
- note taking

on a regular basis. It's not that im entirely unmotivated,
e.g. I do it if I htink of it and I have time, and I feel a
genuine want to do it, but too often I don't have time, or
if I have time I don't think of it because my mind just
wants to play Hades (the nintendo switch game) for a little
bit before the Next Activity.

So I guess some intentional choices, to carve out time for
myself, would help here. More time for myself would help
here (e.g. opposed to traveling around korea with the fam).
It is wha tit is
