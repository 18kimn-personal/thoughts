---
created: 2022-02-14
modified: [2022-02-14]
title: Jonathan crary's scorched earth
tags: [reading]
author: Nathan
---

Just read a pretty positive
[review](https://www.theoryculturesociety.org/blog/review-jonathan-crary-scorched-earth)
of jonathan crary's _Scorched Earth: Beyond the Digital Age
to a Post-Capitalist World_.

Crary seems pretty pessimistic, that a better future is an
offline future, and that the internet from its beginning was
a structure for the dissolution of society, that it began
for military use and research, and that the internet will
drive us towards "unhealthy habits and subjectivities of
digital consumption" (in the words of the reviewer).

That caught me off guard a bit, since my usual understanding
of the internet is that it links and connects (often for the
purposes of generating capital for a select few) instead of
disperses and decentralizes, and also because for Crary
there doesn't seem to be any hope in the internet (the only
possibility is offline).

In any case, it's on my reading list
