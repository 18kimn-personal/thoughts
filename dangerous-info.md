---
created: 2022-02-19
modified: [2022-02-19]
title: Some web database projects are bad
tags: [capital, web_dev]
author: Nathan
---

This site was posted on Hacker News today:

> https://www.judyrecords.com

I don't really know about law research... my first
impression of this was that it was a cool project, sort of
like Evictorbook in being able to search a diverse est of
public databases together

This tool does somethign that ours doesn't though, in being
incredibly comprehensive, and not having a clear audience
from the outset. One HN commentor writeS:

> Cool, I guess. I'm not really a fan of anything that can
> be used to further persecute people who have already paid
> their debts to society. I know this is public information,
> but the information itself has been relatively opaque for
> generations.
>
> Beyond mild curiosity, the only paying customers for a
> service like this will be groups like employers, schools,
> creditors, landlords etc. It removes the pain around paid
> background checks, and it includes data that was already
> legally expunged.
>
> The fact that it includes data that was legally expunged
> actually makes this service more valuable than traditional
> background checks to certain types of people.

Sigh. That doesn't sound so good.

My takeaway ... the fairly obvious point that these sorts of
projects need to be principled and to fulfill specific
purposes, and without a clear orientation (by being treated
just as a cool tech thing, as the monospaced and minimalist
aesthetic of the site might suggest) they can more easily
harm than help
