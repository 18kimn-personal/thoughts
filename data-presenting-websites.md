---
created: 2022-08-08
modified: [2022-08-08]
title: Data-centered websites for all of us
tags: [webdev]
author: Nathan
---

I'm thinking of writing a post on practical guidelines for
more usable and equitable data viz/presentation/exploration
apps. The kind that academics, journalists, artists,
independent people make, aka ones where it isn't feasible to
have complicated server architecture, but still more complex
than a "fetch an HTML page" type fo static site. It's for
people who are telling a story with data. I won't be
thinking about sites with unique content for each visitor,
like social media sites.

Anyhow, here are some basic ones:

- 1. Be verbose about what's happening -- data fetch,
     initial library load, computing anything, ...
     - progress bars
  - and clear, but efforts to be clear is sort of a given to
    me, whereas sometimes we want to be concise or not
    verbose. I think being verbose with data viz stuff is
    good.
- 2. stream your data
  - ndjson, csv file formats
  - file and directory structure
  - server-sent events or websockets
- 3. Balance work on the server and client
  - E.g. do visualization on the client, data processing on
    the server (to reduce what needs to be sent)
- 4. Preprocess your data (this should be higher priority)
  - This is kind of a given and not really a single "trick,"
    as the others are but I'm including it because it's
    still important to think about. Having lots of computing
    work that could happen when a user visits your site
    means time the user spends waiting to see the results.
    Doing it ahead of time means a faster response.
  - examples...? This is kind of the same as #2, a bit.
  - Eliminate unnecessary variables, simplify your shapes if
    you are working with geographic objects or prepare a
    progressive loading scheme, save to formats like
    TopoJSON to help with compression
  - Precompute models (if they are of a finite size)
- 5. Spend time on the design of things to give the user
     additional content alongside the data. (this should be
     further up)

I feel like the above tips deserve priority, but general web
performance best practices also apply here too. Here are
some:

- 5. Care about your bundle size
  - not actually the limiting factor in most of these
- active memory cleanup
- shift work off of the main thread
  - your interactions
- honorary mentions
  - serverless / lambda functions / Cloudflare Workers
    - netlfiy functions, vercel, deno deploy
    - Wary of such services as you can quite easily
      accumulate costs and become locked into architecture.
      Some of these platforms I still consider immature or
      unstable: existing services may change so that you may
      be forced to rewrite code, and even if existing
      services stay the same, new APIs and functions are
      being added at a breakneck pace, such that it's easy
      for your codebase to become "outdated" and for
      additional pressure to "modernize your codebase" to
      pile up.
    - But still, websites like these are a prime opportunity
      to use services like these. These kinds of websites
      mostly can be operated as a static site (meaning
      without a designated server), in that they usually
      don't require user-specific content like social media
      sites would, or handle things like e-commerce, or have
      a seemingly infinite store of content like youtube.
      But there are definitely situations where a server
      would be nice, for example handling a modeling task
      for a user or doing a periodic data fetch. For these
      situations, "serverless" architectures may be a great
      fit, in that they can provide a much simpler interface
      for executing needed functions, and don't require
      either dedicated hardware or too much command-line and
      server architecture knowledge.
