---
created: 2022-02-12
modified: [2022-02-12]
title: deno ?
tags: [workflow, sustainability]
author: Nathan
---

Node

- just feels really, really solid
- core is stable, established, and a lot of contributors
- huge ecosystem of modules

Deno

- web-aligned (relies on a stable platform)
- secure
- built-ins for node third-parties (webpack, code coverage,
  testing, linting)
  - but tbh some of these third party things are more
    powerful
- doesn't rely on a central repo (npm)
  - although tbh it does on unpkg and other stuff
- interfaces nicely with typescript and rust

So should I switch? Which is more sustainable? Node is more
stable, becuase it is so big already, but is that just me
being hesitant about makign the switch to an otherwise more
ethical and sustainable option?

I'm guessing most enterprise companies do not benefit _at
all_ from the features Deno offers. It seems that while Deno
is an objective improvement, inertia and the fact that Node
was also great won't make teams move.

Okay, I've resolved to make the move. Most of the reasons
that the move should not be made (learning new things, the
fact that the ecosystem isn't there yet) are invalid to me.

---

fuck i dont know anymore

like I don't want to just reach for every shiny new thing. I
don't liek that paradigm of "move fast and break things."
Deno is a better way to do JavaScript. So it should be
adopted.
