---
created: 2022-04-22
modified: [2022-04-22]
title: depression and memory
tags: [personal]
author: Nathan
---

I have this intution that we remember things based
on...vibes. That's the best language I got, it's not exactly
feelings, It's not exactly the specific experience...our
memories are an amorphous mess of emotion,

Anyhow the reason I'm writing is that there's this idea (i
think actually backed by science) that depression causes
memory loss

A flip side of it is easier for me to understand, though.
When I'm depressed I can't feel things. When I can't feel
things I can't remember them. Everything blends together
into one mess.

I can't remember things even now, now that I am playing
every day, now that I feel loved, now that I am enjoying
freedom and physical and social distance away from many of
my responsibilities. I wonder if I can feel things now, if I
am cold and dead inside, if I am just depressed, or the
opposite --if I am immature and happy enough that all I feel
is just a constant positive and I am unable to distinguish
one idea from another.
