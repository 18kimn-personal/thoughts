---
created: 2022-04-08
modified: [2022-04-08]
title: Disabling sleep timeout
tags: [linux_tips]
author: Nathan
---

```sh
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target
```
