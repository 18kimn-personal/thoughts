---
created: 2022-01-06
modified: [2022-01-06, 2022-01-22, 2022-01-23]
title: how to use encfs
tags: [linux]
author: Nathan
---

Began encrypting my personal files today. Here's a summary
for myself of how to use encfs:

- to make a new vault:

```sh
encfs encrypted decrypted
```

Both directories should start out empty. Both have to be
specified with absolute paths.^[1] When you drop things in
decrypted, encrypted automatically updates. It will have
modification times and have the same directory structure as
the unencrypted variant, but the actual filenames and
contents will ahve been decrypted.

After you drop things in them, unmount the decrypted version
with:

```
fusermount -u decrypted
```

Trying to see inside decrypted after this shows nothing. To
remove even the filename from being seen at all, you can run
`rm -r decrypted`.

Side note: after all this time I still don't exactly know
what unmounting and mounting is??

When you want to add more things or otherwise edit the
contents of the encrypted folder, you can run:

```
encfs encrypted decrypted
```

Once more. The decrypted destination directory doesn't have
to be the same name or location as the first.

Then unmount, rm -r, and go on with your life. :)

[1]: Actually, you can use relative paths with the `-f`
option. encfs wants absoltue paths because the automatic
update of the vault happens in the background via a daemon,
so it needs to run independent of where the user currently
is (although couldn't file resolution have been programmed
into the utility?). The `-f` option specifies it not to
update in teh background, so I believe it also won't update
the encrypted vault when the decrypted folder is changed.
That could cause some trouble too I'm guessing. TLDR: just
use absolute paths.
