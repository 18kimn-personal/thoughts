---
created: 2022-02-11
modified: [2022-02-11]
title: Epistemelogical breaks
tags: [standards]
author: Nathan
---

Harvard professor john cormaroff sexually harrassed (at
least) three people, and thirty-eight professors signed on
to a letter supporting him. There's a lot more to be said on
that, a sample is below:

- one student's therapist gave their therapy notes to
  Harvard which in turn gave them to cormaroff
- many of the professors signed a retraction, which is
  probably one of the most pathetic letters ive read in a
  while and on par with their first letter
  https://www.thecrimson.com/article/2022/2/10/comaroff-faculty-letter-retraction/
- this doesn't just happen at harvard; this happens
  everywhere

This triggered a thought I've been having for a while, which
is that the academy has atrophied into a big-brained
cockfight that is full of sex pests, harassers, abusers, and
nepotistism's nonsense. I thought of this with Foucault
(pedo), but this also applies to white professors that
[fake their own race](https://www.washingtonexaminer.com/news/metoostem-founder-admits-she-created-phony-twitter-account-depicting-native-american-geology-professor-who-died-of-covid-19),
professors that support us interventionism, writers from the
1800s that advocated for colonial takeover, ...

In any case, I feel we need a large, sustained
epistemelogical break from the old. A refusal to cite or
refer to the old world, and a wide-scoped effort to redo and
rebuild a new world.

Most discussions I've seen about this are a lot more
cautious. Perhaps we should critically re-explore and
revalue the work those "problematic" scholars have done. To
pretend that we can simply start anew is disingenuous, since
history and knowledge are always dependent on the past
whether we like it or not. Plus, lots of scholars can't be
picky about who we cite or not; people want a job and then
tenure after that, and more generally these professors we
want to break from necessarily have a huge and dominant
presence in the academy anyhow.

I just disagree. We need to break from them. I don't care
that it's impossible, or impractical, or dangerous. It has
to happen, otherwise the academy can only become worse. I
want to communicate that desperation to others... that we
need a new scholarship, a sustained fight against the old,
and transparently so. This break needs to be the floor, a
prerequisite, for all future worlds to be built.
