---
created: 2022-01-06
modified: [2022-01-06, 2022-01-22]
title: Expert components
tags: [workflow, webdev]
author: Nathan
---

A quick map to becoming an expert in components, or things
you should be able to do with them, rated from beginner to
really expert:

- Make a component
- Style a component
- Add reactivity to a component
- Add interactivity to a component (transitions)
- Nest components
- Use stores well
- Make a recursive component
-
