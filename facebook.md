---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: Facebook and the metaverse
tags: [big_tech]
author: Nathan
---

discussed in ART750

Why did Facebook become the metaverse?

The answer is accumulation by dispossession. Facebook
(especially outside of the US) is not just a social media
company, although it started as such; it literally is the
internet in a bunch of places. It makes money by getting
people to use the internet in general, becuase it has such a
monopoly over the internet.

When it profits, it needs to reinvest; revinest means it
needs to find a way to do so; that often means finding a new
avenue of investment. Facebook has exhausted or reached the
limit of current avenues, and it wants to expand to the
"operating system" level -- not just social media, but in
some sense our social interactions in general.
