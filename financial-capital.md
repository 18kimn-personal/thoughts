---
created: 2022-02-12
modified: [2022-02-12, 2022-03-14]
title: BNP and Biden's executive order
tags: []
author: Nathan
---

(this was originally a tweet thread)

in 2014, the US sanctioned the french bank BNP Paribas for
$8.9 billion for trading with Iran, Sudan, and Cuba

the US told BNPP that it had to either stop dealing in USD
(over which the US has sovereign power) or pay $8.9B

it couldn't stop dealing in USD, because that would mean
being unable to have transactions with any other bank in the
world (as the entire world uses USD) and would fail

so BNPP, a french bank, paid up, and stopped dealing with
Iran, Sudan, and Cuba

(aside: the global reliance on the USD used to be codified
as the dollar-gold standard by the 1945 bretton woods
conference, but that ended in 1973. Now banks just do it
because that's what they did before and the alternative is
often to just fail as a bank)

I'm reminded of this after biden signed the executive order
today freezing $7B from the Afghanistan central bank. He's
able to freeze that money because it's held in US
institutions, which the world has been forced to see as the
most stable option and thus entrust their assets in

The difference between BNPP and Biden's order today is that
Biden strikes a country already under famine and a massive
economic crisis, putting millions in danger. I'm not going
to try to compare BNPP here; Biden's order is so much more
an immediate herald of death

The similarity is that through no designated international
law, the US under the directive of Democrat presidents is
able to do what it wants with other countries' money since
US financial capital rules the world. All countries are
affected, and the Global South is targeted

This to me is essentially imperialism today. A hundred years
ago empire was sovereign; today it is informal, expressed
through the state of perpetual war, the undisputed hegemony
of US financial capital, both the logic and sheer size of
which atrophies and metastasizes constantly

I do have some hope, for example through the belt and road
initiative and banks in Asia in particular using a basket of
currencies instead of just the USD.

But mostly I think we're just fucked.
