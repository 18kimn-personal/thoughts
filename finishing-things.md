---
created: 2022-07-2
modified: [2022-07-29]
title: Finishing things part 2 -- irresolute resolutions
tags: [workflow, sustainability]
author: Nathan
---

I've always believed in my head that perfect is the enemy of
good, e.g.
[some random thoughts on the matter and vim](./vim-wishes.md)
and [finishing things part 1](./working-with-others.md)
Recently this has come to mind with a few things.

## yale detour

Nobody had been responding to me, perhaps becuase they are
busy in their own lives, perhaps there are other reasons,
but I felt a little bit of an ass to always be pestering
about pushing out the project for release. I hadn't even
talked ot most of them for anything else
not-project-related, e.g. I don't know where any of them are
geographically. But still I want to just put this down.

So I made the difficult choice to stop working on it. There
are a few features that could be implemented, like a
subtitle and picture-based preview for the mobile version,
but I won't. I even want to, it's enjoyable and fun to code
on this and I feel itches I could scratch, and oh would it
feel good to scratch them, but I won't. Resty indicated that
he would like to lead the next stage of the project, which
is great, and so I moved to add his name to the contact
section. I decided while doing so to take my name off. I'm
done here.

## evictorbook and BA4A

I work with a group called AEMP on a landlord lookup tool
called evictorbook. The project is all-volunteer, and this
among other things has caused delays among a contracted
partnership with a group called BA4A that is funding us to
build out the tool for oakland. But we are now at a stage
where we would like to just be finished.

## misc.

- followed up with emma zang
- making steady progress w/ alex hanna
- finished up high theory internship formally; have some
  deadlines (making steady progress) for podcast audio
  editing
- still need to add on features for datahaven

I'm still caught in the stage of "need to finish instead of
starting and half-assing" for myself, but I feel that I'm in
a better place now. Time helps.
