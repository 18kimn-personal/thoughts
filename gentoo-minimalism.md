---
created: 2022-03-08
modified: [2022-03-08, 2022-03-09]
title: Gentoo is definitely not minimalist
tags: [workflow, linux]
author: Nathan
---

I first had this impression (and I'm sure others have had
this too) of Gentoo being hellbent on minimalism. USE flags
and the compilation step in general lets you turn on
features that are by default in gentoo, but they're on by
default in other distributions that don't have this same USE
flag / compilation process. In that way gentoo tries to push
you towards selecting only the features that you'll use,
although it provides quite a few ways out of this (e.g.
`genkernel all` or distribution kernels, available binaries
of other packages, etc). These features help your computer
(in theory) cut down on memory, CPU usage, and disk space
too

But after a few days of setup the impression im left with is
that this is not a minimal distribution lmao. I always knew
the performance optimizations were going to be pretty much
nothing, but i underestimated the sort of negative downsides
of the gentoo process too. Just now when installing
qtwebengine (in the process of installing RStudio) I saw a
premerge check for at least 14GiB RAM and 7GiB disk space,
which sseems like an honestly ridiculous amount. I feel my
computer fan, vivaldi, and `top` all indicating signs taht
my computer is working really hard. The architecture of
portage and portage overlays is also complex compared ot the
very simple system of Arch + AUR (granted this isn't
complicated or a mess, this is just not "minimal").

I know that these things will disappear from my mind after a
day or so, when I really have everything I use installed
again, but yeah. Gentoo seems like a lot. I definitely
wouldn't recommend this for the average user and to be
honest I don't know if I really want to be compiling
everythign forever into the future (perhaps I will lean more
into my poser humanist sides rather than my poser
technologist sides). I don't know if it aligns with my
ideals of how we build better things, or if it really makes
me more intimate with my computer.

I suppose that like so many thoughts this one i hhould just
stop thinking and just carry on with life :')

---

short addendum: i forgot that the compilation process also
requires you to install way more dependencies in the process
that you often don't want to remove in case you have to
compile more things
