---
created: 2022-03-08
modified: [2022-03-08, 2022-03-09]
title: gentoo
tags: [linux, workflow]
author: Nathan
---

I'm using gentoo now :')

it's been a difficult process just as arch was in the
beginning, but i have a feeling that it won't get much
better from here as arch did because the AUR isn't available
to gentoo

and I don't know how much ill feel the benefit of USE flags;
from what I've seen so far they seem to be useful for
cutting away from features available in mainstream versions
of software rather than for adding new features. I guess
that's sort of the philosophy behind gentoo, that you can
get just what you need by compiling it yourself

some postinstall notes for myself:

- iw is ok, switch to iwd for systemd integration and a
  better interface
- for pulseaudio support make sure to have sof-firmware
  installed
-
