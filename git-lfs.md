---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: Git LFS
tags: [storage]
author: Nathan
---

continuation of [[obsidian vaults and git]]

For these humanities projects that have media, I tried to
convert them into a `.tar.gz` archive. But this actually
didn't work; I believe the PDFs format might already by
"compressed" in some way. As a temporary solution, I stored
all of my medium-to-large size files inside `readings/`
folders in various places, and added the `readings` pattern
to my `.gitignore` file.

But I need a real way to store and access data through Git.
Not just a solution for humanities PDFs, but one that can
comprehensively cover the other sources of data I have too
-- my maps from HIST467, rendered LaTeX documents, large
datasets that I use for applied spatial stats, and so on.

One popular method is through Git LFS, or Git large file
storage. This method stores pointers to files instead of
files themselves in a Git repository, and when the git lfs
extension tool sees this pointer it can grab the correct
file.

Some criticisms are that:

1. this still limits my file size to 2Gb
2. this ties my data storage to Github since Github is the
   only hosting platform that uses Git LFS
3. Most of the time I don't have an issue with a single
   large file, but of many large files at once -- like
   readings, stored as PDFs. Using git lfs over and over
   will get to be a pain, I predict.

Git LFS still seems like my best solution so far but I'm
going to keep looking to see if there's anything else I can
use.

[Microsoft Azure team's strategies for Git](https://github.com/MicrosoftDocs/azure-devops-docs/blob/master/docs/repos/git/manage-large-files.md)
