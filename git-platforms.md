---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: Git platforms and their stances
tags: [git, storage, tech]
author: Nathan
---

I think git is really, really useful and is almost a
necessary feature for managing open-sourced projects, as the
predominant version control and file history tool.

A problem I have is that hosting platforms are limited by
file size. Github limits repository sizes to 2Gb and Gitlab
limits repository sizes to 10Gb. This is not a problem for
many, many code projects, because plain text files take up
very little space. Even if you and dozens of collaborators
make hundreds of files of code modules, these would take up
very, very little space. A character is a byte (pretty much)
and a single file might take up a few kilobytes, and to
exceed github's file size limit would thus take on the order
of a million files.

Becuase Github and Gitlab provide file size restrictions
partly with this justification -- that they are version
control hosting platforms and not storage platforms, and for
data and media you should find a storage platform instead of
version control hosts.^[of course, they could if they really
wanted to, but they don't because it would be
financially/logistically difficult for them as companies.]
