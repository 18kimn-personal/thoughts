---
created: 2022-02-07
modified: [2022-02-07]
title: Good landing pages, personal websites, and blogs
tags: [web_dev]
author: Nathan
---

<!-- prettier-ignore-start -->
Jenny Bryan https://jennybryan.org/about/
Yihui Xie https://yihui.org/
<!-- prettier-ignore-end -->
