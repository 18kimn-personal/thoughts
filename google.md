---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22, 2022-02-14]
title: On Google and AMPs
tags: [big_tech]
author: Nathan
---

https://en.wikipedia.org/wiki/Accelerated_Mobile_Pages

https://www.reddit.com/r/webdev/comments/r0pnfc/amp_and_google_anti_trust_lawsuit/

[## Google’s mobile web dominance raises competition eyebrows](https://www.politico.eu/article/google-amp-accelerated-mobile-pages-competition-antitrust-margrethe-vestager-mobile-android/)

An example of "we actually need more than just open source".
AMP is an open-source HTML framework made by google to make
mobile phones load faster, but apparently they add an
artificial 1-second delay to non-AMP pages.

Publishers can pick the largest bidder using JavaScript in
the header section of a page ("header bidding"), but this
would circumvent Google's ad services. This is disabled in
AMP (which I believe doesn't allow JavaScript).

I don't think the current tide of the web favors AMP;
twitter announced they're moving away from it, Google took
it off of their performance criteria (I forgot the details
lol). In my view htis isn't actually because of ethical
objections to ads handling and Googloply, but because of the
trend towards "one javascript website works anywhere"
paradigm behind responsive web pages / PWAs / React Native /
Flutter / Electron / and so on.
