---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: Complaints about grad school
tags: [trajectory]
author: Nathan
---

Someone on twitter was complaining about people who go to
grad school and then complain about grad school or tell
others not to go to grad school

I sympathize a lot with them... despite being massively
underpaid I still feel that graduate students are a
privileged class and that is why people wnat to go to grad
school... I also feel like academia is a unique field in
that people's jobs are to complain (or ~critique~) their
industry and workplace, which feels so disingenuous,
unproductive, fake,...

I recognize the opposing argument that grad students and
contingent faculty are workers that deserve protections too,
and that complaints are valid even if we chose to go into
the job we are in
