---
created: 2022-02-12
modified: [2022-02-12]
title:
  When all you have is a hammer, everything looks like a
  nail
tags: [workflow]
author: Nathan
---

JavaScript/nodejs

- web dev
- basic fetches
- basic server

Rust

- server in performance considerations

R

- data

Python

- text
- machine learning / ai
