---
title: On hardware and open-sourceness
tags: [big_tech]
date: 2021-11-27
---

I think hardware shows a good scenario why I'm a
Marxist-Leninist and not an anarchist, although again I
don't think there's much of a distinction between the two.
Maybe the distinction is a point that doens't need to be
made.

In any case, hardware. Free software and open source
software are not that easy to make, but much easier to make
in my opinion than trying to build computers from literal
scratch. For open source software, you can begin with tools
other people made in their basement and make a tool that you
yourself made in your basement, without paying a single
sent, without traversing over a single closed-source
license. The actual logistics might get wonky there as
projects grow bigger, but yes, it is possible to do
something completely on your own and completely freely.

The same cannot be said of most pieces of hardware in my
view, and especially for things like CPUs where factories
today literally cost
[$17B](https://www.npr.org/2021/11/24/1058770506/samsung-says-it-will-build-17b-chip-factory-in-texas)
to build. How would I construct transistors that are smaller
than a micron by myself? How would anyone, except with
insanely costly machines? Even for higher-up tasks, where I
buy all equipment and assemble it myself (which is honestly
what a lot of free software is like, so not a cop-out at
all), there is still that requirement of _buying things_.
With software, the only thing you need is time (given most
of us begin with a laptop even before we become open source
folks); with hardware, you need material goods, and for that
you need money.

Because of this, I feel that the fight to wrest hardware
from gigantic for-profit manufacturers has much less hope
than the corresponding movement of free software and open
source. Linux has already won in the server world, NPM and
JavaScript libraries in general has won in the
web-development world, R/Python/Julia have won over ancient
proprietary things like Stata/SASS/SPSS in statistical
computing, and down to systmes-level architecture there is
less of a need for external libraries and still an abundance
of free ones. But it's conceptually impossible to say the
same about free hardware. The best we could ask of hardware
is that they are made ethically, for the user in mind, and
prevent a relationship where the corporation is depended on
forever. [Framework laptops](https://frame.work/) and
[Fairphone](https://www.fairphone.com/en/) come to mind as
examples of companies that do this. But to win this on an
industry-wide scale seems impossible to me except with
government intervention and a culture of revolutionary tech,
neither of which are really at hand right now.

---

update 2022-01-12: apparently open-source tools in this
space are indeed a thing, see
https://www.youtube.com/watch?v=OmEbzRp_NGg

the US invested 100million for open source semiconductor
design
