---
created: 2022-01-14
modified: [2022-01-14, 2022-01-22, 2022-01-26]
title: On asking questions and helping others
tags: [sustainability]
author: Nathan
---

One thing I've become particularly annoyed at is the Arch
community's approach to helping others. There are lots of
threads where this si discussed, the one that made me write
this note being
[this one](https://bbs.archlinux.org/viewtopic.php?id=207794).

This thread links to
[a blog post on 'help vampires'](https://slash7.com/2006/12/22/vampires/),
and guidelines that the Arch wiki community hosts. The
perspective is that there is a right way to ask questions
and a wrong way, and asking the wrong way is disrespectful
and can waste time. Asking the wrong way would include being
overly vague, not looking at resources in advance, and so
on. Here's a few from the slash7 post:

> - Does he think helping him must be the high point of your
>   day?
> - Does he get offensive, as if you need to prove to him
>   why he should use Ruby on Rails?
> - Is he obviously just waiting for some poor,
>   well-intentioned person to do all his thinking for him?

Sigh. This just makes me so sad. The language here
especially just ticks me off; why is the helper described
sympathetically (if half-jokingly) as a poor,
well-intentioned person, but no such (clearly subjective)
sympathy is shown for the person who is asking for help?

The person in the Arch forum thread asked a vague question;
they wanted to build a custom OS and didn't know how to
start. Clearly the forum has a lot to offer him even if he's
being vague; it's one thing to be vague in saying "I have a
problem in trying to work with strings. Can anyone help me?"
because it doesn't offer any clues on how to help. It is
another to not know where to start and openly acknowledge
that, and clearly be looking for vague and general pointers
and answers. The Arch community rose up against this; it's
oriented around asking very specific questions that have
very specific answers.

I don't know. The man pages and the Archwiki do have answers
a lot of the time, but a lot of the time they don't work or
they're very difficult to read for a newcomer. Sometimes
just general discussion and guidance is more helpful than a
specific answer to a specific problem. If a community only
respects the latter, then every other thing that builds a
community -- the informal exchanges, the thought processes
that occur between questions, the meta-analysis and
reflections on how a process occurs -- are lost, and people
are left to figure those bits out for themselves and only
turn to the community when they have a discrete and
contained problem.

The current culture and implicit guidelines seem to be meant
to build a particular relationship between helpers and those
who are helped, where people being helped should be
respectful and courteous, and it is frowned upon if they are
not, but it is totally okay to be curt or rude to people in
return.^[I say implicit because the Arch guideliens and
others do ahve clauses on being respectful and helpful, but
clearly they apply to some (the people asking questions) and
not others (the people offering help).]

I don't care if someone asks me a vague question. If someone
asks me about Debian instead of Arch, or otherwise in an
area I don't have expertise on, I don't care, I will try to
help but if I cannot then that is also fine. If the answer
is already accessible to the person but they have not yet
reached for it (e.g. they didn't Google or search through
the forum), that's a slight inconvenience to repeat myself
or someone else

If I can help, then that is good, and if I cannot help, then
that can also be acknowledged and expressed with grace, not
through admonishing the asker because I cannot help them.

Fundamentally, I think we have a responsibility to help
others much more than those who need help have a
responsibility to be respectful or anything like that. Bad
answers are much worse than "bad" questions, because bad
answers can discourage new users and the people that
actually need help from participating further in these
fields, whereas bad questions (bad only in the sense that
the Archlinux community forums considers) have no personal
harm to any potential answerers. And the linux community
doesn't really think the same.

last thought: from my own experience it can take an hour to
search for something on the internet, even if the answer is
technically accessible (really) but take less than a minute
for an experienced person to respond.

Related (a good set of opinions):
[The tyrannical mods of StackOverflow](https://www.youtube.com/watch?v=IbDAmvUwo5c)

And from
[Better-Bibtex for Zotero](https://retorque.re/zotero-better-bibtex/support/):

> Before all else, thank you for taking the time for
> submitting an issue, and I’m sorry that I’ve probably
> interrupted your flow.

Better Bibtex proposes guidelines and rules for posting
issues, and state that "my time is extremely limited for a
number of very great reasons... Because of this, I cannot
accept bugreports or support requests on anything but the
latest verson" presumably to preserve their sanity. But they
are gracious about it, as seen in the quote above. The rules
are also prefaced with "for the fastest fix" and after that
first quote they go on to say "your report matters to me,"
like the guidelines are not meant to enforce some social
standard but in order to pragmatically provide a solution.

This fills me with a warm feeling.

The ideal perspective:

> The guidelines are very detailed, perhaps to the point of
> being off-putting, but please do not fret; these
> guidelines simply express my ideal bug submission. I of
> course prefer very clearly documented issue reports over
> fuzzy ones, but I prefer fuzzy ones over missed ones.

---

another reddit comment that ticked me off a bit:

> How in the fucking world are we supposed to know when you
> don't even tell us what commands you are running

[here](https://www.reddit.com/r/linuxquestions/comments/74r2yd/comment/do18029/?utm_source=share&utm_medium=web2x&context=3)

sigh. who cares. why are you so angry. why does a bad
question have to make you mad. please stop.
