---
created: 2022-08-12
modified: [2022-08-12]
title:
  Things nobody told me would be troublesome about home
  servers
tags: [web_dev]
author: Nathan
---

- By far the most expensive thing are disks
- IP addressing is actually kind of a pain in the butt to
  figure out correctly
- ssh robots
- firewalls are also hard to figure out correctly
-
