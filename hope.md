---
created: 2022-03-14
modified: [2022-03-14]
title: regaining hope
tags: [sustainability, trajectory]
author: Nathan
---

hope often feels like one of those things that can only be
lost and never gained. We associate childhood with a sort of
innocent hope in the world, an inherent belief, that is only
broken to varying degrees as we grow older.

Gramsci famously wrote that we have to have a pessimism of
the intellect, but optimism of the will. I guess I'm
struggling to gain that last part. His phrasing and my
general experience both seem to believe that the more you
....think....,,, reflect, introspect, ex-trospect, etc., you
lose hope; there doesn't seem to be a corresponding set of
actions to gain it in a different light

A lot of people would say things like organizing or engaging
with struggle. To be hoenst I guess that's very true nad I
have no place to argue anything to the contrary given my
"organizing" has only been among uber-rich college students
or volunteering for local orgs, not sustained and meaning
relationships

Idk where I'm going with this... I'm tired, I don't have
much hope, either in my own future or in this world's
future, and I am losing motviation to invest in either very
quickly.

---

i also feel like my main perspective as of late has been to
face this head-on. To try to sit with the fact that change
is impossible but no change is even more impossible, and to
commit to a life of struggle in spite of that. This is a bit
of a hopeless perspective... or at least the hopeful answer
would be to say more simply that change is possible.

---

after rereading this i feel i have to discount some of what
i say to avoid romanticizing or arrogantly exaggerating my
current life. I do feel depressed and do not have hope, but
the majority of my days are spent in bed watching youtube or
something equally as pathetic instead of doing something
noble and good that results in my own burnout. i have no
good reason to have lost hope and motivation

---

tangentially related:
[This ironic world: Does climate hope encourage climate inaction](https://www.nakedcapitalism.com/2022/03/this-ironic-world-does-climate-hope-encourage-climate-inaction-2.html)
