---
created: 2022-01-12
modified: [2022-01-12, 2022-01-23]
title: Infrastructure as code
tags: [workflow, trajectory]
author: Nathan
---

https://youtu.be/f5jNJDaztqk?t=1139

I saw a video from wolfgang's channel about ansible, a
system where you write YAML files that document a set of
actions. The idea behind it is "infrastructure as code" --
when we move from one system to another ,there should be a
textual and machine-readable expressoin of the actions to
set up infrastructure how a different system was set up.

https://bost.ocks.org/mike/make/

This is similra to the above article ^^ by Mike Bostock that
encourages the use of the Make tool, which is usually used
for compiling executables but which he argues is useful for
documenting workflow and code.

e.g.: encoding, making textual the more fleeting actions and
structures that we don't often recognize as scripts. This is
like my previous thoughts on standards.
