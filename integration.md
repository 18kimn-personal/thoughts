---
created: 2022-03-04
modified: [2022-03-04]
title: Integration of systems
tags: [workflow, sustainability]
author: Nathan
---

I wrote a few days about wanting a better system for my
phone

[phone.md](./phone.md)

but really I think I want a better system for... integrating
everything! E.g. I had a similar thought while logging some
events into my gcal that I wished my journal's to-do list
system could integrate sync into gcal.

I also have wished that katalk could work well on my
computer so that messaging can be integrated on my computer
and phone. I should check the progress of that kakao rust
project...
[kiwitalk i believe](https://github.com/KiwiTalk/KiwiTalk).
It appears that development is ongoing but slow.
Understandable. I currently have to run it through wine, and
then everything breaks -- push notifications force an i3
split, i can't seem to turn them off from within the kakao
app (turning them off hides the notif but still causes a
split), if i start it once and clsoe it then it refuses to
start again utnil reboot

ill think of more things as time goes on... but yeah,
integration is very ergonomic and convenient
