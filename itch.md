---
created: 2022-02-08
modified: [2022-02-08]
title: "scratching the itch: sustainable workflow?"
tags: [workflow, sustainability]
author: Nathan
---

My most common pattern of working is in a style of
"scratching the itch." I have several things on my plate,
hopefully one of them is something I want to do, I am able
to do it, I enjoy doing it.

This has been fun, and if things could stay that way it
would sustainable. But unfortunately I have tasks that I
don't want to do that I need to do nonetheless. So I'm
looking for more motivation for those tasks.
