---
created: 2022-01-26
modified: [2022-01-26]
title: should i learn jquery? lmao
tags: [learning, sustainability]
author: Nathan
---

jquery is oddly still the most popular library by far, but
it's definitely not trendy by any means. Nobody is excited
by it, nobody even writes about how to use it (anymore)
since it has already become a sort of unspoken giant that
has moved past its peak trendiness

the longetivity of jquery makes me laugh a bit... it's got
nothing on svelte and is more imperative than react, but
it's still top dog because it got there first and companies
don't want to migrate and rewrite everything

is this a sustainable approach (trying to preserve something
that works) or not sustainable (refusing to repair
something)?
