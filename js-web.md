---
title: the JavaScript web license
date: 2021-11-27
modified: [2021-01-23]
tags: [big_tech, ip]
---

Richard Stallman wrote an article called
["JavaScript Trap"](http://www.gnu.org/philosophy/javascript-trap.html),
in which he states his belief that minified JavaScript
running unbeknowst to the user is dangerous, because sites
can run scripts that record user inputs and actions or
otherwise obtain personal user data. His preferred solution
is [LibreJS](http://www.gnu.org/software/librejs/), which
blocks all JavaScript that doesn't have a specific
[label](http://www.gnu.org/licenses/javascript-labels.html)
that I haven't seen before.

I really don't know how to feel about this. Well, I feel
sort of against it, but am unsure. Minified JavaScript runs
on literally every modern website, and I've never seen the
license clause they want actually in the wild. So I feel
like the LibreJS extension just locks down parts of the web
without actually putting more open software in the hands of
the users.

A gut feeling I didn't want to admit to myself is that
Stallman and the GNU approach to this seems like a relic of
the past. We need responsive and interactive web pages
today, it's cool and good to have animations and so on that
all require JavaScript. HTML-only pages seem like they come
from the 1990s. I didn't want regular pages to be disabled
because the website owner didn't know about the GNU license
tag.

Maybe I should reconsider this view. The LibreJS extension
looks pretty actively maintained, as in there were commits
[made](https://pagure.io/librejs/c/553eebc8af745551ed658f0e0493de744b2bcf0e?branch=master)
just a few days ago, so perhaps they're not just coming from
the past. I don't know :D
