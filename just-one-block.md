---
created: 2022-02-12
modified: [2022-02-12]
title: Just one block!!
tags: [random]
author: Nathan
---

https://www.youtube.com/watch?v=Iby7PLeVUBs

Tsukshima blocks Ushijima's spike -- Haikyuu

> There's no way I can beat Ushijima. But... I plan on
> stopping at least a few of his spikes.

> It was just one block. It was just one point out of 25.
> This is just a club.

> But once that moment arrives for you, that's the moment
> you'll be hooked on volleyball.

SHAAAAAAA

lmao

there are lots of things that don't matter. I've written
before here that I want my life to orient towards the things
that do matter -- things that are affecting people's
material reality, not things that we debate as a sort of
intellectual cockfight where the largest things at stake are
our egos.

But things only...matter... because we invest time and
meaning and energy into them. Doing the things that are
impossible or seemingly meaningless or both (as Tsukishima
did), and finding joy and meaning as a result.
