---
created: 2021-12-30
modified: [2021-12-30, 2022-01-22]
title: KDE
tags: [linux]
author: Nathan
---

KDE is a pain in the butt to configure sometimes, as great
as it is.

This note will serve as a sort of running list of reference
stuff:

- [config file locations](https://github.com/shalva97/kde-configuration-files).
  Panel specifically is located at
  `.config/plasma-org.kde.plasma.desktop-appletsrc`. I've
  been having some trouble where panel changes don't get
  saved on restart, especially deletions, when the changes
  are made graphically.
