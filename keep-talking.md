---
created: 2022-02-20
modified: [2022-02-20]
title: Just keep talking!
tags: [workflow, sustainability]
author: Nathan
---

Just keep talking, just keep writing!! That is one
perspective I want to encourage myself to have

I often feel discouraged by lack of ... progress, ability,
credentials, anything, in all sorts of fields. In Korean
class. when communicating with my thesis advisor. with emma
zang. with mark from datahaven., with my friends.

Just keep saying something!!! Of course useless meetings
will forever be useless, and there is something like saying
too much, but the alternative of being stuck in unproductive
stagnation, where the imposed strategy does not change at
all, is bad too! So say something, so that others can
respond to you and help, so you can vocalize your issues and
realize that they're smaller than you previously thought.
say something!
