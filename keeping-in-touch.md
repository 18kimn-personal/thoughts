---
created: 2022-02-14
modified: [2022-02-14]
title: Systems to keep in touch with others
tags: [people]
author: Nathan
---

Recently read
[a post](https://jakobgreenfeld.com/stay-in-touch)I saw on
hacker news about automated systems to keep in touchw ith
people. he referred to someone that uses a postgresql
database (! granted that database is also used for other
personal things) and that he himself uses airtable

this seems a bit cool, but... I don't know. I definitely
don't want to automate human, interpersonal interactions or
come close to that in any way.

His system just prompts him with the name and contact of a
person, and then he reaches out to them in a pretty personal
manner (no performulated message or template). But...I don't
know!! It seems creepy!!

> Most importantly, I always send the kind of messages I’d
> like to receive. They’re short, genuine, and (ideally)
> helpful. I never try to sell anything and there’s no
> agenda other than to keep in touch.

a sarcastic/playful comment thread sort of on this line, in
response to this post:

> Interesting way to approach it, my approach might be less
> technical, when someone comes to mind randomly, I just txt
> them with:
>
> > _Hey, you just popped into my mind, I hope all is well
> > with you and yours!_
>
> It's simple, lightweight, and you'd be shocked how often
> the other person pings back.
>
> My completely un-scientific view is that most people think
> of others once in a while. Perhaps we're too busy to reach
> out, or the guilt of getting out of touch makes it hard to
> push through that resistance. I just push through it.

> Exactly. Agree with this. I think we come to terms at a
> certain point that its impossible to stay in touch with
> everyone. I actually kept a table with people to stay in
> touch with them every: - month - quarter - 6 months - a
> year (if its less then you remember anyways)
>
> I quickly killed that approach, because people who
> genuinely understand overwhelm also are always happy to
> catch-up once in a while and we hold no hard feelings if
> we don't speak for another year.
>
> Serendipity and randomness wins.

> I don't think this is fake or forced at all. It's just a
> necessary counteraction to the way the modern world is
> constructed. In the past, you'd maintain all of these weak
> social connections automatically, simply by going about
> your daily life. You'd talk to your coworkers daily, the
> baker/grocer/butcher a few times a week, neighbors weekly
> at the church, other citizens monthly at a town hall, and
> so on. You didn't need a system because it just happened
> naturally.
>
> These days, most of these situations have been removed.
> Workers change jobs every couple years (and can work
> remotely), most people barely speak to their neighbors,
> grocery stores are either huge and faceless or just have
> items delivered by an anonymous gig worker, town halls
> don't really exist, etc.
>
> If you think that social connections have value (and they
> almost always do), this is the rational move.
