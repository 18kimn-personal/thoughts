---
created: 2022-02-05
modified: [2022-02-05]
title: Keyboards are like pens
tags: [workflow]
author: Nathan
---

In that both are the tactile component to writing in a
particular medium. Despite some pen and keyboard enthusiasts
claiming that they heighten workflow, they really don't;
they just make people feel slightly better about their work
by making the physical experience of expressing a thought
something that can be literally valued.
