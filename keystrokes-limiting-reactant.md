---
created: 2021-12-30
modified: [2021-12-30, 2022-01-13]
title:
author: Nathan
---

I can't remember where I heard this, but I think it was from
a talk about Vim to some programmers. Vim is a
keyboard-based terminal-based text editor that is popular
among some programming circles.

It's keyboard-based, which means that mouse controls are
limited and that the ability to do things with your keyboard
are almost unlimited. There's a million different keyboard
hotkeys and combinations you might want to memorize in order
to record and execute macros or to edit multiple lines at
once and so on. This differs greatly from even highly
performant/productive toosl like VSCode, which rely at least
a bit on mouse interactions.

I don't really see myself using Vim, for the reason that
it's sort of obtuse and I want to preserve my current
identity as
programmer-because-I-want-to-program-a-better-world, not a
reclusive tech nerd whose computer is just another extension
of their hands.

But I bring it up for a secondary reason I won't use Vim,
which is that I don't really see much value in a
keyboard-based workflow. The mindset behind (modern) Vim is
that hotkeys and macros can be just everyday unthought
behavior that we execute on reflex if we do it enough times.
But my response to this is... who cares?? I waste a few
seconds on typing out an extra sentence, or a few seconds
trying to position my pointer to highlight a line correctly.
It might be a little awkward, but the difference really does
come down to a few seconds.

I've seen some videos argue that those few seconds,
compounded over days and months and years, will build up
into time gained back for yourself to be even more
productive. I don't think so, and more importantly I just
don't care. Keystrokes aren't the limiting reactant in the
equation that is workflow. Even if I could type as fast as a
machine and knew every possible keyboard combination and
could execute them optimally, I don't know if I'll actually
change or grow in any way. I don't know if I'll get any more
work done. Because my brain still moves at an insurmountably
finite speed. I'm not the fastest typer in the world, and I
use exactly zero items inspired consciously by Vim, but when
I write or type I can even feel my brain not knowing what to
say next and myself pausing to think about what to put on
the page. For coding, this is much, much worse, I spend much
more time Googling and reading logs and pressing up+enter to
repeat a command than I do actually writing and producing
content. I don't care about typing much faster, because my
brain doesn't actually move that fast. Typing is useless
without meaningful content behind it.

There are a few counterarguments to this that I see. One is
that the point of keyboard-based workflows is that they
reduce the latency between your fingers and your brain, so
that as soon as you think something you can put it on the
page. If we have to pause our thinking to think about how to
write, well... that's time not spent thinking, time spent
distracting our train of thoughts with the keyboard, our
brainspace allocating a little bit too much space to the
keyboard and thus a little bit too little space to our
brain.

I definitely see the point in this, and to corroborate this
I think that we should all learn how to type with our
pointer fingers on F and J and so on. But I don't feel like
this changes my point at all. I know that I could get some
time and productivity back by having an unfocused train of
thought, where the keyboard becomes a distraction that tends
towards zero, towards ignorability. But I don't really see
the value in trying to push the distraction factor of the
keyboard continuously towards zero when it doesn't actually
dent the roadblocks that are actually in my productivity --
not understanding things, not having motivation to do
things, having too many tasks at once, and so on. These
aren't keystroke problems, but a
step-back-and-solve-your-life-at-large type of problems.

One more caveat is that I am definitely being hypocritical
in this. I spend a ridiculous amount of time first on
Notion, then on Obsidian, customizing and recustomizing, on
Pandoc, on NPM and yarn and autoclean and renv and so on.
Workflow is addictive and it distracts from the actual work
I need to do. And Vim isn't any difference in this respect,
so why oppose Vim but spend so much time on Obsidian?

My cop-out answer is that even though this is valid and I
should probably stop spending so much time thinking about
_how_ I work and instead just work, it doesn't detract from
my points at all. I don't need any more keyboard speed, so I
won't learn Vim; I definitely don't need a graph visualizer
for my notes like Obisidian offers, but so what? It looks
pretty.^[also, I feel like Obsidian is much less obtuse than
Vim.]
