---
created: 2022-03-24
modified: [2022-03-24, 2022-07-31]
title: Confusing patterns for korean learners
tags: [learning]
author: Nathan
---

and by "korean learners" i mean ME

- when ㅇ comes at the beginning of the second syllable
  - 안자 is wrong; 앉아 is right
- when to use which suffix or ending to a word;
  - 에
  - 을/늘/etc
  - 는/은/ㄴ
  - i can probably just learn those rules though
- when someone asks you "you don't like **\_\_**?" or "you
  don't do **\_\_**?" or anything else in that negated form,
  you're supposed to answer "yes", not "no"
