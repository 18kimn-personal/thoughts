---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: Large file types
tags: [git, storage]
author: Nathan
---

continuation of [[Git platforms and their stances]] The
issue comes in when you work in formats that are not
plaintext -- as far as I can see, these can be broken down
roughly into 1) media and 2) data. For media, think PDFs,
PNGs, MP3/4s, and so on. All of these take many times more
space than plaintext files, because they are encoding
detailed information about pixels (or sound information) and
might even need to do this many times a second for video and
audio formats.

The other area where this becomes a concern is in data.
Storing data, even in plaintext, can get out of hand
relatively quickly. Say I want to record the physical
distancing rates for every county in the United States --
that'd be around 6000 pieces of data, just to get the ID and
the physical distancing rate for each of the roughly 3150
counties. That's pretty simple, but what if I were to get
them for each day? Or with a hundred different variables in
addition to distancing? Or with the more granular block
group? Or with also including the geometry of these counties
so that I can make a map later^[this can take up a lot of
space because you'd record each of the (potentially
thousands of) twists and turns for a single count]. The list
goes on.

The reasons data and media can get out of hand are
innumerable, but they have the common theme of exponentially
growing without careful management. They also have in common
being things that Git can technically manage, but is not
what Git was created for -- the comparing of plaintext files
in order to describe incremental changes inside of a
comprehensive version history. With most data and media
formats, comparing one version of the file with another
line-by-line won't return anything useful.

Sometimes you can still include data and media in Git
repositories anyhow. DataHaven (an amazing nonprofit I've
done work with) keeps lots of data online, because the size
of data needed for lots of demographic analyses can be
fairly small. Lots of Git repositories exist online that are
specifically made for media or data, for example in hosting
a font or in providing a set of icons. And the problem
doesn't emerge in most of my own projects, even ones that
have data and media, because I work by myself on relatively
small scales.
