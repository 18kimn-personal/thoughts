---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23, 2022-02-14]
title: Learning Arch
tags: [trajectory, learning]
author: Nathan
---

---

## 2022-01-23 update

after about a month of using Arch I honeslty odn't think
about it anymore. I don't visit any reddit, I don't visit
the wiki... I've stopped caring about it :/

I guess that's a good thing too though! As In I have moved
past the paralyzing attempt to work on workflow. Although
now I am working on other workflow things, like a true
zettel system

---

I think migrating to Arch would be fun :)

I feel like in an unacknowledged way to myself i just wnat
to do this bc i think it's cool and for the clout factor

(although, honestly, who has heard of arch... even when i
talk to people about linux they are confused on what it is)

But of course i would still learn a lot of command line
utilities and linux syntax and how networking and formatting
drives works etcetc

### essential applications for the arch migration

workflow GUI things

- caprine
- vivaldi
- obsidian
- albert
- R and RStudio
- vscodium
- Zotero
- figma linux
- Anki
- nvm + yarn
- nvim
- coc-prettier coc-typescript coc-r

other office things

- Zoom
- cisco anyconnect / vpn tool
- remmina
- slack

quality of life things

- Spotify

CLI tools

- pandoc and pandoc-crossref
- unzip
- pass
- latex (pdftex and xelatex) (can be installed with tinytex)
- curl and wget
- gzip
- hostname
- git
- docker
- bat /batcat
- br
- rclone

system things

- plasma things of course
- kmix (needed to get thinkpad audio buttons to work
  properly, apparently)
- fcitx

### files I need to restore

- remotes
