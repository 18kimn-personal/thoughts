---
created: 2022-01-25
modified: [2022-01-25]
title: I want to learn Rust
tags: [learning]
author: Nathan
---

The personal orientatoin I've come to accept is that I want
to build a better world. But I know no lower-level or
systems architecture stuff.

I should. I think learning rust could be a good way to
understand performance on a low-level (e.g. on a pointer
basis) and especailyl with regards to setting up servers
