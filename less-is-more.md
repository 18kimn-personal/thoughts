---
created: 2022-01-25
modified: [2022-01-25]
title: In web development and elsewhere, less is more
tags: [webdev, sustainability]
author: Nathan
---

See also [redesign thoughts](./blog-redesign.md)

The recent paradigm I've been going with, mostly out of
hatred for web3, is a return to simpler, stable things. The
path to better things is not shiny new technologies that let
us build more and more, it's those technologies that help us
do less, to consume only those resources that are absolutely
necessary and to be conscious of exactly hwo we do that

Svelte, not react. CSS transforms, not d3/framer. Rolling
your own componnets, not using a framework like mui or
tailwind or bootstrap. Zero dependencies.
