---
created: 2022-02-11
modified: [2022-02-11]
title: Libertarians and software IP
tags: []
author: Nathan
---

Free software is pretty vague, there's a lot of different
political views that try to usurp it. For instance
[this guy](https://www.stephankinsella.com/2009/10/thick-and-thin-libertarians-on-ip-and-open-source/#identifier_0_3588)
is a super-libertarian who believes the non-libertarians
Moglen and Stallman have created products that are in line
with libertarian values.

That guy (Kinsella) opposes IP because it requires
dependence and enforcement from the state, and becuase it
would prevent free market use of an item or intellectual
creation. He also is _against_ net neutrality because he
believes providers should charge whatever they feel is just.

He distances himself from other libertarians who are pro-IP;
those libertarians think property rights are good and IP is
property so they should be pro-IP. He thinks IP isn't a
property right. But whatever. They are the same to me in
fundamental principles, in believing in some notion of a
"free market" along which the rest of society will be
supported.

Of course, the difference between me and him is that I'm not
a sociopath, and I also believe that no such free market is
possible. Right now we have a power structure where without
the state, we will have rule by capital. The specific
difference regarding IP and free software is that I do not
only believe that software should be free because we as
producers and consumers should have access to things as the
free market allows, but because I think we as humans should
have access to basic necessities like an OS and broadband.
