---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: A library
tags: [knowledge]
author: Nathan
---

I want to import my library of books into here, which
previously was just a folder of almost-uniformly named PDFs

steps:

- reassemble local library (i believe the current copy on
  this computer is a backup that doesn't have a lot of
  books. May have to start up the other computer again)
- copy books into personal/library
- create template for books, put that in personal/templates
- the templtae has a yaml field for filepath
- use dataview to render it

See also
[this blog post](https://thebuccaneersbounty.wordpress.com/2021/08/21/tutorial-how-to-create-a-bookshelf-in-obsidian/)
(strange hentai books here it seems)
