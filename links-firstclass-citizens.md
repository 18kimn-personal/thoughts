---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22, 2022-04-27]
title: On links as first-class citizens
tags: [zettelkasten, plaintext, obsidian]
author: Nathan
---

continuation of
[initial thoughts about obsidian](./obsidian-first-impressions.md)

my motivation for using obsidian came first from trying to
find a general note-taking software that _worked with plain
text files_ and was _locally stored_. This is the first of
[Obsidian's three motivations](https://obsidian.md/about). I
really, really wanted this because:

- it allows portability
- it allows extensibility because it's a single standard
  that many programs can adjust (principle #3)
- it allows git to index and archive it
- it integrates well with a lot of my existing projects
  which have some markdown and are all plaintext-focused
- Notion was almost-markdown based, and that felt good, so I
  wanted to take the next step and have it all be
  markdown-based

But probably the most popular use of obsidian is as a
"second brain", a tool that holds your thoughts in a
semi-permanent way, turning your fleeting ideas into
"literature" notes and then "permanent" notes. This is
Obsidian's second principle: links as first-class citizens.

"Links as first-class citizens" is a bit of vague/obtuse
wording that doesn't mean much to those who don't use the
[[initial thoughts about obsidian | zettelkasten]] system. I
didn't understand it, really, and thought something like
"uh, ok? that's fine I guess." But it's really what the
whole zettelkasten system depends on. You need to be able to
trace your thoughts from one note to the next and back
again. This is only possible if inline/local links are
supported. It's taken to the next level by features like
Obsidian's graph that interprets these hypertext links as
visual links between ideas. And by the immediate creation of
files if they are linked to but don't exist yet -- in other
words, in some cases the links precedes the content of the
files themselves -- links are _that important_.

But again, I'm not in it for zettelkasten (although I am
almost at the point of using that system) or for the
links-as-first-class-citizens idea. Some of my goals that
conflict with zettelkasten's:

- I want my "vault" to be a general workspace, not only for
  note-taking, so it has things like my thesis and creative
  projects that can benefit from note-taking but are not
  exactly note-taking
  - e.g. the zooterwesten (??) plugin doesn't work here
- I have a few too many immediate tasks that need to get
  done to be able to take notes for the sake of learning or
  taking notes, and then reflecting on those notes, which
  seems to be what zettelkasten is meant for

This is just a general, file-based system for managing my
work. Perhaps zettelkasten principles will work themselves
in here, but for now this vault is just that -- my vault.

References [second brain](https://youtu.be/OP3dA2GcAh8)
