---
created: 2022-01-11
modified: [2022-01-11, 2022-01-22]
title: On Linux and gender
tags: [linux, gender]
author: Nathan
---

One thing I've noticed is that literally everyone I can name
with some involvement in the Linux community is a man.

- wolfagangs channel
- luke smith (kind of the archetype here)
- linus tech tips (sort of linux not really)
- linux for everyone
- distrotube
- mental outlaw
- alex booker
- techhut
- ef - linux made simple
