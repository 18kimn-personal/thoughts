---
created: 2021-12-30
modified: [2021-12-30, 2022-01-22]
title: New users for linux
tags: [linux]
author: Nathan
---

I started my linux journey with ubuntu, which I had read
somewhere was the easiest for new users to use. It was
actually pretty intuitive for me, and I don't remember even
having troubles configuring things or making things work (as
I did in arch, which I am using now).

I don't actually think distros make much of a difference,
except 1) when you're inexperienced and 2) when you're first
setting up your computer. After a point everything is the
same; I spend time in only a few apps like the web browser
and the terminal, both of which are not handled by the
distro itself, and I'm sure it's the same for most others.
Running `sudo pacman -S <package>` is really not that
different from running `sudo apt install <package>`, and
almost all modern distros have snap and AppImage support.
Even where distros really do diverge, like for Arch the AUR
and for Ubuntu the widespread general support by appmakers,
the difference is really not that important.

If I had to start my journey over or recommend a distro to a
new user, I'd tell them the above (that the distro doesn't
really mattter), but recommend Kubuntu (KDE + Ubuntu) as a
starting point. KDE just offers so much more customization
to the user and the aesthetics have so much of a
cleaner/more modern look than GNOME, and I think both parts
are important for new users to experience Linux with. Linux
isn't an antiquated obscure thing that requires you to learn
exactly one way to do your work, it's something you can own
and tailor to your own needs, and it's for the future, not
the past.
