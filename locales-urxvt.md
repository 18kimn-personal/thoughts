---
created: 2022-03-15
modified: [2022-03-15]
title: locales and urxvt
tags: [workflow]
author: Nathan
---

quick note for myself: I had some really weird characters
showing up in my urxvt, and korean wasn't working, which was
annoying because the assumed promise of urxvt is that these
things are supposed to work in this application

The problem was that even though I had edited
/etc/locale.gen and ran `locale-gen` and verified with
`locale -a`, the system locale still had not been set to
this value and was left to the default of:

```
C
POSIX
en_US.utf8
ko_KR.utf8
```

I'm not sure exactly wha tthe first two encoding sets are
but I guess they aren't utf-8, and characters that pm2,
vim/neovim, and spt used for borders wasn't in those
cahracter sets. Running:

```
localectl set-locale LANG=en_US.UTF-8
# or edit /etc/locale.conf with LANG=en_US.UTF-8
unset LANG
source /etc/profile.d/locale.sh
urxvt
```

made the resulting terminal have the correct characters and
handle korean text correctly, so it seems all is good.
