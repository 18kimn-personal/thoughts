---
created: 2022-02-09
modified: [2022-02-09]
title: I have a fondness for web standards
tags: [standards, web_dev]
author: Nathan
---

originally posted on my private twitter

idk why but i have such a fondness for web standards. tcp,
http(s), unicode and utf-8, the html/css/ecma specs, the DOM
APIs, the JSON/geojson/topojson specs, the SVG spec, ...

like idk how to explain it (i dont fully understand it) but
every website would be massively worse today if any single
one of these agreements were absent. and yet they're all
here because people worked for this sort of common good, and
the specs orgs never paid anyone to do so

of course the real story is somewhat worse,
google/fb/ibm/etc profit from and push these standards
because it allows them to make stuff faster, and really
really big ones can just hire employees to work on specs
full-time (even though the spec itself is non-proprietary)

but yeah. Unlike lots of recent tech stuff -- the nft/crypto
cults, microsoft trying to rebrand itself as OSS,
surveillance tech, AWS owning everything -- web standards
are publicly owned/moderated ways for ppl to come together,
and for that I think they're important and good
