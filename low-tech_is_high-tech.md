---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: Low-tech is high-tech
tags: [zettelkasten, plaintext]
author: Nathan
---

This is about markdown. Markdown is in a way low-tech
because it doesn't depend on any advanced technological
tools to get it working; you don't need Microsoft Word, or
Google Docs; to write markdown, you just need any old text
editing application on a computer. From the outset, that
output is human-readable and interpretable.

Of course, this isn't to say markdown is primitive or
technologically backwards or anything like that. Markdown
can target all contemporary rendering formats like HTML and
PDF and LaTeX and whatever else.

But that's besides the point. Markdown is hi-tech to me
because its true value is brought out when a society
prioritizes educating its members on a few fundamental set
of free tools (how to read and how to write) and
deprioritizes depending on a few companies to do that work
for us (microsoft word and even google docs). That is an
advanced technology in my opinion, one that requires us to
advance beyond the current stages of capitalism.
