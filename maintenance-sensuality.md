---
created: 2022-02-25
modified: [2022-02-25]
title: Maintenance and sensuality
tags: [workflow, sustainability]
author: Nathan
---

I spent some time cleaning my keyboard and screens. Plugged
in an external SSD and did a hard drive backup. Ran
`sudo pacman -Syu`.

There is something super intimate about this. "Sensuality"
isn't the right word, because it doesn't have that kind of
explicit or scandalous connotation sensual things usually
have, but it seems more right than "phsyical," because the
actions are not just physical. They scratch an itch back
there in my brain.
