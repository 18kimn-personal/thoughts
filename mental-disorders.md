---
created: 2022-03-29
modified: [2022-03-29]
title: mental disorders
tags: [workflow, sustainability]
author: Nathan
---

Despite the pattern of _work intensely_-_fall into
depressive state_, described in
[obsessions.md](./obsessions.md), I think I'm definitely not
bipolar, at least by the clinical definitions of the up and
down swings lasting for weeks and months; my swings are
circumstantial and short. I also hope not to imply that any
of the above swings are in any way a medical disorder, as I
think that the constant push to classify something as a
disorder is itself a symptom of a sick society! Perhaps
doing rewarding work and wanting to do rewarding work are
natural and good things!

I wonder sometimes if I have ADHD, becuase the twitter
discourse relating to ADHD seems to recognize patterns like
these as symptoms. But mostly I think that this discourse
tries to overdiagnose (like the bipolar thing) normal and
natural behaviors as unnatural and unhealthy ones

I still hold that the one week I was on adderall last winter
was perhaps the most enjoyable week of my life

The last thought is that perhaps it's not relaly a question
of whether we are under/overdiagnosing, but relaly more of a
reality that both disorders are on spectrums that I have
trouble placing myself on, and my confusion stems from
constantly being somewhere in the middle instead of the
premise itself being flawed
