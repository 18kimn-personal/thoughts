---
created: 2022-08-05
modified: [2022-08-05]
title: Strategic misrecognition and being simple or obtuse
tags: [social]
author: Nathan
---

I wrote about [misrecognition before](./misrecognition.md),
but I think the case I wrote about there (accepting
university funding and advancing "diversity" to gain
resources for more radical politics) doesn't do the maneuver
justice -- in that situation, misrecognition is a
compromise, something that can trap us just as much as we
gain from it.

I realized that often misrecognition is helpful for when we
want to be simple instead of obtuse. I guess I had this same
thought when I was writing my `no-more-paywalls` website:

> Knowledge should be free. There's a lot more that can be
> said there by people more eloquent and politically
> developed than I, but there is also no need to. It's as
> simple as that -- _knowledge should be free_.

In that of course there are complicated ways to say things, but
we can also misrecognize the problem at hand by pretending its
simply a matter of valuing knowledge as a public good. And it
is! 

So it's misrecognition in the sense that it brushes aside
complicated politics and reasoning to pretend it's simpler,
but I don't feel that the truth of the matter has been lost at
all. It's not a compromise or being dishonest to do this sort
of misrecognition; in fact we could even say we are making a
compromise to explain the details of why knowledge should
extend past copyright's walls, if we could do so in so much
simpler terms.
