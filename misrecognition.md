---
created: 2022-08-04
modified: [2022-08-04]
title: Miscrecognizing diversity and power
tags: [social]
author: Nathan
---

One of my first non-tech related comments. And it is a
little bit related to tech.

The main observation being that power is often misrecognized
for diversity in a lot of racial justice and social equality
settings. I'm thinking of DEI initiatives that are
supposedly parroted by HR teams and university dean's
offices. As in, they say things like we're better when we
can add to each other's strengths, we take pride in our
diversity of experiences, etc. Which has no real argument in
and of itself, and provides an opportunity for right-wingers
to launder back that there is no (intellectual, political)
diversity, that there is no true diversity of experience,
that in that case race and gender shouldn't matter, what
should matter are the things we experience.

Of course, the reason we should have diverse settings is
because those settings were built to systematically exclude
and enforce certain power hierarchies over certain groups.
Not to white people who have rare experiences (who may
supposedly add to "diversity"), but to black and indigenous
students/workers, to women, to those outside of the narrow
binary gender constructs our society holds close...

But anyhow. That's sort of old news. The comment I want to
make is that I consider it actually a very effective
strategic maneuver to misrecognize power for diversity in
this way, because it's a feel good thing that can bring
resources in fast. You can build a cultural center
ostensibly to host dumpling nights and hot pot saturdays and
use it as a space for radical organizing, of the kind meant
to wrest officials of their positions, to demand divestment,
to demand further resources for those disempowered by the
university (and other corporations, and in particular _not_
for white people of niche backgrounds).

The kind of key point here is to turn it away from
cooptation, the more common kind of misrecognition, into
subversion, in other words make the misrecognition
intentional and momentary. Instead of being lost in
diversity rhetoric and getting distracted from the struggle
for power, it can be possible to use the diversity rhetoric
to gain resources in the struggle for power and keep the
focus there.
