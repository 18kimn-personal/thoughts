---
created: 2022-02-14
modified: [2022-02-14]
title:
  Mitski's _Working for the Knife_ is really, really great
tags: [music]
author: Nathan
---

I've been a fan of Mitski since _be the cowboy_... in
popular culture I feel like people sort of reduce her to a
soft sad girl aesthetic

in reality I feel like her music is way more expressive...
which she's been pretty adamant in interviews

_Working for the knife_ is so good...it's a departure from
the kind of melodramatic, longing, shouting expressiveness
of her previous phase of music, and leans in more towards
rhythmic, muted, almost monotonic expression of the
resignation-after-frustration in modern capitalist knife

it's not that it's catchy or has a memorable melody... but I
have it on repeat
