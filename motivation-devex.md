---
title: developer experiences and staying motivated
tags: [workflow, webdev]
date: 2022-01-06
---

There's not much more to say here than that. With a good
developer experience, I want to code forever, I want to
remove libraries and code that functionality myself. I don't
even need to share the project or tell anyone about it
(although that usually is a plus). It sustains me rather
than taking things out of me.

Ok, there's some more to say. Firstly, what even is a good
developer experience?

- You are using frameworks and libraries built on a well
  thought-out and extensible grammar; only a few things to
  learn/install to do many things
- You have tooling that gives you
  intellisense/autocompletion, type checking,
  linting/formatting on save.
- For web dev, HMR or fast refresh
- Readable colors and fonts
- An IDE that never freezes, lags, and does things like the
  aforementioned intellisense/autocompletion and related
  things like fuzzy file finding and text searching
  _extremely fast_. With my current tooling I currently
  don't have to wait, ever
- Control and ownership over the above decisions. Control
  over what libraries and stylign systems I use, over what
  IDEs I use, over what hosting platforms I use, ...

I had these thoughts while browsing things on the PSID's
enclave. Man. It's so slow, graphics are terrible, can't
install anything (no internet on the enclave), no custom
fonts, can't export code or data without theri consent,...
