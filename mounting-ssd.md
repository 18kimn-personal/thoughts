---
created: 2022-01-26
modified: [2022-01-26]
title: Using an ssd
tags: [workflow, linux]
author: Nathan
---

since I always forget this:

- check device port with `sudo fdisk -l`
- navigate to place you want to mount and then run
  `sudo mount /dev/sda <name of empty folder>`
