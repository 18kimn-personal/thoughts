---
created: 2022-01-22
modified: [2022-01-22]
title: What an effective Zettel system should have
tags: [workflow, zettelkasten]
author: Nathan
---

For a note-taking system to function effectively as a
Zettelkasten system, it needs to have the following
features:

- [x] a command to quickly create a template note (e.g. with
      some YAML metadata filled out)
- [x] a way to quickly insert links
- [x] a way to quickly insert tags
- [ ] a way to quickly view what files have a given tag
- [ ] a way to quickly view what files link to this one

I used "quickly" here, but the more accurate (and slightly
more obtuse) one would be "frictionless." If it takes a
second longer but requires you to expend zero brain energy
and doesn't interrupt your train of thought, that's
frictionless.

I've made some fzf commands to insert links and tags, but
think I need some new commands for the latter two.

In addition to the above features, some additional aspects
are useful for helping organize the files, but they're not
strictly necessary. Those are:

- a consistent file naming structure. A single filename
  format is not necessary because the zettel system relies
  more on tags than on filenames, and in fact my contention
  currently is that the Zettel prefix is less helpful than
  the tags because it's not informative as to what the
  content actually is.
- a flat folder, as opposed to a nesting structure. When I
  open a previously edited note, I shouldn't have to go
  through nested folder structures to find related files.
  This is difficult because:
  - files often belong in multiple folders, but they can't
    (they can definitely have multiple tags though). This
    means that when I look through a folder in search of a
    thought (file), that thought could have just as rightly
    be placed in a different folder.
  - For folders, it's ridiculous to continue `cd`ing and
    `ls`ing to see what thoughts exist. It's too much
    friction.
  - The tag system requires me to comprehensively add tags,
    and prevents any sort of hierarchy (e.g. before all
    plaintext-related thoughts were in a workflow-related
    umbrella, but that can't happen with the tags system).
    But that's fine, and it's even a feature that I want. It
    requires me to take time to reflect and classify, and
    the additional time involved in the nested folder system
    only takes up more time without focusing on this
    reflective part.
- a good formatting configuration. for me this is
  printwidth: 65 in prettier; also useful is the spell
  function and restricting autocomplete to only those words
  that are tags in the zettel system. This is helpful to
  give files a uniform format without thinking about it, as
  free-form the actual content may be

## more on tags

- i briefly thought about the tag system supporting nested
  tags. The syntax would be:

```
tags:
  - parent:
    - child1
    - child2
  - standalone-tag
```

The tags this would generate are parent, parent/chidl1,
parent/child2, and standalone-tag. The benefit of this is
that it helps prevent tags from getting overwhelming, like
it'll be impossible to choose a correct tag when there's 100
of them. This hierarchical system limits the number of tags
at any level in the hierarchy, letting me make two or three
easy choices instead of one long and messy choice.

But the problem for me is that this is way too complicated
to implement and maintain. Even a simpler nested tag syntax
of just writing parent/chidl1, parent/child2 is too
complicated. I would have to code an autocomplete thing
separately, and tags would have only one place in the
hierarchy when they might just as well fit better in
multiple places.

My solution:

- only use tags for meta-topics. "authors" is okay as a tag,
  but "james boughton" is not. For when I want connections
  on that smaller level, I should create a structure note or
  simply use markdown links. This is to limit the number of
  tags that will be generated overall, and increase reliance
  on links rather than tags.
-
