---
created: 2022-01-27
modified: [2022-01-27, 2022-02-14]
title: NFTs and nothing new
tags: [capital]
author: Nathan
---

Luxemburg argued in 1913 and lenin threeish years later that
when capital exhausts one market, it has to find another

For someone's million dollars to be worth anything, it has
to be able to consume something and make even more money. If
it sits in a vault with nowhere to be spent or used, it is
not worth anything, and if this happens on a marketwide
level, that's a currency devaluation and the market will
collapse

Sometimes it makes up ways to make money (the screenshot of
lenin's credit and fictitious capital that's been going
around). Sometimes it forces its way into new markets (old
imperialism). Nowadays especially, it mostly does both, e.g.
IMF-guided liberalization of markets so that speculators can
be let in.

In any case my main impression of NFTs are that they are
just another new invented way for capital to exercise
itself, since only in exercising itself can it receive any
value whatsoever. There definitely haven't been bored apes
or cringey 3d videos or neckbearded web3 enthusiasts in ages
past, but there have always been people that want to make up
new ways to make money that don't have any material basis.
There have always been money laundering schemes, wash
trading; the world finance market is a hype train and all
major crises come from that hype train stopping eventually.
There definitely haven't been GPU-intensive mining that
guzzles more electricity simply to find another
factorization of prime numbers in ages past, but there have
always been energy-guzzling ways to make more money (heck
that's why we're in this climate crisis right now), and
that's not new.

I definitely think the combination of energy-guzzling for
imagined credit to be a bit new, and the laughable but
really publicized high-priced nature of NFTs to be a bit new
(e.g. previously laughably high-priced art was only for a
select few to begin with, not marketed to the masses). But
NFTs are nothing new.

So I don't have any special position against them as I do
against the more general ongoing expansion of credit and
fictitious capital, of territorialization and conquest.
