---
created: 2022-01-24
modified: [2022-01-24]
title: Nix OS and package management
tags: [linux]
author: Nathan
---

I've been using [arch](./learning-arch.md) for the past
month or so, it's been alright

today i saw a hacker news
[post](https://blog.wesleyac.com/posts/the-curse-of-nixos)
about nixos, a linux distro that uses a
content-addressable-store method to manage packages

packages are never installed globally, and you have to
specify which packages you want to use when you open a shell

the author implies that there are default versions for the
programs that are used, and i'm guessing nixos provides a
way to configure default packages for a shell too (although
if it doesn't you can roll it yourself)

for the author this is the best aspect of nixos, and that
he'd been using it for his laptop for three years

I don't know. This seems too much for me. **I only ever want
one version of a package, and that's the package that
works.** I want version management to be a solved problem
that I don't have to worry about, and to be honest, despite
this problem being potentially feasible in enterprise
settings, in all personal cases with AUR stuff it has just
worked. I've never worried about which versions of packages
to install, only what packages to install.
