---
title: learning to do things without KDE
date: 2022-01-10
tags: [linux]
---

I recently uninstalled KDE and Plasma Desktop, and am
working solely with i3 (and i3bar, and i3status) to act as
... my desktop environment...although i3 is profoundly just
not that.

So I'm relearning how to do everything...

- USB
  - Get a list of the connected drives with lsblk
  - mount it somewhere, KDE used to do it by making a
    subfolder at /run/media I think so that seems good. Do
    that with `sudo mount /dev/<drive>`.
  - work with the files
  - unmount with `sudo umount /run/media/<subfolder name>`.
