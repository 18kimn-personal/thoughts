---
created: 2022-03-29
modified: [2022-03-29]
title: Obsessions
tags: [workflow]
author: Nathan
---

Something I've thought about a lot but can't find anywhere
(?? maybe it's because I consider it too personal for my
public archive; the closest I have is
[this note](./workflow-debt.md)) is that I work in spurts,
and despite my best attempts to be disciplined (and genuine
gains from these efforts) 90% of my work is done in 10% of
the time i spend "working," and the other 90% is spent
lounging around, writing notes, watching oyutube videos,
texting friends, etc.

I work in obsessions; chasing after the last bit of dopamine
I see in front of me as fast as possible, then when it runs
out being frustrated and in a depressive state. I hate doing
logistical work or paperwork of any kind, even filling out
my timesheets in order to receive pay, because often enough
the tasks I have in front of me are in itself rewarding
instead of doing something to receive a later reward (e.g.
they give instant gratification). When I do have to do
logistical work or any other task that is not my primary
obsession at the moment, I become distracted every minute,
looking at my phone, staring off into space, getting up to
get water, getting up to go to the bathroom.

I was first into R, and then later JavaScript and web dev,
and Linux and other free software-related things (i3, vim,
my zettel system, etc), being able to "do" them in my free
time even because I considered them fun and interesting,
even if to an observer they seem like the dryest shit in the
world. I think they attracted me because coding in general
has this allure of being both obtuse/obfuscated and built on
indisputable logic, and despite knowing that both aspects
are myths, being able to understand one part of it made me
feel like i was in a special club or was able to do
something others couldn't.

---

> I do not mean to say that conventional web development
> cannot be a contemplative or transcendental act. I only
> mean to distinguish between conventional web development,
> which I know little about, and my own emulation of web
> development as a poet in a culture of poetry steeped in
> printed matter.2 I also understand that many web
> designers, developers, students, and other creatives work
> in a similar, albeit unhealthy, way—working more than
> eighty hours per week, losing sleep, eating poorly, being
> obsessed, or whatever.

- [benjamin wil's blog post on his project "Sleep when exhausted"](https://benjaminwil.info/weblog/sleep-when-exhausted/#fnref:4)

---

My usual answer to this has been two sort of complementary
strategies:

- form habits/patterns/structure, so that even with this
  obsessive engine I could still work sustainably. They
  reduce the "cost" of work I don't like, since I get used
  to it or can offload it into structure (and even
  automation)
- Inject the things I do like into the things I don't like;
  this is what I wanted to do for a while regarding my
  thesis about the IMF

unfortunately both of these have their limits and
limitations, they don't work that well and I'm not that good
at them.

---
