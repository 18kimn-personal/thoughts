---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: First thoughts about Obsidian
tags: [obsidian, zettel]
author: Nathan
---

in [[2021-11-01]] i talked about how obsidian is good for
turning jumbled mess into purposely organically flowing
all-over-the-plcae sort of files

i realized after viewing
[some reddit comments](https://www.reddit.com/r/ObsidianMD/comments/qgg2qi/im_officially_using_obsidian_for_1_month_today/)
that this requires some more management than i thought

for instance, in order to have each journal entry date be
connected to each other, they need to link to the previous
or next day

that seems a bit difficult

another difficulty is in the dataview plugin, which can be
used to autogenerate links in the preview mode for notes but
is not picked up by the graph view

---

but probably the most important thing is that i do not need
to use this for the graph view, although that is fun, i
should be using it to organize my thoughts instead of make
them appear more organized than they are
