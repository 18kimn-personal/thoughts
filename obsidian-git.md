---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: Obsidian vaults and git
tags: [obsidian, git, storage]
author: Nathan
---

continuation of [[types of large files]]

This problem of storing things with Git has emerged for me
at two recurring moments in the past: 1) I work with spatial
data and 2) I try to make GIF or MP4 animations. It emerges
for me now as I try to organize my Obsidian vault as a Git
archive. I previously cut down on space by storing
individual projects inside the vault as submodules instead
of plain folders.^[This means that Github doesn't host them
inside the main repository, it hosts them as a separate
repository and inserts a pointer to that separate repository
inside the appropriate location of the main repository.]
Github doesn't have a limit on the number of repositories
one can make, so this is a fairly sustainable solution.

But there are lots of projects where it doesn't make sense
to host them as submodules, for example my humanities
classes that are full of readings and PDFs distributed by
the professor. I have large files that are hard to store
with Git, they don't really benefit from being stored
through Git because Git can't track internal changes within
these files well, and yet they are part of my vault and I
would like to make them accessible.
