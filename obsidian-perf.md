---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22, 2022-01-23]
title: Obsidian and performance
tags: [perf, obsidian]
author: Nathan
---

Obsidian seems to be slowing down by quite a bit because of
too many .md files to keep track of, or perhaps becuase
there's a few md files in particular that are large that
make it difficult to parse. Whatever the case, performance
has been lagging a bit.

This mostly comes from the fact that I'm using my Obsidian
vault not only to house my notes but also to house all of my
schoolwork and personal projects, meaning that there are
lots of .git and node_modules directories all over the
place. There may be many more markdown files (and PDF and
png files, which obsidian tracks but doesnt parse) in
Obsidian's brain because of these situations.

The strategy I tried so far was to go in my biggest npm
projects and run `yarn autoclean --init` and
`yarn autoclean --force`, which _definitely_ did a large
part of the job at taking .md files from 4537 to 1165. The
largest chunk came from my blog repo.

By the way, the command to find the number of files was
`find . -type f -name "*md" | wc -l` where `.` referred to
the top-level directory of the vault.

The other strategy I did was to navigate to
`~/.config/obsidian` and deleted the cache directory, which
had hogged some 180M. When i restarted obsidian it
repopulated at some 20K, so the cache was definitely hogging
up some disk space but it looks like it wasn't the
bottleneck for obsidian performance.
