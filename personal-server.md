---
created: 2022-03-15
modified: [2022-03-15]
title: Personal servers
tags: [web_dev, sustainability]
author: Nathan
---

I wrote in
[servers and commodity fetishism](./servers-commodity-fetishism.md)
that I think servers should be more...personal! far from
alienated, a relation between people instead of a relation
between things

This is an attempted list of concrete ways I can do this,
and that are easier / more practical / more fitting with
personal servers rather than cloud architecture:

- personal website
  - "currently listening to" (spotify)
  - "currently doing" (gcal)
- my zettels
  - there's enough of them / they are updated frequently
    enough that there would have to be constant rebuilds for
    cloud architecture (probably a reality for some
    companies but more fit for where i have a personal
    server where I can make flexible changes)
-
