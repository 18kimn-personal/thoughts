---
created: 2021-12-25
modified: [2021-12-25, 2022-01-21, 2022-01-23]
title: personal statement
tags: [trajectory]
author: Nathan
---

Towards tools for a better world.

I am frustrated with the way that the world is -- how the
forces of racial capitalism have recapitulated themselves
after every mass movement against the hierarchical
structures they create, to the point of living in our fiery
neoliberal hell.

I think a lot of -- to be honest, the only -- way to break
out of these are in a radical sense. Radical as in root, as
in we need new architectures and structures and assumptions
from which a new world can be born.

I am interested in two avenues in this pursuit.

- Critique: how the existing _everything_ -- tools, culture,
  economic system, political structures, -- have failed us
  and succeeded for the ruling class. I'm specifically
  interested in globalization in this pursuit, but really
  everything has to be questioned and rebuilt, from the
  1600s to the world tomorrow.
- Creation: making and promoting new tools. We need new
  systems, new expressive languages.

A lot of my current work kind of messes around with these
topics tangentially. Creative code, visualizations,
