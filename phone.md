---
created: 2022-03-02
modified: [2022-03-02, 2022-03-24]
title: need a way to link between computer and phone
tags: [workflow]
author: Nathan
---

I want a way to take notes on here and then very easily
transfer to my phone, and vice versa

on my phone it's difficult for me to type so i really prefer
being able to type to-do lists or grocery lists or something
and just sending it to my phoen

i used to use ubuntu touch, thinking that the
programmability would help a bit with this, but i was
unfortunately mistaken

needs to have a single button to press to sync

perhaps ill just use a git repo for this

---

ok yep, my zettel folder is now a git repo hosted on
[gitlab](https://gitlab.com/18kimn-personal/thoughts.git)

i had to copy my ssh keys over to my phone to give it
read/write access to that repo. this was a bit of a
conundrum because I didn't know the "right" way to do this,
I guess I should have scp'ed it but too late

i still have to use two different apps to manage this, one
being obsidian and the other being mgit

perhaps obsidian git plugin works on the mobile version?? i
should check it out

update: it does not

i wish mobile apps were easy so i could do this myself, but
it seems p difficult to have to learn swift or obj C or
flutter

---

Another thing I want to integrate my phone with is through
pictures; adding the pictures I've taken on my phone to my
journal vault to be referenced easily
