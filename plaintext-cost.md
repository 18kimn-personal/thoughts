---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22, 2022-03-02]
title: Cost and markdown
tags: [sustainability, plaintext]
author: Nathan
---

But for myself, much more convincing than trying to reasona
bout all of the supposed disadvantages markdown doesn't have
(how confusing is that, even) is to argue for the advantages
markdown does have. And to me, the number one reason is
cost.

Universities and school districts shouldn't have to pay
millions of dollars to Microsoft or Google so their kids can
use text editors. Companies shouldn't have to be a Microsoft
customer by default. There is no reason that these companies
can have undisputed monopolies (bipolies?) over cost.

It should not cost anything to write on a computer, or to

Markdown might be harder to use, granted (but not really, as
I mentioned in [[point-and-click is easier to use]]), but
that simply changes one barrier (cost) for another
(educational access). And as a society, we should embrace
tools that have a lower economic cost but a slightly higher
educational cost. Because we as a society can deal with a
standard of having everyone be educated, but we as a society
should not accept a standard of paying millions of dollars
to a few corporations.

See also https://sive.rs/plaintext
