---
created: 2021-12-25
modified: [2021-12-25, 2022-01-13, 2022-01-22, 2022-03-02, 2022-08-05]
title: Plaintext vs GUIs
tags: [plaintext]
author: Nathan
---

For the first point, I definitely agree that markdown can be
a bit obtuse to learn at first. But only a bit. My three
points of response are that:

1. If you have any form of an IDE (Obsidian and VSCodium are
   two to start with) they can fill in the gaps for you --
   you don't have visual cues that can tell you what you
   want, but IDEs do provide "feedback" by formatting text
   between double asterisk pairs as bold and so on. If you
   type the correct syntax then the IDE will give the
   correct formatting
2. It's really not that much to learn. There's only a
   handful of things that I use on a day-to-day basis:

   - bold
   - italics
   - headings
   - lists
   - links
   - images

   All of these don't require anything more than dropping a
   set of asterisks or brackets down. There is definitely
   somewhat of an intimidating learning curve, but is
   learning six things impossible for academics whose job it
   is to learn and to make knowledge?

   To be honest, it's even less than six things because
   Markdown was designed with a grammar in mind. Bolded and
   italic text are close in concept, so they bear similar
   notation (double or single asterisk pairs). Links and
   images are close in concept (linking to external
   information), so they bear similar notation (bracket plus
   parentheses).

   The caveat to this argument is that Markdown is more than
   six things to learn. You can use tables, YAML metadata,
   internal links, inline HTML, and so on. For the case of
   YAML and inline HTML in particular, there's a potential
   infinite amount of things that could be learned --
   markdown's specification was designed to be extensible
   and there's a lot you can learn and do with these
   extensions. That might be a lot more than six things.

   But my response to this counterpoint is that it goes both
   ways: sure, you can learn a lot more than six things, but
   you can also learn a lot less. All you have to do to
   start writing in markdown is open _any_ plaintext file
   (it doesn't even have to end in .md, to be honest!) and
   just start typing.

3. _Point-and-click is actually more difficult to use_:
   Relatedly to the point of formatting, even if there's a
   lot of things you _could_ learn, what you _actually use_
   is on the whole manageable. Another aspect of the
   markdown philosophy is that we don't need an endless
   amount of formatting and that in fact hinders us from
   writing. We just need a few things to start with, and if
   we really wanted we can reach for more. And those few
   things take less than a day to learn.

   Point-and-click interfaces are designed with a different
   philosophy. You might
