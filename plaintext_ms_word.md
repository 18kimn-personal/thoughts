---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: Microsoft word is not good
tags: [plaintext]
author: Nathan
---

The most popular alternative to plaintext files for
academics is microsoft word. Microsoft word has a few
supposed advantages over writing markdown files:

- [point-and-click is easier to use](./plaintext-guis.md)
