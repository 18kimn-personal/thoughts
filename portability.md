---
created: 2022-01-13
modified: [2022-01-13, 2022-01-22]
title: portability, from markdown to i3
tags: [sustainability, plaintext]
author: Nathan
---

A huge benefit of markdown is portability: it's based on a
small, (now) clearly-defined standard that can be converted
to lots of differnet formats.

I seem to have lost a lot of that value in switching to i3.
i3's not portable to any other widely used format,... tiling
window managers aren't going to be useful to the masses in
the future, so why switch to this niche/obscure and hoenstly
obtuse system?

After some difficulty in i3 I have been considering
switching back to plasma. At least that way anything I work
on for KDE that is eventually published can benefit other
desktop environment users, which are many millions of times
more than tiling window manager users.

I'm not quite at that point but I may be soon. i3 and
markdown are similar in exposing low-level information and
being relatively transparent in how they function and how
user functionality can be added, but i3 was meant to be the
opposite of portable. It was meant if anything to be a
domain-creating item, to let you create your own unique
configuration completely differnt from everyone else's,
rather than a domain-integrating or domain-bridging item.
