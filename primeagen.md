---
created: 2022-02-27
modified: [2022-02-27]
title: primeagen and contagious excitement
tags: [media, sustainability]
author: Nathan
---

I stumbled across Primeagen's youtube
[channel](https://www.youtube.com/c/ThePrimeagen)

In
[his talk at vimconf 2021 on harpoon, a vim plugin he wrote](https://www.youtube.com/watch?v=Qnos8aApa9g),
there's a comment that reads

> I frigin love this guy. He is so insane! What a pleasure
> to watch

It's so true. He's so insane. Everything from his intro and
outro to random animations, to his out-of-breath screaming.
I really like it though. His excitement about seemingly
mundane things (vim plugins, other workflow things, rust
setup, etc) just makes those things so much more interesting
and engaging.

There's a contrast I want to draw between him and
traditional clickbait-y youtubers that similarly just end up
screaming, in that 1) his screaming hides what must be many,
many hours of silent and tedious learning of the things he
talks about at a very low technical level, and 2) he
discussed his kind of crazy and difficult path to where he
is today

I just feel like he's more genuine because of this than the
basic fuckboi youtubers lmao

> You know why?!! Because I started navigating with the
> SQUIRLY BRACES!!!

lmao

It's definitely not for everyone to like, but not everyone
has to like it... his personality is an argument to the
listener that the thing he's talking about is interesting
and valuable, and the other person doesn't have to buy it.
But it's an effective argument imo! Becasue excitement is
contagious!!

I hope I can spread a similar kind of contagious excitement.
