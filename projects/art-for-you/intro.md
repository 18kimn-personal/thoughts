---
created: 2022-04-21
modified: [2022-04-21]
title: art for you
tags: []
author: Nathan
---

## the thing

a simple website. you click a button, and it makes a nice
art piece for you

the kicker is that it will always be the same if you ever
visit the site again, through a canvas piece

## the angle

Two angles:

1. talk about canvas fingerprinting, a browser technique
   that is incredibly popular but not many laypeople know
   about, in a harmless and playful way.
2. point out that the current sort of intimate, hashable art
   world can be done in different ways than relying on a
   blockchain. Fingerprinting is one, but even better there
   are simple password hashes (you have to log in to NFT
   marketplaces anyhow, so this is pretty much the same
   thing)

## the approach

do it in svelte and with **no** additional libraries -- no
p5, no d3, no...no nothing.

allow for a PNG download. The PNG will always be the
`window.width`

## the problems

there's actually no personalization beyond the uniqueness;
the art isn't actually tailored to match the person.

when something aobut the user machinery changes -- if they
use a phone vs a computer, a different browser. in that
sense it's a misrecognition of a person for their
machinery...

canvas fingerprinting is still sort of sus even if we do it
nicely. maybe this is supporting the practice instead of
going against it.
