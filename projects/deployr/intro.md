I had an idea for a GUI app that helps you do two things:

1. assemble the content of a website
2. deploy the website from your machine

---

Web3 has us convinced that we need a new decentralized way
to do things, built on cryptocurrency and IPFS and
everything else. For most, it's appealing since it'll help
them get rich (it won't). The pitch is that

In reality, what we've seen over the past year of web3
evolution is its inherent centralization.

There's nothign wrong with centralization. But it's
laughable to me becuase web3 is so full of shit. The
promises that it offers of decentralization was always a
lie.

Anyhow, that's my personal motivation for making this app.
We don't need the cool new tech of web3, because the
internet itself began with so much potential to be a
decntralized protocol (before the inevitable happened and
the gravity of capital prevented it from being so). We
should embrace that decntralized nature, by returning the
potential to build websites to individual people. Not even
people that are interested in web development or software
engineering. You shouldn't need to care about devops or
building a server. You shouldn't really even need to know
about the basics of how HTML and CSS work.

There's billions of us connected to the internet right now.
There's billions of us consuming the information on the
internet. Providing and building the structure and content
of the internet should be available to all of su as well.
