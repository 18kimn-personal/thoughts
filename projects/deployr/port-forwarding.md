One thing I forgot about is port forwarding.

In order for a port on your computer to be publicy
accessible to the internet, it must pass through the router
(all connections pass through the router to get to you; you
have to configure it to open it up a bit more).

I'm not sure how to include this in the app....at some point
the user is left to go to their browser and log into their
router's interface and enter the right port numbers etc.
etc., all outside of this app's scope.

I could do it through puppeteer/querying and regexing ports,
but... that feels hacky. I'd also need to get my hands on a
lot of routers to do that. And the routers could change at
any time.

Another process I've been seeing is a tunnel, but I don't
like it because it requires all data to be sent through a
server, which is quite a lot.

The other process I've been seeing is TCP hole punching, but
it 1) requires a server to handle the initial state for each
connection, and 2) is unreliable.
