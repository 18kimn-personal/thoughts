2021-01-02

I've hit an unexpected (but really I should have expected)
error in that although the content is being streamed in
fine, d3.geoPath is having a hard time loading this amount
of data onto the screen at 60fps

The solution I'm thinking of is to maintain offscreen
canvases and update the offscreen canvas when a new shape is
received.

This would mean that zooming and dragging have to be handled
with by zooming into the canvas instead of by modifying how
d3 projections and path are drawn. I'm kind of sad about
this because it means that as we zoom in we don't get any
additional resolution, we just get a zoomed in canvas.

It would also mean that smoothly rendering in polygons
wouldn't work because each frame of the render-in would be
drawn on top of the previous frame involved.

One strategy to try is that perhaps rendering one
multipolygon would be faster than rendering a hundred
polygons (although I don't think this is the case).

---

I think I've run against a sort of unavoidable tension:

- to take advantage of streaming, I should send large
  amounts of data to the user
- to have fast render times, I should draw relatively simple
  shapes by having small amounts of data

For the IMF project where I was able to get shapes to 60fps
with constant changes of color, opacity, and projection, the
total file size was 68k. For this project, the largest file
("subregions" at full resolution) take up a massive 76M.
This was becuase I figured that on my connection, which is
just home internet, this file is downloaded in like 5
seconds. A 5-second intro sequence is fine I thought, and if
the user can take it, the user should download it.

I think now that this was a mistake, because a single render
takes like a second to load. I should keep even the largest
file at less than a megabyte in total size. Streaming then
doesn't benefit anyone but those who have slow internet, and
that is fine and good.

Sigh. It means quite a bit more preprocessing though, which
I had hoped would be done by now. I also worry that my
GitLab storage limit for this repository will eventually be
pushed over the 10Gb limit with all of the rewrites, but if
it hasn't reached its point yet it probably won't be as I'm
drastically cutting down file sizes now.

---

The topography file is at about 350kb, which is fine by me
for sending over the wire but I'm worried that I won't be
able to get anythings 60fps with it.

There are two parts where animatino is needed:

- animating in
  - opacity fade in
  - linestroke draw in
- zooming and dragging
  - begin zoom by just zooming the canvas
  - on an offscreen canvas handle the zoom with a d3
    projection
  - after the zoom on the onscreen canvas is finished,
    transition the offscreen canvas in

A useful performance trick cache the result of path() in
path2d() so that redraws can directly reference the computed
paths. As seen
[here](https://gist.github.com/john-guerra/05fafb55db94775db6b40ba62aa02772)

---
