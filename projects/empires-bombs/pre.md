I want to make an interactive story of the bombs dropped on
Korea during the Korean War. This story is devastating,
indicts the US deeply if it is told with two eyes open, and
has not been told in a visually motivating way before.^[This
point should be explored a bit. I still want to tell the
story if it has been told before, but we should be aware of
existing work. Some links are below]

But there have been too many projects that have started and
dropped off; I should get the site to a near-working state
first, and then present it to her/whoever to show them that
it's viable and almost ready to go. With the proposition
that except for the basic idea of animating and showing an
interactive map of the bombing of Korea during the Korean
War, we can change any detail of the design or story.

- Existing work
  - World War II map on GMaps
    - https://insight.livestories.com/s/v2/world-war-ii/3262351e-df74-437c-8624-0c3b623064b5/
  - [Reddit user WWII map](https://www.reddit.com/r/dataisbeautiful/comments/5j8nlp/every_bomb_dropped_by_allied_forces_during_wwii_oc/)
  - [Reddit user's map of Korean War](https://www.reddit.com/r/MapPorn/comments/92cjng/the_korean_war_armistice_was_signed_july_27_1953/)
  - [Digital NK's exploration](https://digitalnk.com/blog/2017/10/08/visualizing-the-korean-war-bombs-propaganda-and-data-visualization/#sec3)
- Departures from existing work
  - We can improve on all of these visually
    - animation/transition, interaction
  - We can improve on all of these politically and
    historically
    - The WWII reddit post has comments like "bet u
      masturbated to this"... disturbing in the face of so
      much death
- Narrative themes
  - The war was long, and the element of time is itself a
    source of pain. The war trudged onwards, bombs were
    steadily dropped for a long time...
  - The amount, number, location of the bombs is devastating
  - The war was overwhelmingly devastating because of the US
    instigation and involvement
  - Other themes I want to include but are hard
    technically/scope-wise
    - The war was international
    - The war started before 1950 and hasn't ended yet
    - My response to this: we can definitely do it but it's
      a different project. My project is about the bombs in
      particular, because it is so underdiscussed and that
      is where our main visual argument strikes; everything
      else is important and related but firstly just a
      different project and secondly one that is hard to
      communicate visually the way that I can see this
      working for the story of the bombings
- Aesthetic design
  - 80% height 40% width panel for text
  - Home panel: a summary of the project, with link to
    github repo (where methods and data description will be)
    and contact info
  - Side menu:
    - a menu button is present always that replaces any
      existing buttons with the following when clicked:
      - home: goes back to initial state
      - \_\_ mode: allows for transition between modes
      - further reading / other resources: link out to good
        texts
    - At other times, the story itself is shown
  - Story: two versions
    - Narrative mode
      - animation pauses after each story
      - Users move either with arrow keys or clicking when
        prompted
      - Users can't navigate between story components, or to
        different points in time
      - The idea being they have time to read and process
    - Explore mode
      - animation marches onward, with control buttons for
        play, pause, and a slider for time
      - Text is displayed in sidebar and users can toggle
        between our posts with tabs at side or bottom
  - Map
    - Map begins drawing automatically as data streams in;
      use d3.timer to smoothly color in lines and shapes
      (e.g. so they fade in one by one)
    - Map begins to be scrollable and click-able after
      render-in is finished
      - clicks should show details of locations
    - Each bomb begins with a red circle, that has an
      expanding ring, which fades out and leaves only the
      red circle (small)
- Implementation notes for aesthetics
  - UI built through Sveltejs
  - Text should be written in markdown, saved to public
    folder, and fetched in (make sure to do this in
    parallel, e.g. with Promise.all)
  - Maps made with d3.js
    - Use Canvas for map, DOM for text
- Data handling/processing
  - [Korean War THOR Data](https://data.world/datamil/korean-war-thor-data/discuss/korean-war-thor-data/mfqtmyju)
    - Unfortunately only appears to cover 77k tons out of an
      estimated 600k tons dropped during the war. I don't
      think this will make much of a visual difference
      (thousand)
  - [GADM dataset](https://gadm.org) was used by Digital NK
    for the shape of North Korea
  - Three files
    - Dataset of bombings
      - coords
      - number of people affected, and in what way
      - party dropping the bomb
      - bomb size/kilos
      - date
    - Korea/China/Japan backdrop
      - full shapes for fast connections
      - simplified shapes for slow connections
      - Both should be prepped as TopoJSON
- Data providing/server setup
  - Try to get data down to less than 300kb
  - _Stream_ shapes from public folder
    - Before streaming, check
      `navigator.connection?.effectiveType === '4G'` and
      fetch the right dataset
      - [this API isn't supported in firefox, IE, Safari/Safari IOS, Opera Mini](https://developer.mozilla.org/en-US/docs/Web/API/NetworkInformation/downlink#browser_compatibility),
        I'm guessing for privacy reasons
      - It does work on Chrome, Edge, Opera, Android native,
        chorme android, firefox android (?), and some
        others, and we aren't doing anything malicious so I
        think it's fine
      - If the user's browser doesn't support connection
        detection, use
        `const loadTime = window.performance.timing.domContentLoadedEventEnd- window.performance.timing.navigationStart;`
        instead
    - Reference: https://github.com/mbostock/shapefile
  - The actual bombs dataset
  - Save with `.json` extension to make sure netlify gzips
  -
