This module exports state objects for our app so that state
or updaters don't have to be passed down the component tree.

There's three major reasons I'm using a plain object instead
of a svelte writable (or any other) store:

1. A lot of areas that need access to state are d3 functions
   and not svelte functions. Svelte writable stores don't
   provide any magic in those vanilla typescript
   environments; they're mainly useful for triggering DOM
   updates which I don't really have

2. The state in this app are large data objects, to the tune
   of thousands of objects. Things that need access to those
   state objects have to use <store>.subscribe(<callback>)
   signatures, where the callback is invoked every single
   time the data changes. These aren't needed with vanilla
   JS/TS object mutation

3. Besides large data objects, the state involved are
   actions like "start animation" that are written once and
   invoked in many different places (and never overwritten).
   This also doesn't benefit from Svelte architecture.

So a simple JS object will do :)
