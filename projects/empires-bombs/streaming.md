2021-12-29

Streaming data looks to be a bit more complicated than I
thought it would. Two major thoughts:

- There's no convenient streamer for TopoJSON, because it
  compresses by having stuff at the beginning of the file
  depend on stuff at the end of the file etc. IOTW it's not
  separable into convenient streamable chunks
- The solution would be to convert this to newline-delimited
  JSON where each line is a feature. But this is also hard
  for the case of the country dataset because each feature
  is a whole country's boundaries, which I'd want to split
  into separate chunks.

OK! After some messing around the solution is as follows:

- organize a single NDJSON file for each level of grouping
  (country, main, region, subregion) (this should be changed
  into country, region, subregion, district)
- in each file, a single row represents a topoJSON object as
  a single polygon
- this means that a single group (e.g. "china" for
  countries) may be represented by more than one item

2022-01-01

The above was a bit complicated to put together but it seems
to work fine now. Whew.

The actual streaming I wrote up today, with heavy reference
to the GeoJSON and Fetch specs/MDN docs, as well as
[can-ndjson-stream](https://github.com/canjs/can-ndjson-stream/blob/master/can-ndjson-stream.js)
which I essentially copied and then modified to fit my
purpose more directly.

The coolest thing here I think is that I don't need any
event handlers or anything to update the data as d3 draws
it. It all happens through the magic of copy-by-reference.
The logic looks a little bit like this:

```javascript
// App.svelte

<script>
import {timer} from 'd3-timer'
const data = []

function streamer(){
  fetch(...).then(response => {
    <streaming logic here>
    ...
    data.push(new item)
  })
}

function drawFrame(time, data){
  <Canvas 2D logic>
}

function render(data){
  timer(elapsed => drawFrame(time, data))
}

streamer()
render(data)

</script>

...

```

d3 draws frames as fast as possible by way of d3.timer,
which uses requestAnimationFrame() under the hood. The data
object it draws from is just updated via data.push(_new
item_) without any special logic needed to trigger an event
handler. The drawFrame() function always, always has access
to the most recent copy of the data.

A similar process can be done with JS objects:

```
const obj = {

}

function streamer(){
  obj.new = 'hi'
}

function render(data){
  ...
}

render(obj)
```

Every time render runs, it has access to a live and updating
value of the data object -- at first it won't have the "new"
property, but as soon as streamer updates it, it will.

It's like magic!

---

The one major problem I have with this though is that it
requires a lot of data to be unpacked into memory, because
of the topo -> geoJSON conversion. I don't think this will
be an actual problem but I might have to find a way to
release them and then handle user updates with things like
an offscreen Canvas rather than redrawing from data.

---

2022-01-03

The streaming aspect works great, but the one change I'd
like to make is to put the precomputation of paths as
referenced [here](./perf.md) into the streaming function
too. Right now the paths precomputation occurs synchronously
inside of a d3.timer call, aka inside of a
requestanimationframe, to the effect of the user potentially
experiencing a laggy start to the animation. This could be
offloaded to the streamer so that it can be done
asynchronously, even though it's not explicitly a streaming
task
