- [x] package up streamable NDJSON files
- [x] get decent shapefiles for rivers, mountains, other
      natural boundaries
- [] prepare dataset of highlighted points as a CSV
  - han river
  - 20 major cities total
- [] create svelte store for map start/stop actions
