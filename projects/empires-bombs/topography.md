2022-01-01

I spent way too much time today trying to sort out
topography :')

I realized that to make the map look halfway decent I needed
to add some styling within each country's borders, and not
simply draw lines

So I decided to add some topographical styling! But this was
difficult, because d3 isn't made for printing raster data
and there had to be a conversion process to vector data
first.

I started with
[this blog](https://yangdanny97.github.io/blog/2020/11/26/D3-elevations)
, which was pretty good if still a bit lacking in style, and
eventually I found
[this](https://bl.ocks.org/hugolpz/6279966) and that was
even better (and looks fantastic)

The first blog led me to
[Open topography](https://portal.opentopography.org/datasets)
, which I guess is sort of a king of topography data. But I
still feel like data availability is lacking LOL I had a
hard time finding medium-quality data fit for my use, which
wasn't to use the raster data directly but to make vector
polygons out of them. Eventually I found
[GMRT](gmrt.org/GMRTMapTool/) linked on that site, and
decided to use it.

There were many more small problems that came up because I
wasn't sure how to process it with R. The terra package was
made for this and I had recently saw it online as the more
modern successor to the raster package. But it adheres to no
syntactical grammar so it was a bit clunky to use.
Additionally some methods to manipulate sf data as usual
were difficult to work with.
