12-29-21

- got a working version of the graph up and running. i think
  tomorrow i'll add a bit of interaction and styling, and
  then leave this project for now and work on the korea one
  instead

some notes:

- for d3.forceSimulations, nodes have to be specified before
  links
- links that point to a missing node breka the whole
  simulation. so some verification should take place before
  the simulation runs
- 60fps still baby
