## assemble.R

**Question 1 Why separate 2001 and 2007 with 2013 when
importing cds data from “data/raw/psid_imports/cds/”? Line:
59-98**

- The psid usually identifies people with a 1968 interview
  number, which tracks a single family based on that single
  ID from the origianl 1968 interview. I preferred using
  this way because it only required two columns from a
  single person instead of a unique ID for every year that
  I'd have to keep track of. 2001 and 2007 had this; 2013
  either didn't have this or it only had the interview ID
  assigned to this family in 2013.

  That was a little annoying but my workaround was to create
  a crosswalk mapping 1968 interview IDs/numbers with
  numbers in every following year -- so I took out from this
  crosswalk a map between the 1968 interview IDs and the
  2013 interview IDs.

**Question 2 There are different unique intnum in
psid_public_ind.RDS and family data from
“data/raw/psid_imports/fam/” Line: 102 – 131**

- In public_imports.R, I seem to have created two separate
  columns for interview ID; one specific to 1968 that was
  labeled intnum.1968, and one for each year labeled simply
  "intnum".

  I'm not sure where the discrepancy arises or what exactly
  is meant here ...

Question 3 Need to modify the income variable Line: 169 -
173

## public_clean.R

**Question 1 Why need to adjust for blood_press =
(blood_press | hypertens)? Line: 21 – 13**

- The PSID uses "high blood pressure" and "hypertension"
  synonymously for their cross-year indexes, but when
  reading the documentation I labeled them separately
  because I thought they were different diseases. This line
  accounts for this by using blood pressure if hypertension
  does not exist and vice versa.

  See https://simba.isr.umich.edu/cb.aspx?vList=ER15458

Question 2 Related to calculating chetty’s chetty_psid_inc
Why filtering age from 30 – 35 instead of 29 to 32 Why
calculating psid_income \_24 filtering age from 22 to 26,
what about the half psid_income? Starting line 99

Question 3 Any_condition: wasn’t coded in the new_control
dataset and confused of coding Line 310

- i should add new variables from the TAS

Question 4 Bingetas should be coded in continuous variable?
Line: 476

- Yes, this is a coding error.

Question 5 Why the variable: sexprotectiontas only has 0 and
9 values in initial cleaned file? Line: 482

- Hm, I can't figure this out but it's definitely my
  mistake.

Question 6 New_control variables don’t eliminate the NAs
Line: 510

- remove missing values (9, -1) before asserting if
  (variable == 1) etc

## restricted_clean.R

Question 1 Why import IND2017ER? Couldn’t find the codebook
for IND2017ER Line: 73

- I believe this was just to have a crosswalk matching
  interview IDs and person IDs. Code could probably be cut
  down because it appears to be the same intention as in
  assemble.R and public_import.R.

  The dataset was found in P:/public/ind/2017; the data and
  the codebook itself arent' there anymore thought because
  the PSID appears to have updated it with 2019 data. I
  saved it to a file on our own folder before that happened.
  So this could be updated. IND2017ER was just a sort of
  all-encompassing dataset that had data for all people for
  all years for all variables (for individual-level
  variables), it was useful here for getting that crosswalk.
