## colors

- do the colors suggested have semantic names? E.g.
  navyblue, aquablue, ... how can we differentiate between
  the blues here?
- What about semantic meaning? When should we use the first
  blue first the second or third?
- What about non-blue colors? I often turn to red for more
  serious or urgent tasks; does that have a place in our
  project?

## typography

- What about subheadings? What size should they be?
  - Websites should in generally only have one h1 tag, or
    title, for accessibility -- it's hard to tell for screen
    readers how one page differs from another when there's
    more than one h1. So some use of subheadings are
    necessary for us to distinguish and label content.
  - We also have a lot of different types of text too -- the
    buttons at the top, the buttons in the Intro segment
    (which is styled at 19pt on the Figma doc), the image
    captions, the block quotes and their captions.
- I'm cautious aobut loading any more fonts than necessary,
  because fonts are a relatively large asset and can take a
  while to download. Even on a stable and fast internet
  connection and with a small set of fonts, users can
  experience a layout shift, or a Flash of Unstyled Content
  (FOUC; or FOUT for text specifically) when the fallback
  font the user has on their computer is replaced with the
  one our website wants to use. This gets a lot worse as we
  load more fonts, so I would prefer to load just Regular,
  Italic (not included here) and Bold. When would be a
  scenario where we should use Light and other types of
  fonts? Could we just use three weights?

## visual effects

- can the glitch effect be explained? What kind of semantic
  meaning does the effect have, and when should it be used?
  What are the individual components of the glitch effect
  that I can write up as code?
- The glitch effect and the color theme of the map mesh
  together super nicely to make a sort of futuristic theme.
  Can we alter other items we have / add effects in places
  to get that theme back?

## main content

- Items we still have to sketch out are:
  - The sidebar for each location on both the explore and
    guided modes
    - Should we have a citation style or otherwise a
      consistent style for linking out to other resources?
    - Should images be interactive? i.e. link out to a
      full-res version on click?
    - How should attribution be formatted?
    - What should spacing be between each paragraph?
    - Should content be scrollable or paginated?
    - Just a few sketched-up screens could help answer these
      questions
  - How the map differs on the explore and guided modes
  - What the default zoom level should be and how much it
    should zoom in when a location is selected; if this zoom
    level differs for the explore and guided modes
  - What the about, add location, and contact us screens
    look like
- Should we have scrollbars / how should they look? It would
  be easiest to do away with them, but we would need another
  way to indicate that content is scrollable. We could also
  style the scrollbar so it at least looks nice / doesn't
  get in the way.
- Are there general patterns for responsiveness?
  - What should happen when the user hovers over the arrow
    button? Is this different from when the user hovers over
    the buttons at the top? How about when the user hovers
    over an item on the map?
  - What is the area of responsiveness for each item (how
    close does the user have to be to each item for a
    response to be triggered)?
- image sizing: a lot of our images are a lot longer than
  they are wide, so it's difficult to make them take up the
  full width of the containing element without stretching
  several screens height-wise and generally looking not so
  good. I'm going to make each image not take up the full
  width of the containing element, but separate content
  before and after

## Other items

- can we still get an icon LOL it'd be useful for search
  results, social media previews, etc
