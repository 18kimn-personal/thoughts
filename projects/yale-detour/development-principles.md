In addition to the general principles outlined at
[fill in...](), there are a few development guidelines I
thought would serve us well to have in writing. For future
contributors to follow, but also in general to promote
ethics and conscientious web design.

The development principles of this project in a single word:
_clean_.

What does that mean?

## Modular code

For this project, the most direct way this applies is to
wrap all pieces of nontrivial logic in a function (if you're
not sure, it's nontrivial).

Functions allow for reusable and modular code, and force you
to think at least a little bit about the structure of your
code and separate out pieces of logic as you package it into
a function. Even more usefully, a function forces you to
name a piece of logic, bringing a reader of your code
halfway to understanding the logic behind it. Functions
finally allow for JSDoc comments, which in a modern code
editor can give helpful hints to the logic of a function
when you use it.

Functions are named, component-ized pieces of logic; our
website also has named, component-ized UI elements
themselves through the [Svelte](https://svelte.dev/)
framework. This isn't something that really needs to be
addressed consciously by future developers, since the
decisions behind this architecture have already been made.

## Semantic code and user interfaces

By semantic, I mean that we should follow the standards that
the web has coalesced around. These standards are often
unlintable because they don't have deterministic definitions
or because they just haven't been configured, but like
linting they help us coalesce around a common style for code
and user interfaces that make them easily understood by
others.

Some examples:

- Use HTML elements through their official designations. If
  an element is clickable, use a `<button>` element instead
  of a `<div>` with an event handler.^[There is a slight
  tradeoff for this; for example sometimes a item we want to
  make clickable has children inside of it, but HTML button
  elements can't have children. Thinking that accessibility
  is usually more important than completely neat CSS
  solutions, I have used `position: absolute;` to nest
  buttons and attached event handlers to them so that the
  button element can control actions while not interrupting
  the document flow.] Our website should have just one
  `<h1>` element at a time, because there is only one
  top-level heading in a document structure. This is to push
  semantic development, but also because accessibility for
  us should be a first-class option as mentioned in our main
  principles document, and correct HTML element usage is
  necessary for screen readers to read our website well.
- Follow the conventions of the language in writing classes
  and variables; use camel case (myVariable) for JavaScript
  and dash-base (my-variable) for CSS.
- Use arrow functions `() => {}` for only anonymous
  functions; otherwise use
- Instead of manually formatting via pixels or even
  percentages^[Using 'width: 100%; and height: 100%;' is
  okay, but we can try to avoid anything else. `flex: 1;`
  and `width: fit-content;` are our friends here. There are
  still some exceptions, though; our sidebar has a
  `max-width: 60ch;`, and the arrow buttons on it also have
  an explicit width.], use paradigms of CSS (for us, mostly
  flexbox) for layout. In addition to flowing via logic
  rather than position, it usually means our design can work
  across many screen dimensions.

## Consistent formatting and code style

Besides modular and abstracted code, the sort of cleanliness
of code also rests on several more trivial things. It's very
helpful to use a consistent level of indentation, for
example, and to create new variables rather than redefine
old ones when possible. A consistent style of code lets our
brain learn a style once and focus on content after that,
instead of having to constantly adapt for every new function
a different way or writing or expressing similar logic.
Abiding by certain practices can also help us eliminate any
bugs we might run into.

Many of these settings can be found in our `.eslintrc` and
`.prettierrc` files at the root of our project, which refer
to configurations for the linter ESLint and the formatter
Prettier.^[A linter is a tool that helps enforce code
practices, often automatically.] When the project is opened
in an editor that has ESLint and Prettier installed, it'll
automatically warn you if your code style doesn't match the
rules described in those files, and many editors even
support formatting-on-saving so that these can be done
near-automatically.

## Zero extra stuff

As much as possible, our project should avoid dependencies
and especially runtime dependencies. We don't need to ship
any more code than is required for our application; clean
means no bloat. As part of this line of thought, we're
currently using [Svelte](svelte.dev/) as the engine behind
our development.

The .eslintrc file has more rules;\

## Ethical, sustainable, and responsible code

All of the points above center around code being "clean."
But most of our development principles are inherited from
those discussed in our general principles document, which
are far more important than the above relatively trivial
points. It's good to have a clean code style; it's necessary
and unavoidable to do so ethically and with a

This section details some simple ways we've tried to orient
our site towards this function.

### React and Svelte

We used to use a JavaScript library called React as our UI
framework and (briefly) `yarn` as a package manager. We've
stopped using those for several reasons, but important of
these are because they're made by Facebook. It's impossible
to completely rid ourselves of the sins of capital, but it's
also very easy to simply stop using products that let a
bloated, bililionaire zombie company maintain a titan
position in the web development world.

Instead of React, we use Svelte, which I feel represents a
more sustainable future for web development. Unlike React,
Svelte relies heavily on a compilation step before the
website is deployed, meaning we use it to write our code,
have Svelte transform what we write, and then send that
transformed result to the user as our website. Svelte, the
JavaScript framework itself, is never sent; this is not so
with React, which always has to be sent to the end user for
a React website to work properly. The compiled nature of
Svelte projects also lets us avoid a tool called a virtual
DOM that React requires, which lets React run faster than
other potential systems but in itself is also unnecessary
overhead.

On a modern machine, the smaller bundle size of our project
and bypassing a virtual DOM mean next to nothing. The
difference of a few hundred kilobytes for a website to load
or a bit more computation for a website to work don't matter
if you're on a high-speed internet connection with a
powerful computer. You probably wouldn't even notice the
difference. The only areas they actually matter are for
those with "slow" machines and those with low bandwidth
connections. But that's sort of our point. That's why this
-- care for the performance of our website -- matters.

More generally, this is what sustainable web development
means. As small as possible, as maintainable as possible,
with just enough code for our website to run and nothing
else needed. And at least more independent than React.^[This
one is hard to reason with. Svelte was first made by Rich
Harris while he was a journalist at the New York Times, with
the participation of many others in the open-source
community. In that sense it's almost a true independent
project. Harris is now at Vercel to work on Svelte
full-time, though, and believes that Vercel's support for
Svelte (read: money) can help Svelte a lot with development.
So I don't want to position Svelte as exactly the greatest
ethical framework ever. But that ideal doesn't exist,
really. I think Svelte is great enough.]

Finally, Svelte also ships with several accessibility
checkers built in by default. It discourages you from
writing autofocused elements, for example, or writing image
elements without alt text. The caveat is that this is
limited and there's a lot Svelte is still implementing, and
even when those checkers are complete there are many things
Svelte (or any automatic tool) cannot enforce that we need
to be careful of ourselves.^[See more of this discussion at
https://geoffrich.net/posts/svelte-a11y-limits/ ]

### Hosting and deployment

One way I feel we could improve and do things _the right
way_ lies in deployment and hosting the actual website. We
currently host our code on GitLab, which I like as a company
and I feel has less...compromises on its open-source support
than GitHub.

Our website is hosted on Netlify, which is set up to watch
our GitLab repository; when GitLab receives an update,
Netlify sees that and rebuilds our website with that new
update. It also produces preview deploys with throwaway URLs
for branches that aren't the main branch, which is
incredibly convenient for me for sharing updates. This is
especially so for a project like ours, where it's so
valuable to not have to pull down a branch from GitLab or
anything relatively technical.

Netlify has its servers on AWS, though, and more fundamental
than that, relying without question on GitLab and Netlify
mean that the deployment of our site is taken out of our
hands. A tiny bit of our independence and control in the
project is lost.^[I recognize that this might sound a bit
dramatic to any other web developer, as hosting platforms
like these are kind of a given / indispensible for getting
something published.]

One solution would be to set up a system on GitLab that does
this for us like Netlify does, but from my initial read of
the docs this could be a tad difficult (Netlify prides
themselves on being incredibly easy to use) and not
something I really have experience in. It would be even
better to have our own hardware that actually serves this
content, but I feel this is out of our scope.

### Overlaps

The ethics/responsibilites/sustainability of our code
overlap quite a bit with the principle of clean code at the
beginning of this section. Semantic UI for accessibility is
part of responsible code design. Minimizing dependencies is
part of sustainable code. Ensuring our code is annotated and
organized into modular components is part of sustainable
code. It's all connected !!
