I have been frustrated about this for a while, and have
changed my mind on how things should be done each time, but
I've finally decided on what to do!

The issue was that we need a balance of human-readable and
machine-processable content. I think Collin used the Netlify
CMS for this, which I'm not really a fan of for us because
it requires us to depend on another service than we probably
need. Currently all of the content is stored in markdown
files in the `src/` directory, and pulled in at runtime with
a weird `require.context` call.

I'm nt really a fan of this. It's hard-to-read code and it
requires developers to manually edit markdown files
themselves. It also makes the process a bit less transparent
than it needs to be, because the back-and-forth of editing,
revising, markdown-ifying content is kind of kept inside of
the Github repo or in other conversatinos.

The solution to this I'd like to turn to is to use a Github
(or a Gitlab) wiki, pull down the wiki and process the
markdown files into a single JSON as a build step, and send
that JSON file over the wire via vanilla fetch during
runtime. Hopefully this can gzip well.

This:

- Lets us have publicly viewable and even editable markdown
  files
- Lets us avoid having a markdown parser as a runtime
  dependency
- Lets us use vanilla fetch to serve content, which avoids
  the sort of obtuse `require.context`/`import.keys` (I
  forgot the syntax already) calls, and we don't have to
  resort to Vite's `import.meta.glob` calls.

Disadvantages:

- Will take a few hours to set up
- Is another addition to our codebase, which may be
  unnecessary
- Probably no one will work on the project after us, so it
  may be a waste of time trying to get the dev process
  completely right
