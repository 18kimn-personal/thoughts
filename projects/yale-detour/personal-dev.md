---
date: 2022-01-08
title: logic of the app
---

I'm having a hard time keeping track of and cleanly managing
state in the map, so I've decided to try to write in english
everything that happens in our website. There are so many
pieces of interdependent code and it is hard to separate
them into modular functions.

Before user interaction:

- the map is fetched from mapbox and displayed
- routes are fetched
- location text is fetched

Event: The user first enters the map

Effect: This is equivalent to either the switch from guided
to explore or vice versa

---

Event: The user switches from 'guided' to 'explore'

Effect: All locations are displayed on screen. No routes are
displayed on screen. currentIndex is set to undefined.

---

Event: The user switches from 'explore' to 'guided'

Effect: All locations save the first are hidden from the
screen. No routes are displayed on screen. currentIndex is
set to 0.

We can hide all locations since the first will be shown when
currentIndex is 0.

---

Event: the user clicks an arrow button

- update locatoin to current index +- 1. Merge with this
  next event ->

Event: the user clicks a location on the map
