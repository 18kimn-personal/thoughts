---
created: 2022-02-26
modified: [2022-02-26]
title: Strategies for responsive design
tags: [workflow]
author: Nathan
---

I've been having some trouble getting the site to work
nicely on mobile. It's almost there, just the sidebar (or
content area) is the main problem

there are sort of two main issues i was grappling with:

1. on mobile (or otherwise vertical), the viewport should
   switch from vertical to horizontal

This was a bit difficult because I had first done it the
"right" way, using `flex-wrap: wrap;` and letting the
browser CSS engine figure out when to wrap.

The problem is that I need to know when this happens in a
programmatic way, so I can move arrow buttons to the bottom,
so I can set up the below swipe gesture, etc. This isn't
possible with the `flex-wrap` way, because this is CSS
(doesn't fire a JS event) and is not always transparent at
what pixel (or otherwise calculable) value this happens at.

So I redid this simply as a media query, but I think I'll
write it one more time as a resize observer thing so I can
stuff even more JS logic in there, and because that's how I
did things in other places in the site. It also means that I
don't have to repeat a pixel value (e.g. use `900px` for the
media query and then also write `900` wherever i need to
know if the media query has triggered), hopefully making my
code a little more DRY.

The drawback is that a CSS-only solution would have been
best, and I am giving up on that completely.

- when vertical, the content area should be small and then
  expand on click (or swipe)

Sigh. This was also hard.

I think what i'll do is, when column, to always have the
content area be three divs:

- header (height min content)
- body (flex: 0.001 and then moved to flex: 1)
- buttons at bottom (height min content)

And then transition the flex property when the user clicks.
I hope this doesn't trigger many layout shifts and can be
done smoothly :')
