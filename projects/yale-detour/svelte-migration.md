checklist

- [x] install svelte via vite
- [x] remove everything react (will break a lot of things.
      good!)
- [] transition components - first pass
  - [x] index (now main.js)
  - [x] home
  - [x] intro
  - [x] guidedcontext -> store
  - [] SiteNav
  - [] About
  - [] Sidebar
  - [] Location
  - [] Map
- style everything again
