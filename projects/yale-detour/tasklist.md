---
created: 2022-02-28
modified: [2022-02-28]
title:
tags: []
author: Nathan
---

- [ ] close buttons on navbar
- [ ] updated icon
- [ ] tooltip above marker
- [ ] "tap to expand" disappears but the height it takes up
      does not
- [ ] left/right arrows aligned with "expand" arrow in style
- [ ] on location change, "Expand" arrow should not rerender
- [ ] clickable items have fade-in gray background on mobile
- [ ] wording in "about" is updated to condense current
      intro and include
  - source code link
  - short history of project and its various iterations
  - future ambitions (though we consider the project now to
    be at a stable phase, we do hope to update and add sites
    as tiem goes on...)
