---
created: 2022-02-26
modified: [2022-02-26]
title: It takes a really long time to get something polished
tags: [workflow]
author: Nathan
---

I've spent all of today working on the mobile version of the
Yale Detour site. It works, it's readable, but
also....ARRAWGHWAIEFHWAIEHF IWAEFAWIFEWAEF

I'm so frustrated by how long it's taking to transform a
simple design into a production/publication-ready site

issues that have been solved today, that don't relate to the
proof-of-concept "does it work" bit but specifically to the
UX/polish

- vertical mode
- in vertical mode, an expanded and collapsed mode
- better and shorter introduction
- fixed strange marker rendering on mobile
- author and image attributions

design things that have yet to be resolved

- map can't rotate fml
- popup covers marker too much
- guided tour vs explore mode doesn't make sense
- arrow icons don't match
- content area should be draggable/swipeable

and these are just the things that I feel I have
responsibility to fix, there are so many language and
content things I want to address that I will trust to those
whose job it is to implement that

---

this is a major project that i feel frustration on this
issue over, but the same feeling applies to many other
projects. my thesis, papers with emma zang, other side
projects, evictorbook. things take a verys hort amount of
time to show promise and execute so that the conceptual
value shines; things take a very, very long amount of time
to be publishable
