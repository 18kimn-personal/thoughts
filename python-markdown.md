---
created: 2022-05-01
modified: [2022-05-01]
title: A good markdown system in python does not exist
tags: [python]
author: Nathan
---

At the very least, my requirements are:

- follows commonmark
- is extensible // lets you interact with the lexer and
  parser
- has a community itself to be actively developed and
  maintained

the package Markdown is the best and what I want, but...
doesnt even follow commonmark spec and its extensibility is
not as good ads markedjs
