---
created: 2022-05-01
modified: [2022-05-01]
title: Python packaging
tags: [python]
author: Nathan
---

Another thing I immediately looked for in python were the
equivalent of NodeJS' `package.json` and `package-lock.json`
files that allow reproduciblity of packages, and in
text-based ways (e.g. no `.tar.gz` shenanigans are needed)

Python has no real equivalent. Pipenv and Poetry seem to be
popular, but are heavily criticized for either being broken
or incomplete. Conda seems to be robust and the "correct"
solution, but it doesn't have a mechanism for auto-updating
itself (e.g. once you add a new dependency to the project it
doesn't auto-update),

Some relevant links:

[medium post comparing them](https://ahmed-nafies.medium.com/pip-pipenv-poetry-or-conda-7d2398adbac9)
[reddit post showing pip+virtualenv's the most popular](https://www.reddit.com/r/Python/comments/i41dqq/pip_pipenv_poetry_or_conda_which_one_do_you_use/)
[best guide to virtaul envs i've seen, comes out pro-conda](https://whiteboxml.com/blog/the-definitive-guide-to-python-virtual-environments-with-conda)
