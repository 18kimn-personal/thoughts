---
created: 2022-05-01
modified: [2022-05-01]
title: Troubles getting started with python
tags: [python]
author: Nathan
---

I did all of my personal utility stuff in nodeJS, but
thought that it could be more ergonomic to do things in
python, given that... javascript is a web language. I should
not have thought so. haha. here are some examples of
non-ergonomic-ness:

- [markdown](./python-markdown.md)
- [package amangement](./python-packaging.md)
