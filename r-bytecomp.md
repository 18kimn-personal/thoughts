---
created: 2022-01-09
modified: [2022-01-09, 2022-02-05]
title: Byte compilation for R
tags: [perf]
author: Nathan
---

From
[The Shape of Code](http://shape-of-code.coding-guidelines.com/2021/05/23/where-are-the-industrial-strength-r-compilers/):

> The fact that it is probably not worth the time and effort
> has not stopped people writing compilers for other
> languages, but then I think that the kind of people who
> use R tend not to be the kind of people who want to spend
> their time writing compilers. On the whole, they are the
> kind of people who are into statistics and data analysis.

That about captures it. Here's some more, though:

> The essential ingredient for building a production
> compiler is persistence. There are an awful lot of details
> that need to be sorted out (this is why research project
> code does not directly translate to production code, they
> ignore ‘minor’ details in order to concentrate on the
> ‘interesting’ r esearch problem). Is there a small group
> of people currently beavering away on a production quality
> compiler for R? If there is, I can understand being
> discrete, on long-term projects it can be very annoying to
> have people regularly asking when the software is going to
> be released.

This is a super insightful blog.

the last point the author makes is that probably not many
people will care because in R people aren't relaly concerned
with performance. I'm concerned about performance quite
often (e.g. spatial stuff) but also people might care about
the compiler because of WASM reasons nowadays.
