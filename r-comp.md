---
created: 2022-01-09
modified: [2022-01-09, 2022-02-05, 2022-02-26]
title: Compiling R
tags: [perf]
author: Nathan
---

I've been thinking this would be really cool, to run R on
the browser without 1) a server 2) clients needing R on
their computers.

Unfortunately this seems pretty far from happening. R is
written in R, C++, and Fortran. C++ and Fortran-to-WASM
compilers already exist, but the problem is made difficult
for R in that R does not translate directly to either C++ or
Fortran but instead is running on a virtual machine on
compiled versions of both. R itself has just-in-time
compilation, but to bytecode, not assembly.

Still, it would be cool to do this. I'm going to keep
reading and trying to find work on this.

---

I think the best way would be to use the GCC front-end to
compile an R source file. I'd leverage the R str (similar to
lobstr) to generate an AST, then pass that into the GCC
front-end, then from that either generate WASM or native
assembly using the GCC backend or LLVM.

---

first tasks:

- start here

https://www.llvm.org/docs/tutorial/MyFirstLanguageFrontend/index.html

- download and read through R source code

---

main benefits:

- practical
  - r on the browser and in other targetable settings
  - squeezing performance out of R
- conceptual
  - investing in R low-level, which is pretty unusual for
    the R ecosystem where user contributions mostly come in
    either packages, R-embedded formats (Yihui Xie's work,
    Shiny), or IDE tooling (RStudio, rlanguageserver in
    VSCode)
  - forcing a separation between R specification and
    implementation, by providing a completely different
    implementation
