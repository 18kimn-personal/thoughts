---
created: 2022-01-09
modified: [2022-01-09, 2022-02-05]
title: Rcc, a compiler for R
tags: [perf]
author: Nathan
---

There's also RCC (after 'GCC'), a master's thesis.

Unfortunately it seems to have been mostly academic, as
Shape of Code noted.

https://scholarship.rice.edu/handle/1911/17678

http://www.hipersoft.rice.edu/rcc/
