---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: readings on open source licensing
tags: [ip]
author: Nathan
---

[https://ethicalsource.dev/resources/](https://ethicalsource.dev/resources/)

some references to [Hippocratic](./Hippocratic.md) and
[996](https://github.com/996icu/996.ICU/blob/master/LICENSE)
among others

oh they actually made the hippocratic license
