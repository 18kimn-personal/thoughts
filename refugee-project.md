---
created: 2022-03-09
modified: [2022-03-09]
title: some thoughts on the refugee project
tags: []
author: Nathan
---

i originally had this in an email, but decided not to send
it bc it wasn't relevant. I'll put it here for now !

My main impression of The Refugee Project's map is that it
looks beautiful but it's a bit complex. It lags quite a bit
on my computer because of this, which I found strange since
my machine is pretty powerful. Their map is also a bit
confusing to use because of everything it does. Animations
make moving the cursor around the map a bit of sensory
overload as things flash in and out, the cursor inexplicably
becomes a "zoom in" icon sometimes, and the use of circles
to represent refugee migration numbers can be a bit annoying
as I clicked the wrong country a few times when trying to
find what country a circle was associated with. This isn't
to criticize the project itself, but just to say that
complicated things can be hard to manage. I think for our
map that focusing on something relatively simple could be
helpful -- focusing on one country helps a lot here, and I
also plan to go easy on the animations and colors.
