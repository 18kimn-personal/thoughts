---
created: 2022-08-29
modified: [2022-08-29]
title: Repressing feelings
tags: [personal]
author: Nathan
---

Personal but not that personal

Only a (cis, het, upper middle class, nonblack) man could
write that repressing feelings is good. but to me it has its
uses. repressing feelings is a controllable action, if you
can repress your (intrusive, self-harming, destructive)
feelings and push yourself to do other (healthy, healing,
productive, caring) things, then you feel better afterwards,
and the need to repress is gone

of course, it doesn't always work like that. sometimes
repressing your feelings doesn't mean you can find the will
to do other things. finding the will to do other things
doesn't mean you feel better afterwards. Sometimes you must
rely on things that are not You, you must rely on your
friends, your partners, your SOs, your mentors, your family,
the internet, just watching content, other sources and
agents, to lift you up. sometimes you cannot repress at all,
sometimes repressing doesn't solv eanything, sometiimes you
repress and that has its caveats.

but personally... im the kind of person that feels easily
rewarded. i am proud of myself if i am able to brush my
teeth, if i am able to clean, if i am able to cook or eat
something, if i am able to write one paragraph, one note,
one script, one email. And that pattern of having a feeling
of reward is something i hold onto for the above reason ...
it means i can control, somewhat, my mood swings,...make
myself rise when I feel I cannot lift myself...
