---
created: 2022-03-23
modified: [2022-03-23]
title: Resets (do we need 'em?)
tags: [trajectory]
author: Nathan
---

(too lazy to find links for this)

Do we need to reset our computer? People conventionally say
it's healthy, but for linux people it's a pride to never
have to manually restart our computer, and on most distros
things just continue to work. Stability ~~~~

Do we need to reset our human selves? I am in constant
search of true stability, not knowing whether I am in the
right place or if I need to tear down and change direction.
Perhaps I should invest in where I am right now; perhaps I
should find something that is worthwhile to invest in. I
don't want to speak in more concrete terms lmao because this
note is a public forum but yeah. Friendships, work, school,
future trajectories, time in Korea, ...

Do we need to reset our workflows? A more trivial example
somewhere in between the two above perspectives. Do I need
to switch my Linux distro to gentoo (after doing so I had
decided the answer was _no_)? Do I need to learn a new
window manager or even just a new i3 config? A new vim
extension? Vim at all?
