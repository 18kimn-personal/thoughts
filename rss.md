---
created: 2022-02-07
modified: [2022-02-07]
title: coding my own rss feed
tags: [workflow]
author: Nathan
---

decided to code up my own rss feed to make me feel something

IMPLEMENTATION

---

SITES

https://www.mappingasprocess.net/?format=rss
https://yihui.org/en/index.xml

https://hnrss.org/frontpage?points=100
https://flowingdata.com/feed
https://dukeupress.wordpress.com/feed/

https://third-bit.com/atom.xml
https://junkcharts.typepad.com/junk_charts/atom.xml
https://harvardpress.typepad.com/hup_publicity/atom.xml
