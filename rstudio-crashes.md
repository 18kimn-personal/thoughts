---
created: 2022-02-15
modified: [2022-02-15, 2022-03-14, 2022-04-08]
title: RStudio crashing ???
tags: [linux-tips]
author: Nathan
---

struggled with a bug for the past few hours with tring to
get rstudio to work. It used to work, I probably ran
`pacman -Syu` too carelessly, and then it doesn't work
anymore. This note is a bit of venting and a resource for
future me in case this happens again.

The only error message was incredibly uninformative (found
in `~/.local/share/rstudio/log/rsession-nathan.log`):

```
2022-02-15T06:28:21.402367Z [rsession-nathan] ERROR Parent terminated; LOGGED FROM: void {anonymous}::detectParentTermination() src/cpp/session/SessionMain.cpp:1443
```

The key for me is:

```
export RSTUDIO_CHROMIUM_ARGUMENTS="--disable-seccomp-filter-sandbox"
```

(addendum 2022-03-14: thi sis in /usr/bin/rstudio-bin)

I don't know why??? I don't know what seccomp is, or why my
chromium is breaking this (heck I didn't update chromium,
it's just in the rstudio install)

There are also some other env vars that seemed to solve the
issue for others but did not work for me:

```
QT_XCB_FORCE_SOFTWARE_OPENGL=1
RSTUDIO_CHROMIUM_ARGUMENTS="--disable-gpu"
```

This kind of fickleness, this unreasonable error --
unreaosnable because I dont know what it means, there are no
good resources to find what it means, and even the solution
is uninformative -- is what I want to not have. sigh.
