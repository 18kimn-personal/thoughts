---
created: 2022-01-31
modified: [2022-01-31, 2022-02-15, 2022-03-14]
title: RStudio's business model
tags: [big_tech]
author: Nathan
---

https://www.rstudio.com/blog/rstudio-pbc/

https://www.rstudio.com/assets/img/451_Reprint_RStudio_02DEC2021.pdf

A PBC has in its charter that is is meant to serve the
public -- the society, workers, the community and the
environment.

Notably this is pretty recent, im not sure exactly what date
but all the states that passed in did so in the 2010s,
RStudio said in their announcement that they couldn't
incoprorate as a PBC until "recently," and the actual
announcement came in July 24 2020.

I'm a fan :') perhaps because I trusted RStudio even before
this happened

I don't know if the PBC has explicit incentives or
punishments tied to its commitments, and the only actual
commitment I can see is a yearly report describing the
pbulci benefit they've created.
