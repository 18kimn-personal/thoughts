---
created: 2022-02-11
modified: [2022-02-11]
title:
  two reasons why I see rust as a good language for
  beginners
tags: [standards]
author: Nathan
---

By "beginners," I mean only those people that have little to
no prior experience in systems or other low-level
programming

1. The cargo tool is so, so useful for understanding errors,
   much more so than toolchains in the C/C++/Go ecosystem
2. Rust's type safety make it a language that should succeed
   other systems languages, and that succession is only
   possible if we make it a de facto first language to learn
