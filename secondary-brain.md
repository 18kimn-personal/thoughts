---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: A second(ary) brain
tags: [zettelkasten, obsidian, sustainability]
author: Nathan
---

continuation of
[links as first-class citizens](./links-firstclass-citizens.md)

One thing I thought of while walking down the stairs to Bass
Library is on the bridge between "links are cool" and "the
zettelkasten system, of which links are one part, in its
entirety can become a sort of second brain." This entry is
an attempt to hold some of those thoughts.

### second brain

What "second brain" means is in my view pretty simple.
Systems that function as a "second brain" means that a given
system 1) holds thoughts and 2) holds connections between
thoughts. Obsidian and the Zettelkasten system are simply
methods for holding the thoughts we've (our first brain)
already had. When our brain forgets thoughts, because
thoughts are fleeting and leave our mind, we've also stored
them in a second area (our second brain) that holds them.

### sustainable tech

The second thought is a little bit more complex. In my view
how Obsidian serves as a tool is _exactly how all pieces of
technology should serve humans_ -- to augment our existing
abilities using things we understand and can modify as we
please.

Obsidian and the Zettelkasten systme are not "second brains"
as in they are GPT-3-trained algorithms or neural nets that
can tell us thoughts we've never thought of. They're just...
reserve areas for our primary brain, a resource to look to,
that can give us information that we've thought of at one
point in the past. Perhaps a better name given this would be
secondary brain, not second brain.

### personally

This provides a better motivation for me of using the
Zettelkasten system. It seems like a lot of work to have to
write down every thought, have to link thoughts together
manually, and so on. But it's really doing the work up front
(while they are in our primary brain) so that our secondary
brain can hold them. Then when they leave our primary brain,
it costs very little to go and retrieve them from our second
brain. So in this sense it saves a lot of work.
