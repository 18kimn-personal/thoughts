---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: Self-hosting?
tags: [big_tech]
author: Nathan
---

Evictorbook and AEMP at large uses a shit ton of AWS
services, despite a general orientation against big tech at
large.

I am daydreaming about a self-hosting project... to move all
of EB, the main website, the covid pledge, and everything
else onto a single physical server...

- it would be a collaborative project that could be
  supported by and used by organizations we are aligned with
  - this would also be useful cost-effective wise
    - self-hosting has a costly up-front fee that means that
      a setup-once-and-reuse pattern is really helpful
  - together apes strong
- it could be hosted at a university or someplace similar
  - the basic electric bills and network needs are taken
    care of ... lmao
  - (if at a public university) we and the public are
    working together
    - is this parasitic? lol
- it could let us experiment with alternative protocols like
  hypercore and others
  - federation versus decentralization: organizing is a
    positive good
-
