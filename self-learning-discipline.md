---
created: 2022-02-26
modified: [2022-02-26, 2022-03-19]
title: Self-learning and its pitfalls
tags: [workflow, trajectory, sustainability]
author: Nathan
---

Most of my technology learning has been in the form of
self-learning, e.g. i have stuff im interested in and I go
and do it

I am learning a lot, a lot of it is useful, and most of all
it is fun for me to do, so even though I kind of hop from
one thing to the next I feel it's an overall sustainable
path (and one that is more or less in line with academia).

the sort of drawback here is that directed learning forces
you to do things you wouldn't otherwise do, often more
difficult or more boring things, in an "eat your vegetables"
mindset that is necessary for growth. I'm engaging with more
low-level stuff slowly by learning rust and this series of
web server projects, but truly low-level stuff (assembly,
embedded machines, compilers besides R) don't interest me at
all and I'll never get around to learning it. I'm interested
in quantitative information but I've been so turned off by
the more marketable and valued skills of analysis and
modeling; perhaps I should get around to _really_ learning
that kind of stuff. Perhaps I need some more discipline
overall.

---

one addendum: even if you are in a dedicated learning
environment (Except in primary/secondary) truly most of your
learning happens becuase of your own action or volition. In
a graduate program i bet this is true even more, but even in
undergrad in various mentorships and learning environemtns
and classes the actual learning happens when you're in your
room by yourself, not because someone guides the student
through eery step
