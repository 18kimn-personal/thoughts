---
created: 2022-02-26
modified: [2022-02-26]
title: What is self learning, anyhow
tags: [workflow, trajectory]
author: Nathan
---

After writing
[self learning and discpline](./self-learning-discipline.md)
I realized that I don't even know what self-learning is.
I've been learning Rust mostly by heavy reference of the
Rust book's examples, the rustlings crate, and blog posts.
Is that self-learning, since I'm learning from resources
people made specifically to teach others?

I'd honestly still say yes, because I know no one personally
either IRL or online who has motivated me to learn rust
(hell I don't even know a single rust programmer lmao). So
how about two more scenarios:

- my javascript journey. I started learning javascript on my
  own, specifically through d3 (which in hindsight is not
  something I'd recommend to others, but whatever) and
  trying to make force network graphs. I really started
  learning when I began working with AEMP and Evictorbook,
  and definitely learned a lot from just reading the code
  and reviewing/writing PRs with others. Does this count as
  self-learning, since my learning with other people I know
  personally is not through resources they've prepared
  specifically for learning? After I worked with AEMP, I
  took a class called coded design, which taught a little
  bit about javascript concepts. They were all things I
  mostly already knew, but undoubtedly gained from through
  repetition and practice and in trying to help others. Does
  that count as self-learning? I'd say probably not.
- my R journey. I started learning R similarly sort of on my
  own, through sitting by myself in that USDA dairy
  chemistry lab. My most valued learning experiences iwth R
  though has been when I had people either in my life (nick
  graetz, camille) or who I repeatedly read the work of
  (wickham/xie/lin pederson/averick) that provided learning
  materials. Does that count as self-learning?

After writing this up, I think it's valuable to reflect on
how I learn but not worthwhile to debate the question of if
something is self-learning or not. Firstly there's no
unambiguous definition of self-learning, and secondly even
if there were, why am I conflicted or wondering if that is
me or not? Learning with or through others is even more
valuable than learning by myself, yet in asking this
question I feel myself valuing the opposite way (glorifying
my own supposed independence, intellect, and motivations).

A simple (trivial) takeaway: I usually start learning on my
own, then eventually find my way into a community that I
treasure because they provide such a wonderful alternative
to my previous learning experience.
