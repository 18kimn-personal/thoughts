---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: Efficiency and self-hosting
tags: [big_tech]
author: Nathan
---

the majority of commentary that exists seems to favor
self-hosting for cost:

- https://www.servethehome.com/falling-from-the-sky-2020-self-hosting-still-pays/
- https://www.reddit.com/r/sysadmin/comments/6ndbf4/cloud_vs_self_hosted/
- https://www.reddit.com/r/sysadmin/comments/6ndbf4/cloud_vs_self_hosted/
- https://exotel.com/blog/aws-vs-self-hosted-dilemma/
- https://gigaom.com/2012/02/11/which-is-less-expensive-amazon-or-self-hosted/

Some pro-AWS stuff:

- https://aws.amazon.com/blogs/aws/be-careful-when-comparing-aws-costs/
- https://www.reddit.com/r/aws/comments/k2lfha/aws_rds_vs_ec2_self_hosted_pricing/

Some in-between:

- https://www.happi.io/aws-instances-versus-physical-servers/
- https://www.reddit.com/r/selfhosted/comments/9u2pkb/is_using_the_cloud_even_worth_it/
  - electricity apparently is expensive in germany lol

Of course, there is not a one-and-done answer for
efficiency, and even for a single situation there are lots
of ways AWS is more "efficient" than self-hosting and vice
versa.

Some conclusions:

- AWS wins for stability (backup and restoration)
- AWS is certainly much cheaper in terms of labor
- AWS wins for scalability
- Self-hosting can be a lot cheaper, but has a higher
  start-up cost

The question of efficiency definitely has to be answered at
some point in the migration process, but it's not really....
relevant to my main thought process. I don't want to
self-host because it's cheaper or eaiser, in fact it's
probably more expensive and harder. I think self-hosting is
important because ownership of our data, our communication
pipelines, and so on is important.
