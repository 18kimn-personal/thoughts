---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: Self-hosted server features
tags: [big_tech]
author: Nathan
---

## features of the server

### services it should have

- storage
- mail
- self-hosted websites
- matrix chat protocol
- gitlab
- node for radicle.xyz protocol
- host for beaker/hypoercore/dat projects

- style guides
- how-tos
- reusable code
- chat/communication platform
-

### things it shouldn't have

- computation
  - configurable scalability for individual users
  - large memory allocation (limited to 4gb or something)
