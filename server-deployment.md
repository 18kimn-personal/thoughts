---
created: 2022-05-03
modified: [2022-05-03]
title: Deploying efficiently from a self-hosted server
tags: []
author: Nathan
---

I want a workflow very similar to netlify:

- I specific a minimal dockerfile that has some commands:
  `npm start` maybe. If someone doesn't want to deal with
  docker then I can set up a simple drop-down or auto-detect
  set of forms to deal with them:
  - auto-detection of npm vs yarn vs pnpm based on
    package-lock vs pnpm-lock vs yarn.lock
  - guessing which package.json script should be run,
    probably "build"
  - guessing which directory should be deployed as static
    files
  - guessing which command should deploy a server
- I push to my gitlab server
- (a few minutes pass)
- website works :D

In those few minutes:

- webhook is sent to my deployment server URL from the
  hosting server URL (which will probably just be the same
  server)
- deployment server
  - how to access these files ??? should I git clone???
  - files are in /var/lib/gitlab/repositories/@hashed
  - ah... somehow access the hash

---

FULL STEPS FOR DEPLOYER

- webhook receives token, git url, hash
- make directory in /var/lib/deploy, where name of directory
  is git hash
  - cd into directory
- either run `docker compose up` OR run
  `pnpm/yarn/npm install` then
  `pnpm/yarn/$(npm run) npm build`

hm maybe i should make a frontend...?
