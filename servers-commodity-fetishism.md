---
created: 2022-03-15
modified: [2022-03-15]
title: Servers and commodity fetishism
tags: [sustainability, web_dev]
author: Nathan
---

I wrote about self hosting servers before too, in
[./selfhost-server-features.md] and [./self-hosting.md] I
think

a realization I had today was that one reason I am in favor
of self-hosted servers etc is because I am opposed to
commodity fetishism, or the misrecognition of relations
between people as relations between thigns (or people and
things).

and the promise of the cloud is to go even further past
commodity fetishism, past obscuring the relations of people
to obscuring the relations behidn the thing itself, to
obfuscate the very physical architecture behind digital
content and stop you from questioning these content delivery
systems

A ton of the modern era of web development has been geared
towards making servers faster, better, more reliable. CDNs
so that if one node goes down, others still server content,
or that the closest/most reliable node can always be serving
content to the user. Complicated pipelines with virtual
machines.

I would like a return to servers as tool for relations
between people. I'm okay with my machine going down
sometimes or with it not being the fastest available deliver
platform, and I even think that's good, becuase that at
least means it's mine, it's my home network, it's my
personal machine. It's akin to messaging platforms: serving
content to you is like I am chatting with you, and if my
internet goes down then of course I can't talk with you
anymore, and that is not really a disaster more than a
slightly inconvenient fact of life.

(trying hard not to sound like a hip design person) When
people see my content, I don't want them to see a website, I
want them to see an expression of myself and an avenue
through which they can understand and respond to me.

---

I think this can also be an argument against distributed
systems / web3 stuff. This premise that we'll have
duplicability, scalability, reliability through amassing
many computers together needs to be taken down from its
abstract form nad begin with relations between people that
emerges into community, not an abstract net that can be a
inherent good on its own. I would even say that certain
things (radicle.xyz for git) don't need to be distributed,
because more "traditional" methods of communication and
relation between single parties is good and preserves that
intimate relation between two parties.

I have to think about this point more though, because a lot
of distributed stuff seems to be pretty good aCkShuLly
