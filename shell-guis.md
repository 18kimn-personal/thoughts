---
created: 2022-08-05
modified: [2022-08-05]
title: GUIs and the modern shell
tags: [workflow]
author: Nathan
---

I wrote previously about how text-based editors were ....
better...?? than GUIs, I think [here](./plaintext-guis.md).
My thoughts on the matter as a whole have tamed for all
sorts of reasons (by not that much, as in I still do all of
my work in a terminal and in vim and in markdown etc), but
there is one point in particular that I think GUIs do much
better at, which is suggesting actions you didn't think of.

If it's your first time in a GUI, there are markers and
words ot help guide you. If it's your first time in a
terminal, you are _completely lost_, there is no cue that
`ls` means "list files" or that `cd` means "change the
directory your terminal is staring at." These are things
that you must learn outside of the program itself. The
terminal requires training. Even once you do become familiar
with those basic commands of the terminal, you are still
left lost in the ocean of commands in domain-specific
development areas. You can use the `man` command, but many
domains of commands don't even provide man entries or even
have good `--help` responses. And even if they do, you still
need to actually think of the command to use it or look it
up; there is very little to guide you in that direction.

The terminal invariably has its strengths because of this,
in that it is concise, can conform to certain standards and
uniform patterns (resulting in proliferation of development
tools on the command line), that it can get a lot done
presumably faster than GUIs can, that because it does not
enforce a certain structure, user-defined structure and
grammar can emerge and ultimately accomplish much. But okay,
in response to that specific point GUIs can have grammars
too and combine things in multiple ways the way terminal use
often is. And that does nothing to solve the fact that the
terminal is incredibly obtuse to use. Can't we have a world
where terminals guide you a bit while still letting you
unleash performant wonder?

I feel like this is the number one problem shells or
terminal emulators should focus their efforts on, especially
in contrast to things like Warp or Alacritty. Terminals
don't actually need to be written in Rust in my opinion,
either for memory safety reasons or for performance reasons.
They also don't need access to cloud infrastructure or
require telemetry. These are non-problems. But terminals
have always needed to be more useful, more intuitive, and
enable faster and more diverse _workflows_ rather than
simply faster execution of commands (that's not even a
terminal problem!!!).

Given the recency of this thought and the fact that I've got
other things on my plate, I hvaen't actually configured this
sort of thing for my own work. But here are some thoughts on
what would be useful here:

- history-based suggestions and completions. Weight by the
  most recently used commands, then use `ag` or something to
  pull out the closest suggestion
- an API for shell commands to provide composable and
  modifiable templates. When I want to recursively search
  for files ending in `.md`, and I have never used `find`
  before, I feel like I should still be able to do this
  without using the internet, and (maybe this is
  controversial) without even reading through the entire
  manual. Understandably referring to the manual is
  considered a positive good, and it is, and I think using
  the internet as a resource (depending on how, of course)
  is also helpful for learning. But you shouldn't need to,
  you should simply be able to pull out a building block set
  that shell commands provide and terminal emulators
  complete.
- Saving workflows. There are times when I repeat a series
  of maneuevers, but the fact is that I still expend more
  brainpower, more keystrokes, and honestly mess up or
  forget things. Like when I start fresh development on
  evictorbook:

  ```
  cd ~/main/work/aemp/evictorbook
  git fetch --all
  git switch <branch-name>
  sudo rm -rf load_data neo4j_data/import-done.flag
  docker compose up
  ```

  That is too many steps for what is essentially a
  repetitive process! I can mess up on the directory access
  if I forget where it is (as I do all the time), I can
  forget to fetch before development (as I did even today),
  I can forget which branches exist and where I should go, I
  can forget which files to delete to ensure I have a fresh
  copy of the database or using the `-r` option to delete
  directories.

  Of course I can make an alias for this, but the reality is
  that even that is too much effort compared to just using
  my brain (which is too much effort), Bash and Zsh don't
  help with this in having absolutely obtuse formats for
  function creation. Terminal emulators don't help with this
  in that they are only an interface for the shell and
  cannot store state or anything like that.

  I'm kind of complaining too much and this is just an
  extension of the simpler history-based completion pattern,
  but I do wish terminal emulators could do this. And it'd
  be best to do in a graphical setting
