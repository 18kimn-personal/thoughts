---
created: 2022-02-26
modified: [2022-02-26]
title: Short term motivations for long-term investments
tags: [workflow, trajectory, sustainability]
author: Nathan
---

A pattern I've noticed in my life is that I gain short-term
motivations from things that I view as solid long-term
investments. Because those motivations are short-term, I
often abandon or move away from them (not completely), not
fully reaping the benefits of those long-term investments.

An example is in data science. This is sort of a
disciplinary move, a career move, that I thought would be
forever valuable at least in the eyes of the market and
academy, so would be a stable long-term investment. I gained
a bit of short-term motivation for it, but as soon as the
newness of it wore off, I realized it wasn't for me, and
transitioned away from it without engaging fully with those
things that would have been helpful to me in the future
(career opps, more advanced modeling stuff, etc).

I do think that trying to look for stable long-term
investments is a good strategy, but a more stable strategy
is to choose those long-term investments that have many
viable and attractive adjacent paths. I consider myself to
have nearly exited from data science, but am still pretty
closely tied to data visualization (should probably finish
the dubois package at some point), GIS and cartography, web
dev, and other paths that aren't really data science but are
still viable things.
