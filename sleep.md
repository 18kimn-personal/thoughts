---
created: 2022-03-16
modified: [2022-03-16]
title: Above all else, sleep
tags: [trajectory, sustainability]
author: Nathan
---

Just wanted to write down a thought I have almost every day,
that for me sleep is so fucking important lmao, pretty much
above all else for how happy I'll feel and how productive
I'll feel during the day. Caffeine, good friends and
relatinoships making me feel like I'm not a loner, not too
busy of a schedule, not drinking insane amounts the night
before, lol those are all helpful in feeling good during the
day, but by far the biggest helper is sleep
