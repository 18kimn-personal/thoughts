---
created: 2021-12-27
modified: [2021-12-27, 2022-01-23]
title: Slowing down
tags: [trajectory, workflow]
author: Nathan
---

## i feel like im slowing down

I am learning a lot these past few days... in a grinding
type of way, I have learned how to configure vim, how
shortened versions of commands are interpreted, how to get
networks actually working properly, how to prevent spotifyd
from failing all of the time, how to get a line highlight,
how to get readable text,

but... honestly i dont have any ideas worth sharing or
writing down. this repository has become more like a journal
than an ideaboard or a space to actually produce
intellectual work.

maybe things will change with time. if i spend more time
working on my thesis then maybe i'll have good thoughts abou
it. we'll see.
