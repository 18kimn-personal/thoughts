---
created: 2022-01-28
modified: [2022-01-28]
title: When you write software, you write values
tags: [sustainability]
author: Nathan
---

What we are really doing when we write software is taking
values we have in our head and putting them into a more
useful form. We might value listening to music, or sending
items from a JSON object, or whatever else, and that
software we build is meant to fulfill those values by
converting them into a more useful form.

Beyond just those practical examples, the values we hold of
individualism or community, of profit or sharing, and so on
are also built through the software we make. We write
values, not software.
