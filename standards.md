---
created: 2022-01-12
modified: [2022-01-12, 2022-01-23]
title: Standards
tags: [trajectory]
author: Nathan
---

I'm interested in disassembling this world and rebuilding a
new one.

That firstly requires a departure from the current standards
-- the current rules and ways we do things that are almost
unquestioned. The fact that we host all of our content on
gigantic trillion dollar corporations. The way we write and
the way we publish. The way we communicate with each other.

And it requires a rebuilding of those things.
Experimentation is key here, to try to find and configure
our own ways to do things.

On my hobbyist side I think this is a reason I love messing
with arch and related stuff like that. We have our
unquestioned operating systems that make design choices for
us, from how to connect to wifi to how to install apps and
how to change our wallpapers.

## We need new standards

For a system to be democratic and accessible, and also just
be ....good...with design..., a lot of these decisions
should be made up front. Re: technical things, they
shouldn't have to pick a network manager or really do
anything on the command line. Re: justice things, we
shouldn't really have to debate and fight for every single
moral standard. We need some central parties^[I hesitate to
use the word "authority" because it doesn't have to always
be an authority figure, it can often be just a standards
committee lik W3C. But often it does come down to authority,
whether we recognize it or not. Just because no one fights
back doesn't mean that they're not authorities.] to make
those decisions first.

## We need to do away with existing standards

But even before that the current processes need to be taken
down and rebuilt. We need to figure things out for ourselves
on a low level and engage in experimentation, rethink our
values and methods and results to abide by different
fundamental standards.

## We need to ethically engage with preexisting systems

Despite having said that we need to rebuild things and take
things down, the reality is we don't need to destroy
everything. We just need to destroy some things and
reassemble them in ways that are better for us.

A return to the sovereignty of Indigenous nations over this
land. A return to the democratic potential of the Internet
as communication before the emergence of platforms. Those
things don't need to be destroyed. It's bad and harmful to
destroy them.

## In sum

So I do things for myself not because I'm an anarchist that
desires decentralization or federation, but because I'm a
marxist-leninist; we need new systems that work for
everyone, and to work towards those new systems we need to
disassemble existing systems, audit the components that make
up these systems, and rebuild them in more ethical ways.
