---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: Stealing academic books and articles
tags: [workflow, stealing]
author: Nathan
---

An essential for all college students.

Books cost a lot of money, and academic books are an
exception in that they cost a shit ton of money. There's no
reason that I should need to pay hundreds of dollars to read
something.

Here is my general go-to practice for getting free books and
articles:

- If it's an article, copy the DOI and go straight to scihub
  and find the article you need
- If it's a book, try to find it on LibGen.

These two strategies generally work for me. If you can't
find it there, generally check out the "Ask for PDFs from
people with institutional access" facebook group, where
people often upload hard-to-find books. Search the group to
see if someone has already asked or uploaded.

If all of those fail, my next step is to turn towards
university resources. For Yale, that means going on Orbis
and trying to find a free version that is available through
institutional access.^[Note that I am recommending trying to
use LibGen and SciHub over university resources. If we
practice pirating it'll be easier for others that come after
us to pirate; if we use institutional access it only reifies
the access of top institutions.]

If I can find the resource here, it generally means I have
institutional access to a resource others don't. I generally
upload a PDF to the Facebook group in this case.

Some resources have restrictions on them to make sure you
don't do exactly what I said above. Some indicators for this
are if the website you got the book from only lets you
"borrow" it for a certain amount of time, and/or if the
website requires you to open the PDF in an app like Adobe
Digital Editions in order to read it.

In many cases this would stop people from sharing, but not
us. The type of restriction is usually a DRM, which can be
removed with some effort. First install Calibre, then
navigate to
[Apprentice Alf's blog](https://apprenticealf.wordpress.com/2012/09/10/drm-removal-tools-for-ebooks/)
and download the plugin to remove the DRM. A warning that
this is a fairly tedious process sometimes; the plugin
didn't work on my prior computer.

Finally, if all of this fails, locate a seller of the book,
like Google Books, Amazon, or Barnes and Noble. Buy the book
and attempt to remove the DRM, then return it. Proprietary
sellers are often a bit tighter about these sorts of things
than providers giving it through university access, so this
might be a little tricky to do. I also think this is the
most inconvenient solution since it requires you to pay
up-front and reimburse yourself later, often at the mercy of
whatever seller's refund policy. It's not great.

If all of this fails, there is a chance that your resource
was only available in print. To check if this is the case,
go to worldcat.org. Perhaps you can arrange something with
someone at an institution carrying the full text; if not,
you are out of luck.
