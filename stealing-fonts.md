---
created: 2021-12-25
modified: [2021-12-25, 2022-01-23]
title: Stealing fonts (sort of)
tags: [stealing, workflow]
author: Nathan
---

No matter what, if you see a font on a page, and it's
genuine text (e.g. it's highlightable text in the DOM
instead of a picture on the page) then your browser either
downloads the font or you have it already. You get that font
for a relatively limited lifespan, and you are just using it
in a specific way that the controller of that font wanted.

Much respect to designers and typographers, but here's how
to obtain that font for use besides a single website:

- Go to a website that has a font you want
- Either head over to the Network pane of the developer
  tools and download every font you see. Generally this
  isn't that many, as fonts are expensive in load time and
  so websites don't want to load them
- Use a free online tool to convert woff2 to ttfs and so on
  to get it in the format you want

You might be done at this point! But if the network tab
doesn't give you the font you want (they've suspected this
already), that's okay. Fonts can also be loaded as base64
data, which can avoids a network request by inlining the
entire font in a regular data file (like a stylesheet). This
means that it won't show up in the Network pane

- To address this, go to the Network pane and look through
  each css file. Open these in new tabs and use ctrl+F to
  search for either "base64" or "@font-face" to find where
  fonts are being inlined
- If you can't find these in the Network pane, you may have
  to go over to the Elements pane and go to the \<head\>
  section of the HTML document. This would be the case if
  the CSS was inlined in turn in the HTML itself, avoiding
  one HTTP request. This section will likely have a few
  inlined stylesheets; some of them may contain the font you
  want in base64-encoded data.
- Once you have this, go to a free online tool to decode the
  base64 data into usable data

These steps might not work; if the web architect is really,
really worried about not having their assets taken, they may
use a specific series of compression/encryption steps that
don't match the above. This is honestly overkill and is less
performant (boo on the maintainer), and even if they have
done this all of the code used to make the font usable is
still obtained by your browser, so with some additional
careful inspection you can retrieve a usable font file.

But if the above steps or some variant did work, you now
have a font! Of course, there are some caveats, in that the
fonts may be of slightly lesser quality than if you were to
buy them wholesale because of compression, font subsetting,
and other things that might happen before it gets to your
computer. But this is still a fun way to s t e a l :D
