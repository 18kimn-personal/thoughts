A version of this note is now published at
https://no-more-paywalls.org .

Knowledge should be free. There's a lot more that can be
said there by people more eloquent and politically developed
than I, but there is also no need to. It's as simple as that
-- _knowledge should be free_.

This page is a resource list describing how to access
paywalled and restricted content, and especially academic
content, for free.

### Academic articles

1. Find and copy the DOI of the article, or digital object
   identifier. This is essentially an ID for academic
   articles. If you can't find it immediately on the site of
   the article, you can search for it on
   [CrossRef](https://www.crossref.org/guestquery/)
2. Go to SciHub and paste it. SciHub's URL changes quite
   frequently because of legal issues, so the best way is
   just to go to Google and find the current one in your
   country.

SciHub is incredibly comprehensive and there is an
international network of scholars contributing their
credentials to make it work, so I've usually never had an
issue with it. But in case of trouble, you can try some of
the following:

- [Ask For PDFs from People With Institutional Access](https://www.facebook.com/groups/850609558335839/)
  from the Facebook group of the same name. Please follow
  their resource list and posting guidelines.
- post with the **#ICanHazPDF** hashtag on Twitter (don't
  laugh)

### Non-academic articles

Use the
[Bypass Paywalls](https://github.com/iamadamdev/bypass-paywalls-chrome)
extension on Chrome, Firefox, or Edge.

For a few other Medium-based sites like Towards Data
Science, I also like to use
[Medium Unlimited](https://github.com/manojVivek/medium-unlimited).

### Academic books

Go to [Library Genesis (LibGen)](https://libgen.is) and try
to find it there. LibGen's search parameter is a little
sensitive, so I would try a few keywords instead of (for
example) the full title, printing press and publicatoin
year.

LibGen has a lot, but it doesn't have everything; if it
doesn't, go to the
[Ask For PDFs from People With Institutional Access](https://www.facebook.com/groups/850609558335839/)
Facebook group as mentioned above. Search for the resource
there, and check out the flowchart in the group banner to
see if any of those resources have it available. If they
don't, ask nicely if anyone can send a PDF of the book.

If that route doesn't work, you may want to try asking on
[r/AAAARG](https://www.reddit.com/r/AAAARG/) for access to
the AAAARG archive. This mostly works for older philosophy
texts and other books in the humanities.

If none of those work, you can try paying for an eBook from
a retail outlet like Google Books, Amazon, or Barnes and
Noble, and "returning" it to get a refund. This is somewhat
unsafe.

If none of these work, your book may only be available in
print. Check on [worldcat.org/](https://www.worldcat.org/)
to see if this is the case. You may be out of luck.

### Share the resources

If you obtained the book through institutional access or any
means other than free and accessible means, you now have the
opportunity to share that resource and ensure others won't
have to jump through the hoops you did. Here are a few notes
on doing so.

Before sharing, we have to make sure the file is shareable.
Resources often have a lock called a DRM on them to ensure
that a PDF for you can't be opened on any other machine
besides your own.^[Some indicators for the case is if you
"borrowed" an eBook (what the hell is even borrowing an
eBook, anyhow), have to open the file through Adobe Digital
Editions, or got it from a retail outlet] If this is the
case, download the eBook viewer
[Calibre](https://calibre-ebook.com/download) and use a
plugin called
[DeDRM](https://github.com/apprenticeharper/DeDRM_tools/wiki/Exactly-how-to-remove-DRM)
to remove it.

If you are sure the file is shareable, you can upload it to
the
[Ask for PDFs](https://www.facebook.com/groups/850609558335839/)
Facebook Group and to
[LibGen](https://wiki.mhut.org/content:how_to_upload). If
you are on the tech-y side, you might also consider setting
up and hosting a
[Calibre server](https://manual.calibre-ebook.com/server.html).

### Last thoughts

You can email me at nathan.kim@yale.edu.

You can offer suggestions or edits at
[https://github.com/18kimn/knowledge-should-be-free]. If you
are familiar with Git/GitHub, feel free to open a PR or
issue with the change you want to make. If that makes no
sense to you, you can email me.
