---
created: 2022-01-23
modified: [2022-01-23]
title: Ways to steal things from Yale
tags: [stealing]
author: Nathan
---

> The only possible relationship to the university today is
> a criminal one. [...] In the face of these conditions one
> can only sneak into the university and steal what one can.
> To abuse its hospitality, to spite its mission, to join
> its refugee colony, its gypsy encampment, to be in but not
> of—this is the path of the subversive intellectual in the
> modern university.

-- Fred Moten and Stefano Harvey, _The University and the
Undercommons_

TLDR:

1. Most generally, adopt a willingness to take what Yale has
   but will not provide freely. At the Bow Wow, at Camp Yale
   and Bulldog Days, fellowships that aren't really
   applicable to you, jobs you don't really work in -- these
   trivial moments are important.
2. apply for the richter fellowship, even if you're not
   actually doing research. Apply for the DSA (or whatever
   the summer award is called now), but that you probably
   already know
3. always bring a tupperware or two to the dining hall,
   especially if you live off-campus
   - and don't be afraid to steal from the bow wow (still
     can't believe that's a real name. Banal nationalism)
   - also, you can enter the Murray dining hall from the
     back to avoid using a meal swipe if you live off-campus
4. Consider living off-campus, as you effectively pay
   $1200/month for university housing and apartments can be
   found for much cheaper than that in New Haven. If you're
   on financial aid, Yale will still pay you the same amount
   as if you lived on campus, meaning that you will actually
   gain a decent stipend per month this way.
5. Institutional access to academic books/articles is
   really, really useful; definitely spread that via the
   Institutional Access facebook group if you find something
   through Yale resources that you were not able to find
   elsewhere.
6. The Zoo and the Women's Center both have free printing.
   Probably lots of other places as well if you have an eye
   open for it; Pauli Murray had free printing my first year
   if you printed from the basement computer lab's system
   interfaces instead of Yale's printing service, but this
   was eventually fixed. Useful for if you have to print 500
   flyers for some event and don't want to spend $50 on
   printing.
7. Yale's unlimited Google Drive access is actually a
   game-changing move. Some other services charge you for
   using just over 5Gb a month, which doesn't really cut it
   for me as I'm using about ~512Gb currently. If you edit
   video, work with large data files, manage frequent
   backups, or anything else that requires you to have a lot
   of storage, consider (ab)using Google Drive as a free
   resources.

---

I think I first began thinking of writing methods to steal
down two semesters ago, when I had to obtain a book for a
class that I could not find for free. Despite going through
all of my usual sources quite diligently, there appeared to
be no pirated copy online and I did not have institutional
access. A bit peeved, I decided to pay ~$30 to the
publsiher, remove the DRM from the PDF, and send it to all
of my classmates so that no one else would have to pay for
it.

This was near the beginning of the semester, so I didn't
have all of my classmates' contact information and I didn't
even know for sure who was in the class. So I asked the
professor to send it out via email to everyone. But she
refused! Very nicely, she explained that she could not due
to copyright laws.

That refusal honestly made me laugh a little. It was an
ethnic studies class where we talked about more free ways to
share and spread knowledge, and the authors we read were the
usual humanities canon of sort-of-radical people (Foucault,
Fanon, Audre Lorde, Cesaire, ...) that all had to move past
boundaries to write and teach. But she couldn't do this very
simple task of giving a free PDF to their students.

I understood that it could be a matter of job security, and
of course I have no harmful feelings at all towards her now,
but it still made me laugh in confusion. If not something as
tiny as this, what were you actually willing to do for the
ideals you were teaching about? (Please excuse my naivete
and melodramatic langauge) what rules were you willing to
break if you couldn't even break copyright law?

I realized that on a more general level the willingness to
steal and take resources from the university as those
resources are fundamentally ours is in general missing. Of
course, there's also the general obscenity of wealth among
Yale students, so there's that. Anyhow, I decided to write a
little bit about strategies to steal, in hopes that I could
convince others to do so. So this is the first in a series
of notes about stealing!
