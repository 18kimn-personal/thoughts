---
created: 2022-03-27
modified: [2022-03-27]
title: Readable Streams
tags: [web_dev]
author: Nathan
---

Readable streams are great. In essence, they allow you, with
a bit more code, to avoid waiting for a request until
something is finished, and just allow you to start doing
stuff right away

this has been most useful for me in the korea map thing,
where I stream shapes instead of waiting for them to load
all at once, since htere are a lot of them

the benefit is felt most strongly for low-bandwidth
connections, where waiitng for _everything_ can take like
twenty seconds

it's also been in the spec for a long time and is in all
major browsers, plus nodejs
