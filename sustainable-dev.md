---
created: 2022-01-28
modified: [2022-01-28]
title: Sustainable world-building
tags: [sustainability]
author: Nathan
---

While watching
[this video](https://www.youtube.com/watch?v=tJuqe6sre2I)
the maker made a comment that the Burj Khalifa had massive
height and a poem to glamorize itself but no sewer system. I
had seen this before, but the author's comment made me
realize that a hallmark of sustainability and a better world
is to prioritize doing things right, and in doing so
departing from the values that make things shiny.

I think that channel is pretty good lol. it's funny and has
good values.
