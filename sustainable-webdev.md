---
created: 2022-01-26
modified: [2022-01-26, 2022-01-28]
title: Sustainable web development
tags: [sustainability, webdev]
author: Nathan
---

This is sort of a recap of many thoughts I've put here over
the past month

I want sustainable web development:

- uses only as much resources as needed
- uses only as much code as needed
- works for everyone on any bandwidth
- is maintainable, documented, and clear
