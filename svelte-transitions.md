---
created: 2022-06-30
modified: [2022-06-30]
title: Transitions in Svelte are great, but
tags: [web_dev]
author: Nathan
---

whew, first note in a while. just wanted to write that
svelte transitions are great and very intuitive, and
definitly an improvement over both vue and react in terms of
ease of use (my main desirable trait) -- it's so easy to
slap on `transition:fade` somewhere and just have it look
nice.

But there are two headache-inducing drawbacks:

1. when transitioning multiple components with a fade, the
   first still stays in the DOM with its original size,
   meaning the new component by default is placed below or
   next to where it will go in the end, and then there is an
   awkward shift into place. You can try to avoid this with
   the `delay` option on the transition, but that still
   brings in a flash of layout shift when
   - the solution is to use `display: grid` on the parent
     and then for all transitioning items (the children in
     the grid), force them to take up the exact same space
     with `grid-column: 1/2;` and `grid-row: 1/2`.
   - example
     [here](https://svelte.dev/repl/18c5847e8f104fa1b161c54598ec3996?version=3.25.1)
2. Sort of ? related to the first situation, I had a problem
   today of trying to replace a component with sliding
   children with a component that fades. did not work. the
   sliding component just did not disappear, and the
   fading-in component appeared in a random spot on the
   screen. how strange
