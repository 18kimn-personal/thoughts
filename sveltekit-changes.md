---
created: 2022-08-09
modified: [2022-08-09]
title: SvelteKit changes
tags: [web_dev]
author: Nathan
---

https://github.com/sveltejs/kit/discussions/5748#discussioncomment-3282732

SvelteKit is preparing for 1.0, (they have been for like a
year and a half now), and as part of it they're announcing
two big changes to their api, one with `load` and one with
routes.

For routing, everything in the `src/routes` folder is a
route (every file, except for those prefixed with `__`).
`routes/my-route.svelte` and `routes/my-route/index.svelte`
are the same route. This proposal makes this unambiguous and
unfiorm by requirng that all routes are directories, and
files are only routes if they are the `+page.svelte`
prefixed file.

There's a bit more there but that's the gist of it. This is
by far the more contentious of the two changes, at least by
viewing the commenting.

The `load` changes are a bit harder to summarize since
they're kind of a lot of small things, but the gist is that
they're throwing stuff out and replacing existing stuff to
make it simpler. This means more reliance on functions from
sveltekit as opposed to properties on the `load` return
object (`status`, `redirect`). The changes here are enough
so that they'll definitely break any app that has even a
remote amount of reliance on the SvelteKit backend aspect
(are using SvelteKit for anything other than a traditional
SPA).

OK. My thoughts are....well, it's growing pains, I guess,
and the whole reason that they're making the changes is
because they aren't stable but want something that is. So
they 1) feel that they can make breaking changes as it's not
1.0 yet, and 2) feel that they should make whatever breaking
changes they will make now, and avoid making them after they
are 1.0. But...personally, after having become used to
sveltekit and svelte, the changes being made don't really
simplify anything for me (they are targeted towards
newcomers, and presumably those of larger apps where
unambiguous and strict routing is needed), and I appreciate
the existing conveniences of "files are routes."
