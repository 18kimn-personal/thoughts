---
created: 2022-09-05
modified: [2022-09-05]
title: Tips on prerendering with sveltekit
tags: [web_dev]
author: Nathan
---

1. Pepper a bit of `export const prerender = true` in
   `+page.(server.)ts`

The error messages and warnings encourage you to do this in
`+layout.ts`, but I find it actually helps to force a render
of the server.

2. Move all data out of `onMount` fetches and into the
   `load` functions in `+page.(server.)ts`

You probably already do this if you're new to SvelteKit, but
at first the easiest/best way to load data was to use page
endpoints instead of load functions. The problem is that
`onMount` is a hook that is _only_ executed on the browser,
not during server-side renders or prerenders, so data
(potentially additional links) loaded by `onMount` hooks
won't be picked up by SvelteKit as it traverses your site
for prerendering.

3. Fix all broken links.

This one is somewhat less tricky becasue SvelteKit will yell
at you if you don't. "Broken" means:

- external links should be prefixed with the protocol
  `https://`
- if internal, the pages they link to should exist
- correct extensions is always necessary (e.g. if you're
  using a markdown preprocessor, know whether or not you can
  use `.md` in your markdown files links to link to others)
- in markdown links, they shouldn't be prefixed with `<` or
  `>` -- the GPL license that I have as an annotated note
  does this, lmao

---

the last thing I'll say here is that these tips actually
apply to SSR too, in that they help create a fully hydrated
and working page instead of relying on client-side
javascript to get things working.
