---
created: 2022-03-09
modified: [2022-03-09]
title: Systems and circumstance
tags: [social, korea]
author: Nathan
---

I don't know exactly what I was looking for when I came
here... I think I was looking to be a hermit, for safety and
comfort and familiarity and to grow on my own

In fact i have been sort of forced to... not do that. to go
outside

today I stayed in my room the whole day, took a two hour nap
in the middle, and am spending time figuring out emerges,
watching youtube videos, and writing notes like these. my
level of happiness could not be higher (word choice...im not
exactly giddy or anything but yeah im pretty content)
