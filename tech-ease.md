---
created: 2022-02-09
modified: [2022-02-09]
title: Some things in tech just need to be sensibly easier
tags: [workflow, web_dev]
author: Nathan
---

I've just started learning Rocket from
[the official guide](https://rocket.rs/v0.5-rc/guide/quickstart/),
and came across the following:

> The absolute fastest way to start experimenting with
> Rocket is to clone the Rocket repository and run the
> included examples in the examples/ directory. For
> instance, the following set of commands runs the hello
> example:

```
git clone https://github.com/SergioBenitez/Rocket
cd Rocket
git checkout v0.5-rc
cd examples/hello
cargo run
```

This just made me laugh. Specifically the version number not
being very comprehensible, but also that you have to `cd`
twice and no folder besides the `examples` directory is
explained in this page.

This is not really a criticism of the Rocket guide at all.
This guide is really, really well put together in most
respects, they link out to so many resources. But some
things should just be made simpler.

Maybe this will come with time for Rocket. The version
number being pretty low and the owner of the Github repo
looking like a person's name (instead of an org) makes me
feel like the project is just pretty young, but it could
just be that this example repo doesn't have the full
attention of the Rocket team considering that Rocket itself
has millions of downloads.

Hm, it seems tahtt hat repo is also the official one for the
library itself. That's actually quite funny. So ti seems
weird that it's still owned by just one person, but does
explain the complicated-ness of the folder structure a bit.
