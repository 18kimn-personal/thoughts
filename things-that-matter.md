---
created: 2022-02-12
modified: [2022-02-12]
title: Some things matter, some things don't
tags: [workflow]
author: Nathan
---

I've concluded after some reflection on the thoughts in
[deno](./deno.md), and other things, that I am thinking
about entirely the wrong questions.

It doesn't amtter whether node or deno is better. or even
whether node or rust is better. Heck I shouldn't evne be
caring about a site written through jquery-only or through
anything else.

These are nice values to have. But they're the wrong values
to prioritize, and the worse ones to spend time on.

So starting this moment I'm going to spend my time on the
things that do matter. Data analysis (for my circumstantial
reasons), maps, my thesis, communist theory ...

no more of this getting caught up in random code semantics
and performance things. onto the things that matter.
