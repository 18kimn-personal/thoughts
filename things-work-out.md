---
created: 2022-03-18
modified: [2022-03-18]
title: Things always work out
tags: [trajectory]
author: Nathan
---

Things always work out. That's what I'm telling myself.
Things always work out.

Despite too much time not working on thesis, delaying korean
hoemwork, stressing over the minutiae and aggravating parts
of everyday life, things work out. The test was actually not
today, I am actually making okay progress on the thesis
despite it still not being in very good shape. Other people
appear not to have done the homework too. I have time to
sort out my visa and the summer applications. Things are
okay.

Of course, things are not always okay. But I'll tell myself
that for now. Things work out, sometimes not perfectly,
sometimes with kinks, sometimes stressfully, but they work
out. Just keep going, nathan
