---
created: 2022-01-25
modified: [2022-01-25]
title: Time to first byte
tags: [webdev]
author: Nathan
---

In web development, there are generally two stages that
cause loading times over a network:

- time to first byte: when the link between the server and
  the user is established, the server prepares and sends
  over data, and the user first receives that data
- everything after: the user downloads a stream of stuff via
  a sequence of TCP packets that increases over time (e.g.
  it has a slow start -- first 10 packets or 14kb, then 20,
  40, 80...)

The second generally is improved by having smaller bundle
size, and it's why a huge portion of optimization is
dedicated towards bundle size optimization

The first can be a little bit tricky because there isn't a
one size fits all solution the way reducing bundle size can
just mean having less code.

Here are some strategies nonetheless:

- prepare a faster server response. I have no idea how to do
  this, and especially on providers like netlify
- inline assets so that critical content can be shoved into
  a single request. Multiple _sequential_ requests always
  require another "ttfb" instance, as in the server and
  client do a tcp handshake and then start sending data.
  This takes a while; the better way is to have only one
  "level" of requests sent first.

  To elaborate, a request usually goes like:

  index.html -> critical js, css -> hydration (no network)
  -> images, assets called from js

  It's possible to eliminate that so that the html initial
  page, js, and css are all loaded at once instead of
  sequentially

  The downside is that inlining value decreases after a
  point because the TTFB that is shaved off will be offset
  by increased download time; we shove everything into a
  single request, and in return we can no longer stream JS
  and css in parallel
