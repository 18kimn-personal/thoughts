---
created: 2022-01-05
modified: [2022-01-05, 2022-01-22]
title: Typescritp
tags: [workflow, webdev]
author: Nathan
---

my first impressions with typescript was that it was
needless. I had heard it described in a JSConf talk as
"enterprise JavaScript," and it seemed to be exactly that --
something that big companies needed because the costs of a
single error were astronomical, and codebases were large
enough, staff transient enough that tracking down a
particular bug wouldn't come naturally as it does for a
single person working on a solo project.

Typescript also was a little annoying for me to begin with
because I had to deal with libraries that were not built in
typescript, like d3.versor; when I wanted to use type
annotations with them, I would have to either configure an
env.d.ts or something and add the specific annotation I
wanted, or I would have to use the "any" type. I often did
the latter when things got frustrating enough.

Even for libraries like d3 that had a corresponding types/d3
library, often the types would clash with the types I drew
up for the dataset I supplied to d3. I'd use "any" in these
cases too.

There's probably a correct way to handle these situations,
but they were frustrating for me as a beginner.

But the next time I started a project, I felt myself
wondering where the type annotations were...and feeling
strange to name my files ".js", because in my mind there was
an implicit value hierarchy now between javascript and
typescript. For some reason typescript was annoying but
just... _better_.

Anyhow. Those opinions aren't meant to be correct or
anything, and this piece isn't relaly a critique or takedown
of typescript... I just wanted to air out my thoughts on how
I felt about

---

2022-01-05

I realized another underdiscussed reason typescript is so
good is because it forces you, the writer of the code, to
have a better understanding of the code you write. Just as
when you write a paper, taking notes and writing down ideas
as you go is better than pretending the version in your head
is good, typescript forces you to translate transient
thoughts to semi-permanent text, in structures that reveal
where you lack understanding and where you have not yet
thought.

It's a note-taking system for your code. Meetings and notes
help people understand an ongoing project by forcing people
to think about things on a meta level, to not just _do_ but
think about how and what they do, and typescript is the same
way. It adds a meta layer, forcing you to think about your
code on one layer higher than we usually do.

Its supercharged (unlike traditional meetings and notes) for
the reasons I noted in the first part of this entry, because
of the intellisense/autocompletion etc. That's great and
definitely a plus; but on a more conceptual fundamental
reason its because that meta layer exists in the first
place.

This also represents a slight departure from the "typescript
is enterprise javascript" paradigm that I wrote about
earlier. Typescript forcing you to think about your code is
a bit managerial, not really cotnradicting the "enterprise
Javascript" conception in every way, but its also
reflective, expressive, allowing you to say things in text
that traditional Javascript (as a dynamically typed
language) does not.

---

2022-01-05

Also, "Typescript" is kind of a misnomer, because the real
reason typescript is useful is not because it forces you to
think about types (which it definitely does) but because it
forces you to think about _structures_.

```javascript
interface MyData {
  meta: {
    dateProcessed: Date,
    author: string,
  },
  data: {
    myColumn: ...

  }
}
```

This semantics argument is halfway trivial; this structure
is of course still a "type" in Typescript, and is composed
of things that are quite explicitly just base JS types (e.g.
strings, Date, etc.). But the connotation of "type" is a bit
shallow, and doesn't convey what Typescript is actually
useful for. Especially for smaller projects, I'll always
remember that "dateProcessed" is a date, "author" is a
string, and so on, because those are immediately suggested
by the names of the properties and there's only like five
proto JS types anyhow. Heck, there's no confusion even about
floats/doubles/ints/etc. You don't really need Typescript
for that, although larger projects might still benefit from
it.

_Structure_ more directly conveys Typescript's usefulness.
It's about how these types overlay into each other, that
MyData is composed of separate meta and data fields, that
meta has "dateProcessed" and "author" columns.
