---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22]
title: Post-install thoughts on Ubuntu Touch
tags: [linux]
author: Nathan
---

Before installing ubuntu touch, I knew of a few problems
that I could have:

- Essential apps like Duo Mobile would probably be a pain to
  figure out
- Lack of native Google support would probably be not good
  for other auth things
- Difficulty
- Google and Apple have way more UX/UI people than
  Ubuntu/Canonical, so I'd probably have a harder time there

In short, I knew from the outset that there would be
downsides to switching to Ubuntu Touch. I chose to do it
because I thought it'd be more fun anyhow, and more in line
with my political goals of obTaiNinG InDePEnDence from mega
corporations

Anyhow, there were still a few hiccups along the way that I
thought I'd document. Those are:

- No home screen. This is probably the most baffling one.
  Why is there no home screen? lmao
- Instead of the home screen, there is the
  much-less-ergonomic "swipe" action from the left and right
  to bring up and close apps
- MMS messages ("group messages") don't work LOL
- Contacts are unable to be migrated because my Google
  backup doesn't allow signin from the Contacts app (it
  thinks it isn't secure or something lol)

Some other things I was able to figure out:

- Mount a libetine container to run desktop apps
- SSH process and workflow
- Fingerprint-based login
- spotify via futify (not sure if offline works)
