---
created: 2022-07-29
modified: [2022-07-29]
title: Gah, vim
tags: [vim]
author: Nathan
---

Despite using vim for everything (and I don't like to
journal with phone or paper because it's become ergonomic to
just rely on a keyboard), vim is incredibly annoying about
some things, some things that will take a fair amount of
effort or investigation to solve tha tI don't want to do
right now

- vimscript. Is a terrible language. Full stop. Thank god
  neovim uses lua. But it sucks that I followed all of the
  tutorials and am still using vimscript for my init.vim,
  and also I don't actually know lua, and neovim is still
  kind of new with their transition so I don't know how
  stable it will be.
- Colors and highlighting, other aesthetic things. This is
  more of a terminal thing, probably, or a vim-terminal
  integration thing, but fo rsome reason like random bars
  and popup windows appear randomly and don't disappear
  properly. I don't understand it.
