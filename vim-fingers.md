---
created: 2022-01-13
modified: [2022-01-13, 2022-02-14]
title: Vim in my fingers
tags: [workflow, vim]
author: Nathan
---

A while back I said vim was not for me.... yet here I am
fully committed to it...

My thought before was that vim was pretentious, useless, and
obtuse. I still think so to some extent, but I discovered
that I am looking for a sense of security and stability...
that obscurity and obtuseness enable a little bit. I can do
my own thing with tools only I can use and a workflow only I
can do... it's super pretentious and stuck-up but there is a
sense of ownership and security in that that I have not felt
in a long time
