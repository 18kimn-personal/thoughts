---
created: 2022-01-18
modified: [2022-01-18]
title: In Vim and elsewhere, perfect is the enemy of good
tags: [vim]
author: Nathan
---

I switched from writing in Microsoft Word, Google Docs to
Obsidian, from Obsidian to VSCode, and most recently from
VSCode to Vim, in search of a plaintext, low-tech, portable,
sustainable way to write. I wanted to write without relying
on proprietary tech in a format that those without
properitary tech could read and write in.

It's been okay, there were a lot of bumps with regards to
the right plugins and aesthetics and shortcuts and such.
What was probably most difficult was lacking perspectives
and information on obstacles that people don't talk about
online. So this post is a condensed perspective I have now
for new Vim users. In a single sentence, it's that perfect
is the enemy of good.

If you're a new Vim user, unaware of the idiosyncrasies of
VimScript, of the sort of duplicitous and distributed nature
of the plugins ecosystem, of the many opinionated
perspectives on what plugins to use, you may be caught in an
endless process of configuration. If you're looking for a
file browser since that was a really useful feature built
into VSCode and Obsidian and others, you might try NerdTree,
as that's what that one Medium post called _A comprehensive
guide to getting started in Vim_ or something suggested.
Then you might try just relying on fzf.vim's browser, as one
Reddit comment suggested. Then you might try just relying on
netrw. Then you might find vim-vinegar's augmentations to
netrw, and add that. Then you might find defx, or
vim-dirvish, or nnn,...

This is sort of Vim's essential feature: that it is minimal
but configurable, and that there are many options to choose
from. But you may soon realize the obvious, that it is
cumbersome to continue to switch from one method to the
next. You might not realize it, and just enjoy the process;
I totally understand the urge to work on workflow, and it
seems to be a common things elsewhere too (an example of
this is "distro-hopping" in Linux, or endlessly switching
what distributions you rely on and work with).

My advice to the new Vim user -- to myself from only the
near past -- is to stop that. No matter if you find it
laborious but trudge on to find the _perfect_ workflow for
you, if you will find it frustrating soon, or if you just
find it fun to try out new things and will never want to
quit -- stop.

The reality is that all workflows are meant to help you do
that: _work_, or otherwise create things and actually get
around to edit files. It's difficult to do that when you
pause every day to make an adjustment to your Vim system.

The argument behind Vim for productivity goes that it is an
investment to learn and configure, but it will pay off in
the end because it is a sustainable technology that you can
use and benefit from for decades. My argument behind
workflow is that you will lose all of these benefits unless
you decide to reach a stopping point to find a configuration
you are comfortable with and stick with it.

I've seen some

The reality is that a perfect workflow doesn't exist

- **It's okay to have things just be good, and not
  perfect.**
- **Use a bigger font size**
-
