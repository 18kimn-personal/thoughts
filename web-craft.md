---
created: 2022-02-20
modified: [2022-02-20]
title: Web dev as craft
tags: [web_dev, workflow]
author: Nathan
---

Two approaches:

- web dev pragmatically, as a nine-to-five
- web dev creatively, the way an artisan designs a chair

(this of course is a bit of an exaggeration)

Both do it to work and make a living, to pay the bills. They
have a life outside of work and work can be both the bane
and a high point of their life

the second is what i feel like i innately wish to be, and so
i work on stuff freely, get excited by new toys, constantly
iterate over previous things ive learned and find things fun

but a lot of that work is lost or made really inefficient by
poring over these meaningless details and ignoring the
bigger ones (how the work actually needs to get done)
