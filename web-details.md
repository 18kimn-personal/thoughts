---
created: 2022-03-13
modified: [2022-03-13, 2022-03-21]
title: The last ten miles in making a good website
tags: [web_dev, workflow]
author: Nathan
---

In web development as in almsot everything else, the jump
from having no project to a good proof-of-concept project is
much different the jump to that proof-of-concept stage to a
polished website. The first jump is about stepping into
action, getting the groove going; it's very difficult if you
have to learn a lot of new material or if you aren't in the
groove of starting new projects. The second jump is about
finishing all of the corners, iterating multiple times on
design, nailing down those difficult bugs you just let sit
before while you pumped out the cooler, more interesting
bits. This stage is difficult and frustrating because many
times those fixes and changes are very minor details that
take a long time to complete, and so you feel as if you're
putting in endless hours of work with very little to show
for it -- in features developed, but in the number of lines
of code changed, how much refactoring you had to do in the
process, etc., it just doesn't feel that productive or
rewarding.

That second stage is the harder one for me as of late.
There's not really one reason to it (there are many
reasons), but if I had to give one I would say it's just
because i'm a single student doing fun thigns in my free
time, and I don't have the time, resources, know-how,
motivation, or anything else to consisntently fix those
things. That's not really a good excuse, since I can
definitely be better about this, but it's my reason

in any case here's a list of those difficulties that often
get pushed to the end for me. A lot of them I know I will
get better at "naturally" (by just learning them in-depth
and doing them earlier) but others (esp qualitative things)
are harder

- mobile/responsive design
- keyboard shortcuts and navigation
- shoring up semantic html and other accessibility concerns
- SEO
- color schemes, fonts + typography in general, -- design
  things
- CI/CD deployment
  - sort of, only sometimes. Netlify is such an easy tool to
    deploy frontend projects that I don't really notice. But
    I want MORE: server-side things in general, learning
    rust/rocket/actixweb, setting up my own CI/CD templates,
    etc.

---

There are some other things that don't necessarily relate to
these last details but feel the same, as in they're features
I would like websites to be conscious of because htey have
been issues for me in the past:

- never steal focus
- always do arial-labels etc
