---
created: 2022-01-27
modified: [2022-01-27]
title:
  The first battleground of the web wasn't really fought
tags: [capital]
author: Nathan
---

Related to thoughts from
[nfts-capital.md](./nfts-capital.md)

I realized that the current battleground against web3 is
totally justified, but just as justified (lol) was the
battleground of web1 and web2. Granted, those terms are kind
of an aphorism since people didn't relaly call it those
things at the time, and that sort of speaks to how the
distinction between the two is a lot less recognized

But yeah. The web was born out as a fundamental democratic
space, where all had their own voice, but it was quickly
conquered by those who had capital (the material resources
to buy and maintain their own servers). That battleground
wasn't fought or protested against; it simply happened. Why
wasn't it?
