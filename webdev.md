---
created: 2021-12-25
modified: [2021-12-25, 2022-01-22, 2022-01-23]
title: Performance and webdev
tags: [perf, webdev]
author: Nathan
---

I am thinking about being a better coder. In the past I did
things a bit haphazardly and tried to do things that were
fun and cool. Right now I want to do things _right_. Mostly
that means fast and reliable.

That means:

- send as little as possible over the wire, especially from
  the outset. Just send raw html and css if possible so that
  content can be loaded immediately even if augmentation to
  that content takes some time
  - this is what a benefit of server-side rendering is, but
    that doens't matter to me yet because I really don't
    want to configure a server and I don't need to given
    some of the other strategies here
- avoid layout shifts; use fades instead
- Use async for everything
-
