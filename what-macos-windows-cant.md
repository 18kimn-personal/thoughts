---
date: 2022-01-06
title: What can Linux do that MacOS and Windows can't?
tags: [linux]
---

This is a question I'm guessing a lot of people would have,
but I honestly haven't thought about it much since the main
reason I'm using linux is because of its open source and
free nature, not because of the features it offers.

Still, I was thinking about this today and decided to try
and make a list. This list is probably entirely incorrect,
as I don't actually remember Windows that much and I've
never really used a Mac, but oh well.

- More system settings (KDE)
- (KDE) support for attaching custom commands to shortcuts

  - This is useful for me for instance for opening my
    workspace and default tmux configuration

- ability to change desktop environment, or its components
  (file browser, launcher, system settings, ...)
  - sort of related: tiling window managers
- package management
  - can upgrade all apps at once
  - can roll back to previous versions of apps
  - scriptable
- related to the above two: you can _uninstall things_. You
  can install, ... well almost everything! Even your desktop
  environment! You can remove yourself!
- boot process - create a custom login screen, boot screen
  (through plymouth)
- development
  - you need to buy a license to develop apps on macOS
- On Windows, I had issues with docker not picking up file
  changes during development. This was fixed on WSL.
- Broken installs can be fixed with a live cd
  - I had to do this when a power shortage (?) broke my
    ubuntu distro last fall
  - windows and macos are not reinstallable
  - granted, windows and macos have power management built
    in so this doesnt need to be done in the first place
-

Counterpoints:
http://www.dvorak.org/blog/2016/02/25/why-linux-sucks-and-will-never-compete-with-windows-or-osx/
