---
created: 2022-01-26
modified: [2022-01-26]
title:
  Zettelkasten only works when you're not in a time crunch
tags: [zettelkasten]
author: Nathan
---

the zettelkasten system only works when you're not in a time
crunch:

- when you have the luxury to take notes and learn as you go
- when you don't have to consciously think about how one
  point will connect to the previous, as you do when you
  write a paper
- when you can pursue content first and let structure emerge
  eventually
- when your notes don't have to be hyper-specific to a
  single thing you're working on
