---
created: 2022-08-11
modified: [2022-08-11]
title: The topics I write about
tags: [workflow]
author: Nathan
---

I mostly take notes on software concepts -- coding things,
web dev, .... and I also write my posts about the same
things. Yet I consider my more serious or long-term career
plans to be somewhere between the humanities and social
sciences, topically around the critique of global financial
capitalism and neoliberalism, and histories of periods of
crisis.

So why do I act or feel this way? Is it only that I just am
more day-to-day interested in coding stuff (and if so, even
then, why)? Is it circumstantial -- I'm in korea and fairly
isolated in all senses from people who are engaged in those
kinds of things? Is it that media that's in front of me
respects or engages more with these kinds of things?

I do want to make a pivot away from simply
software/tech-related stuff, and to be more engaging with
politics and policy and political economy and culture. Some
strategies, or O(1) actions that lead to O(n^x) results,
could be:

- adding different thigns to RSS reader, twitter feeds, find
  a replacement for hacker news reader app
  - counterpunch
  - publishing houses...??
  - ...??
- go back to america or find an organizing group lmaoo
