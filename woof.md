---
created: 2022-03-18
modified: [2022-03-18]
title: Devotion
tags: [trajectory]
author: Nathan
---

i want to be devoted the way denji is to makima. To
completely lose myself, my body, my dreams, to ...someone,
to something

woof

to be the pillar for the bridge (as konan says) for the road
to peace

I don't think the things I have are in isolation useful, I'd
rather be a tool in service of something bigger, a dog for a
master who knows what's best

this language is dehumanizing and regressive but it's so
attractive. I hate thinking on my own. I hate trying to
figure out my values and strategies. I hate deciding that
the choice I have made for myself in the past was useless
and unproductive. I'd rather just have complete unwavering
faith in something, to lose all of my own individuality in
service of that thing
