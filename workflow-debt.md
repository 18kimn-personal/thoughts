---
created: 2022-01-24
modified: [2022-01-24, 2022-01-28]
title: Debt and workflow
tags: [workflow, sustainability]
author: Nathan
---

the Latin American debt crisis and the ASian financial
crisis erupted because of a mountain of debt that could not
be paid back, and most of the relief efforts (especially at
first) were to pile on even more debt.

That's sort of what I feel like when I respond to my
superiors. I feel bad that I wasn't able to "pay" what I
"should have," so I often suggest an extra task that could
be done at the same time or even say that I'm doing that
extra task.

In both cases this never ends, and I become locked into a
cycle of debtorship. If I succeed, the superiors are
encouraged by my work and suggest even more tasks; if I
fail, then either they or I try to get that task and
possibly extra to be done. Either way the debt increases
forever.

The more sustainable approach in both cases is to simply let
rest what should have died, and continue living without that
burden

---

the linguistic similarity is in the misuse of "debt," as the
debt that has to be paid isn't really owed in the first
place, or at least not legitimately owed.
