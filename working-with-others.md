---
created: 2022-02-13
modified: [2022-02-13]
title: Finishing projects, and my own hypocrisy
tags: [sustainability, workflow]
author: Nathan
---

There are four collaborative spaces on my mind tonight.

1. [The Yale Detour](https://yale-detour.org/), a
   cartography/counter-history project that me and four
   friends as of now appear to be interested in. I've done
   all of the work to get us to publication step with
   regradsd to the technical details, but the project has
   been dragging for the past few months with regards to
   punctual responses. I'm currently at the stage of tryign
   to get everyone to sign off on the project and approve it
   for release; the others involved in the project don't
   have to do any work but say yes

2. [Evictorbook](https://evictorbook.com), a sort of
   landlord encylopedia that I've been working on as a
   member of the anti-eviction mapping project. This project
   has also been lagging, even before I joined but
   definitely still now, and I am 10000% a culprit here as
   well.

   The "lagging behind-ness" I feel is different from the
   Yale Detour at least, because there's so much technical
   work involved and the reason it has taken so long si
   because it is just a very complicated project.

   But honestly, especially from my own standpoint, it is
   only my own fault that I haven't been prompt about
   responding to messages or following up on PRs and bug
   fixes. A few related projects with AEMP I have also
   stopped responding to and ghosted in a sense. I should
   not do that; that is completely my fault.

3. My work with Emma Zang. This has always been that sort of
   not-so-productive cycle. I am really grateful for her
   promptness in replying to me, in supporting me so
   consistently through recommendation letters and sending
   me summer opportunities. Of gently following up with
   regards to lagging projects, so that even though I always
   send her results much later than I stated would be a good
   deadline for me to finish by, we are able to publish and
   enjoy the fruits of those efforts.

   But I'm still caught in that cycle, this time with the
   PSID (panel survey of income dynamics). Sigh.

4. My work with DataHaven. This is probably where I feel the
   worst. I love Mark, Kelly, Camille, and the interns. I
   want to support them, and yet I'm so bad about following
   up to everyone involved. Mark told me he also took a gap
   year and felt much better when he came back; that I could
   continue workign with dataHaven and that I'm still on
   payroll, but of course no pressure for me to work.
   Camille tagged me in the `#random` channel in a question
   about object-oriented systems in R, thinking I might know
   about the topic she wanted to talk about. I'm so grateful
   for those comments, for that kindness and openness
   despite me not having done anything useful for them over
   the past year.

   I want to work with them, because I respect them a lto,
   because they do things I'm interested in, that I know
   that I could do the things I'm interested in there even
   if they don't do it right now, and because practically it
   would be really nice ot have some disposable income
   lately.

   And yet I still haven't responded to Mark's message. Or
   to Camille's message about the object-oriented classes.

---

Anyhow, I'm not writing about these in an attempt to assess
or measure my own culpability, nor am I trying to denigrate
or cast blame on others.

I think my main point of writing this is just to vent a bit
and say that...working with other people is hard. Trying to
do a meaningful project, even by yourself (or in a
collaborative project for reasons not because of any aspect
of collaboration) is hard. Sometimes it's because the work
is hard and takes many, many hours, after which people
become tired and uninterested and in general burnt out.
Sometimes people don't feel that way specifically about the
project but about the world at large, and because of that we
fall and stop even before contributing in a meaningful sense
to the project. Sometimes we just don't do or respond to
messages because that's what's easiest, even if we aren't
suffering in any sense. I've been in all three cases, I've
worked with others who were in all three cases. I've been up
and down emotionally, but even in the up swings I'm the
same: nonresponsive and unwilling to work on projects I've
voluntarily committed to. Spending only a few hours a day on
things that are productive for hte wolrd at large and
spending the rest in brainless spirals, not because I am
depressed (perhaps I am, but still) but just because that is
more enjoyable than the things that are meaningful and
important. Evne this post...I'm procrastinating on writing a
message to Mark, on finishing that porject for Emma Zang
that I said I would send to her by this weekend.

Working with other people is hard. I don't see any general
solutions to this for myself or for others. Obviously we
want to transition to more sustainable work environments and
processes, and build systems that encourage sustainable
collaborative work, and have that sort of wider culture to
lift up individuals when we are feeling down or unenthused,
but it's difficult to do that.

I have this sort of expectation that when I have a full-time
job, as long as the number of hours I work in it is not
unreasonable and the work itself is not incredibly taxing,
I'll be in a much better place to contribute towards these
types of projects, because I'll have a stable income, and
with that more control over my housing and leisure, and thus
a greater desire to work on collaborative projects than run
away from them in search of the small moments of time and
space I can own for myslef. This isn't guaranteed, of course
(it might be the exact same way once I have consistent
source of income), and even if it is it doesn't solve or
address the problem I have right now of being able to work
on things that matter to me and bieng able to resonate that
desire with others.

I'm going to Korea, to take a break from things. Perhaps
that will change things, and when I return or when I arrive
there I will have a bigger desire to work on the things that
I find interesting now but cannot bring ymself to
productively engage with. But what am I even taking a break
from? Days of working just for one or two hours? Getting up
at 11:30am and watching three hours of youtube content I
can't even remember after that?

I guess this is where I add some caveats. I have been
working on some projects, especially smaller personal
projects, and I have been relatively productive there. I am
in okay shape with my thesis (37 pages out of the
recommended 45-60 due by March 19th) and even with Emma Zang
re: this project I've been lagging for two years on, I've
contributed over a thousand lines of code and did all of
that in some senses independently (e.g. I learned concepts I
needed on my own, did all the code in my own room, found
data sources on my own, came up with clean code guidelines
on my own... I did engage). For Evictorbook, I submitted
some revisions today and a few people gave positive feedback
and suggestiosn.

And even if I hadn't. If I had not been productive at all.
If I rotten in bed with no good reason, if I am not
depressed and simply "lazy" (quotes because that construct,
along with anything else here, is just a product of our
imagination that does more harm than good to speak about),
even though all of these projects that I am not working on
are positive goods that help make a better world (e.g. not
parasitic jobs that suck value from me for those that own
the measn of production, where it would be ~praxis ~ to not
work) ... that would still be okay. I would still be a human
being, and at least for the aspect of not working I would
not be worth any less.

Working with others is hard. Heck, working by yourself is
hard. I don't really know what to do, except to plod on and
hope things change. Here's to that.
