---
created: 2021-11-01
modified: [2022-11-01, 2022-03-23]
title:
author: Nathan
---

i think writing by keys is much faster than writing by hand.
i also think it feels nicer. I never use vim mode, i've
never bothered to learn or try it, but maybe I should

I thought of this while writing notes for
[11-1](schoolwork/KREN152/notes/11-1.md) today, because
typing in korean is what i want to do but it feels so slow
and difficult

if i just keep doing it it'll become easier and faster

---

Update aroudn four months later. Typing Korean is still not
quite native but is definitely better than writing by hand.
