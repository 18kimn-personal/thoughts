---
created: 2022-03-23
modified: [2022-03-23]
title:
  Writing about code (technical things) actually takes a
  while
tags: [writing, workflow]
author: Nathan
---

Writing about code actually takes a while! Both in terms of
getting the words to be clear, and in terms of the actual
words that need to be said! Often expressing things in code
or documentation seems to take much less time and words than
expressing the equivalent concepts in prose sentences

I was feeling this while working on my RSS feed thing, as I
was explaining the UI setup, the server setup, how they
interact, why I worked on the server, how the public access
mechanism works, etc. It took a long time! and after I wrote
a substantial amount of it I was left with so much dry text
that I had tried to simplify as much as possible, only to be
left with gigantic paragraphs that no one would ever want to
read

I've [written before](./writing-by-hand.md) on how typing is
just so much more ergonomic for me, and I can't imagine
writing these sorts of things by hand even as a draft
format, but still in these situations typing itself (rather
than the thought processes associated with writing) becomes
an annoying hurdle that can only be powered through instead
of torn down
