---
created: 2022-01-24
modified: [2022-01-24, 2022-01-27, 2022-08-09]
title:
  Some conventions on what a zettel should be formatted as
tags: [workflow, zettelkasten]
author: Nathan
---

- file naming
  - avoid "the", "and", "a", etc.
  - use dashes first, then underscores
  - descriptive title in the YAML, don't try to cut any
    corners
  - use lowercase letters, and no numbers (emphasis on easy
    typing)
-
